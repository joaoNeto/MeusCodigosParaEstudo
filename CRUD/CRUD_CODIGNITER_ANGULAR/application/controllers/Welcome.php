<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('Aluno_model');
            $this->load->model('Cursos_model');
    }

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function cadastrar(){
		$params = json_decode(file_get_contents('php://input'),true);

		// VALIDAR DADOS
		$retorno = $this->Aluno_model->cadastrar($params);
		echo json_encode($retorno);
	}

	public function listarCursos(){
		$retorno = $this->Cursos_model->listaCursos();
		echo json_encode($retorno);		
	}

	public function listarAlunos(){
		$retorno = $this->Aluno_model->listarAlunos();
		echo json_encode($retorno);		
	}

	public function buscarAluno($id){
		$retorno = $this->Aluno_model->detalheAluno($id);
		echo json_encode($retorno);				
	}

	public function inativarAluno(){
		$params = json_decode(file_get_contents('php://input'),true);
		$retorno = $this->Aluno_model->inativarAluno($params['id_aluno']);
		echo json_encode($retorno);						
	}

}
