-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27-Out-2017 às 20:54
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
create database `bd_mangue3`;
use `bd_mangue3`;
--



-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

CREATE TABLE IF NOT EXISTS `aluno` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `ativo` int(1) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `senha` varchar(250) NOT NULL,
  `fk_id_curso` int(5) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `estado` varchar(2) NOT NULL,
  `cidade` varchar(250) NOT NULL,
  `nome_responsavel` varchar(250) NOT NULL,
  `telefone_responsavel` varchar(20) NOT NULL,
  `estado_responsavel` varchar(2) NOT NULL,
  `cidade_responsavel` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`id`, `ativo`, `nome`, `email`, `senha`, `fk_id_curso`, `telefone`, `estado`, `cidade`, `nome_responsavel`, `telefone_responsavel`, `estado_responsavel`, `cidade_responsavel`) VALUES
(14, 1, 'teste02', 'teste02@hotmail.com', '06627b60cbefb29bd52236b723ea8287ca98bdbb', 1, '(81) 9312-9391', 'PE', 'feira nova', 'nome completo responsavel', '(23) 1231-2311', 'AM', 'feira nova');

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

CREATE TABLE IF NOT EXISTS `curso` (
  `nome_curso` varchar(50) NOT NULL,
  `id_curso` int(5) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_curso`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `curso`
--

INSERT INTO `curso` (`nome_curso`, `id_curso`) VALUES
('ingles', 1),
('frances', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
