<!DOCTYPE html>
<html>
<head>
	<title>CRUD Angular</title>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
</head>
<body ng-app="myApp" ng-controller="myController">

	<table>
		<tr>
			<td>nome: </td>
			<td><input type="text" 		name="nome" 		ng-model="nome"></td>
		</tr>
		<tr>
			<td>email: </td>
			<td><input type="text" 		name="email" 		ng-model="email"></td>
		</tr>
		<tr>
			<td>titulo: </td>
			<td><input type="text" 		name="titulo" 		ng-model="titulo"></td>
		</tr>
		<tr>
			<td><input type="button" 	value="Cadastrar"	ng-click="inserirDados()"></td>
			<td></td>
		</tr>
	</table>

	<br><br><br><br>
	lista
	<br><br>
	<table border="1">
		<tr>
			<td>nome</td>
			<td>email</td>
			<td>titulo</td>
			<td>deletar</td>
		</tr>
		<tr ng-repeat="std in data">
			<td>{{ std.nome }}</td>
			<td>{{ std.email }}</td>
			<td>{{ std.titulo }}</td>
			<td><input type="button" value="deletar" ng-click="deletarRegistro(std.id)"></td>
		</tr>
	</table>

</body>

<script type="text/javascript">
	
	var app= angular.module('myApp',[]);
	app.controller('myController', function($scope,$http){

		$http.get("crud.php?acao=buscar")
			.success(function(data){
				$scope.data = data;
		})

		$scope.inserirDados=function(){
			$http.post("crud.php?acao=cadastrar",{'nome':$scope.nome, 'email':$scope.email, 'titulo':$scope.titulo })
			.success(function(){
				$scope.buscarRegistros();
			})
		}

		$scope.deletarRegistro = function(id){
			$http.post("crud.php?acao=deletar",{'id':id})
			.success(function(){
				$scope.buscarRegistros();				
			})
		}

		$scope.buscarRegistros = function(){
			$http.get("crud.php?acao=buscar")
			.success(function(data){
				$scope.data = data;
			})
		}

	});

</script>

</html>