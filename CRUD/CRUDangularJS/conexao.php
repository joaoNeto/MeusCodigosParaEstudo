<?php

class Condb{

        private static $instance;
        
        public static function getInstance(){
                       
                if(!isset(self::$instance)){
                        
                        try{

                                self::$instance = new PDO("mysqli:host=localhost;dbname=form","root","");
                                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
                                self::$instance->exec("SET CHARACTER SET utf8");

                        }catch(PDOException $e){

                                echo $e->getMessage();

                        }
                        
                }
                
                return self::$instance;
        }
        
        public static function prepare($sql){
                
                return self::getInstance()->prepare($sql);
                
        }
        
}

?>