-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1
-- Data de Criação: 15-Nov-2017 às 19:34
-- Versão do servidor: 5.5.32
-- versão do PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `bd_aluno`
--
CREATE DATABASE IF NOT EXISTS `bd_aluno` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bd_aluno`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

CREATE TABLE IF NOT EXISTS `aluno` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `serie` varchar(30) NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `nome_pai` varchar(50) NOT NULL,
  `nome_mae` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`id`, `nome`, `email`, `serie`, `sexo`, `nome_pai`, `nome_mae`) VALUES
(5, 'Joao Neto', 'eujoaonetoti@outlook.com', '3º  ano', 'Masculino ', 'Jose Joao de Sousa', 'Mainha'),
(6, 'ikaro sales', 'ikarosales@kobol.br', '3º  ano', 'Masculino ', 'sales ikaro', 'mamae'),
(7, 'Fred lucena', 'fred@lucena.com.br', '1º  ano', 'Masculino ', 'melo', 'melo'),
(8, 'Aldo moura', 'aldo@moura.com.br', '2º  ano', 'Feminino', 'fred', 'melo'),
(9, 'luiz gonzaga', 'luiz@gonzaga.com.br', '2º  ano', 'Selecione ', 'lampiao', 'maria bonita'),
(10, 'Rogerio aguiar', 'rogerio@hotmail.com.br', '2º  ano', 'Masculino ', 'fred', 'melo');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
