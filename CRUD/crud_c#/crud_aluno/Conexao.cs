﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crud_aluno
{
    class Conexao
    {
        MySqlConnection objConn;

        public Conexao()
        {
            // montando a conexao    
            this.objConn = new MySqlConnection("server=localhost;database=bd_aluno; uid=root;pwd=");
            
            try
            {
                this.objConn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossível estabelecer conexão. " + ex.Message);
            }
        }

        public MySqlConnection getConexao()
        {
            return this.objConn;
        }

    }
}
