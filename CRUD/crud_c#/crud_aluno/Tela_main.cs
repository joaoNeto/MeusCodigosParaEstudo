﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crud_aluno
{
    public partial class Tela_main : Form
    {
        public Tela_main()
        {
            InitializeComponent();
            listaAlunos();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            // conectando 
            Conexao conn = new Conexao();
            MySqlConnection objConn = conn.getConexao();

            String sql;
            sql = "INSERT INTO aluno (nome,email,serie,sexo,nome_pai,nome_mae) values(";
            sql += "'" + in_nome.Text + "', ";
            sql += "'" + in_email.Text + "', ";
            sql += "'" + in_serie.Text + "', ";
            sql += "'" + in_sexo.Text + "', ";
            sql += "'" + in_nome_pai.Text + "', ";
            sql += "'" + in_nome_mae.Text + "')";

            if (objConn.State == ConnectionState.Open)
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection   = objConn;
                    cmd.CommandText  = sql;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Registro incluído com sucesso.");

                    in_nome.Text  = "informe o nome completo";
                    in_email.Text = "informe o email";
                    in_serie.Text = "Selecione a série";
                    in_sexo.Text  = "Selecione o sexo";
                    in_nome_pai.Text = "informe o nome do pai";
                    in_nome_mae.Text = "informe o nome da mae";

                    listaAlunos();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocorreu o seguinte erro " + ex.Message);
                }

            }

        }

        private void lstvContatos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void listaAlunos()
        {

            String sql;

            try
            {
                // conectando 
                Conexao conn = new Conexao();
                MySqlConnection objConn = conn.getConexao();

                if (objConn.State == ConnectionState.Open)
                {
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = objConn;

                    listaDeAlunos.Items.Clear();

                    sql = "SELECT nome, email, serie, sexo, nome_pai, nome_mae, id FROM aluno ";
                    sql += "WHERE nome like '%" + in_search.Text + "%'";
                    sql += "  or email like '%" + in_search.Text + "%'";
                    sql += "  or serie like '%" + in_search.Text + "%' ";
                    sql += " ORDER BY id DESC";
                    cmd.CommandText = sql;

                    MySqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ListViewItem list = new ListViewItem(reader[0].ToString());
                        list.SubItems.Add(reader[1].ToString());
                        list.SubItems.Add(reader[2].ToString());
                        list.SubItems.Add(reader[3].ToString());
                        list.SubItems.Add(reader[4].ToString());
                        list.SubItems.Add(reader[5].ToString());
                        list.SubItems.Add(reader[6].ToString());

                        listaDeAlunos.Items.AddRange(new ListViewItem[] { list });
                    }
                    reader.Close();
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        private void in_search_TextChanged(object sender, EventArgs e)
        {
            listaAlunos();
        }

        private void duploCliqueListaAlunos(object sender, EventArgs e)
        {
            String id_aluno = listaDeAlunos.Items[listaDeAlunos.FocusedItem.Index].SubItems[6].Text;

            Tela_editar_excluir tela_editar = new Tela_editar_excluir(id_aluno);
            tela_editar.Text = "Editar ou alterar " + listaDeAlunos.Items[listaDeAlunos.FocusedItem.Index].SubItems[0].Text;
            tela_editar.Show();
        }

    }
}
