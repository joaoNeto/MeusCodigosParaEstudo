﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crud_aluno
{
    public partial class Tela_editar_excluir : Form
    {
        String id_aluno;

        public Tela_editar_excluir(String id_aluno)
        {
            InitializeComponent();
            this.id_aluno = id_aluno;
            buscarAluno();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Tela_editar_excluir.ActiveForm.Close();
        }

        public void buscarAluno()
        {

            String sql = "SELECT nome, email, serie, sexo, nome_pai, nome_mae FROM aluno WHERE id = " + id_aluno;
            // conectando 
            Conexao conn = new Conexao();
            MySqlConnection objConn = conn.getConexao();

            if (objConn.State == ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = objConn;
                cmd.CommandText = sql;

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    in_nome.Text = reader[0].ToString();
                    in_email.Text = reader[1].ToString();
                    in_serie.Text = reader[2].ToString();
                    in_sexo.Text = reader[3].ToString();
                    in_nome_pai.Text = reader[4].ToString();
                    in_nome_mae.Text = reader[5].ToString();
                }
                reader.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            String sql;
            sql  = "UPDATE aluno SET ";
            sql +=  " nome  = '" + in_nome.Text + "', " +
                    " email = '" + in_email.Text + "'," +
                    " serie = '" + in_serie.Text + "'," +
                    " sexo  = '" + in_sexo.Text + "'," +
                    " nome_pai = '" + in_nome_pai.Text + "'," +
                    " nome_mae = '" + in_nome_mae.Text + "' ";
            sql += "WHERE id = " + id_aluno;

            // conectando 
            Conexao conn = new Conexao();
            MySqlConnection objConn = conn.getConexao();

            if (objConn.State == ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = objConn;
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                MessageBox.Show("Registro alterado com sucesso.");

                // atualizar a tela pai
                Tela_main objTela = new Tela_main();
                objTela.listaAlunos();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {


            String sql = "DELETE FROM aluno WHERE id = " + id_aluno;

            // conectando 
            Conexao conn = new Conexao();
            MySqlConnection objConn = conn.getConexao();

            if (objConn.State == ConnectionState.Open)
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection   = objConn;
                cmd.CommandText  = sql;
                cmd.ExecuteNonQuery();
                MessageBox.Show("Registro excluido com sucesso.");

            }

            Tela_editar_excluir.ActiveForm.Close();

        }
    }
}
