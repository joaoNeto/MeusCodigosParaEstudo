﻿namespace crud_aluno
{
    partial class Tela_editar_excluir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.in_sexo = new System.Windows.Forms.ComboBox();
            this.in_nome_mae = new System.Windows.Forms.TextBox();
            this.in_nome_pai = new System.Windows.Forms.TextBox();
            this.in_serie = new System.Windows.Forms.ComboBox();
            this.in_email = new System.Windows.Forms.TextBox();
            this.in_nome = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // in_sexo
            // 
            this.in_sexo.FormattingEnabled = true;
            this.in_sexo.Items.AddRange(new object[] {
            "Masculino ",
            "Feminino"});
            this.in_sexo.Location = new System.Drawing.Point(12, 124);
            this.in_sexo.Name = "in_sexo";
            this.in_sexo.Size = new System.Drawing.Size(379, 21);
            this.in_sexo.TabIndex = 21;
            this.in_sexo.Text = "Selecione o sexo";
            // 
            // in_nome_mae
            // 
            this.in_nome_mae.Location = new System.Drawing.Point(12, 177);
            this.in_nome_mae.Name = "in_nome_mae";
            this.in_nome_mae.Size = new System.Drawing.Size(379, 20);
            this.in_nome_mae.TabIndex = 20;
            this.in_nome_mae.Text = "informe o nome da mae";
            // 
            // in_nome_pai
            // 
            this.in_nome_pai.Location = new System.Drawing.Point(12, 151);
            this.in_nome_pai.Name = "in_nome_pai";
            this.in_nome_pai.Size = new System.Drawing.Size(379, 20);
            this.in_nome_pai.TabIndex = 19;
            this.in_nome_pai.Text = "informe o nome do pai";
            // 
            // in_serie
            // 
            this.in_serie.FormattingEnabled = true;
            this.in_serie.Items.AddRange(new object[] {
            "1º  ano",
            "2º  ano",
            "3º  ano"});
            this.in_serie.Location = new System.Drawing.Point(12, 97);
            this.in_serie.Name = "in_serie";
            this.in_serie.Size = new System.Drawing.Size(379, 21);
            this.in_serie.TabIndex = 18;
            this.in_serie.Text = "Selecione a série";
            // 
            // in_email
            // 
            this.in_email.Location = new System.Drawing.Point(12, 71);
            this.in_email.Name = "in_email";
            this.in_email.Size = new System.Drawing.Size(379, 20);
            this.in_email.TabIndex = 17;
            this.in_email.Text = "informe o email";
            // 
            // in_nome
            // 
            this.in_nome.Location = new System.Drawing.Point(12, 45);
            this.in_nome.Name = "in_nome";
            this.in_nome.Size = new System.Drawing.Size(379, 20);
            this.in_nome.TabIndex = 16;
            this.in_nome.Text = "informe o nome completo";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(12, 203);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 23);
            this.button1.TabIndex = 22;
            this.button1.Text = "Salvar dados aluno";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Location = new System.Drawing.Point(316, 203);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 23;
            this.button2.Text = "Excluir aluno";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "< Voltar";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Tela_editar_excluir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(403, 261);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.in_sexo);
            this.Controls.Add(this.in_nome_mae);
            this.Controls.Add(this.in_nome_pai);
            this.Controls.Add(this.in_serie);
            this.Controls.Add(this.in_email);
            this.Controls.Add(this.in_nome);
            this.Name = "Tela_editar_excluir";
            this.Text = "Tela_editar_excluir";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox in_sexo;
        private System.Windows.Forms.TextBox in_nome_mae;
        private System.Windows.Forms.TextBox in_nome_pai;
        private System.Windows.Forms.ComboBox in_serie;
        private System.Windows.Forms.TextBox in_email;
        private System.Windows.Forms.TextBox in_nome;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
    }
}