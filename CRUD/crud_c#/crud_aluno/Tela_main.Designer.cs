﻿namespace crud_aluno
{
    partial class Tela_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.in_nome = new System.Windows.Forms.TextBox();
            this.in_email = new System.Windows.Forms.TextBox();
            this.in_serie = new System.Windows.Forms.ComboBox();
            this.in_nome_pai = new System.Windows.Forms.TextBox();
            this.in_nome_mae = new System.Windows.Forms.TextBox();
            this.listaDeAlunos = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.in_search = new System.Windows.Forms.TextBox();
            this.in_sexo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(12, 142);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Cadastrar aluno";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // in_nome
            // 
            this.in_nome.Location = new System.Drawing.Point(12, 36);
            this.in_nome.Name = "in_nome";
            this.in_nome.Size = new System.Drawing.Size(379, 20);
            this.in_nome.TabIndex = 3;
            this.in_nome.Text = "informe o nome completo";
            // 
            // in_email
            // 
            this.in_email.Location = new System.Drawing.Point(12, 62);
            this.in_email.Name = "in_email";
            this.in_email.Size = new System.Drawing.Size(379, 20);
            this.in_email.TabIndex = 4;
            this.in_email.Text = "informe o email";
            // 
            // in_serie
            // 
            this.in_serie.FormattingEnabled = true;
            this.in_serie.Items.AddRange(new object[] {
            "1º  ano",
            "2º  ano",
            "3º  ano"});
            this.in_serie.Location = new System.Drawing.Point(12, 88);
            this.in_serie.Name = "in_serie";
            this.in_serie.Size = new System.Drawing.Size(379, 21);
            this.in_serie.TabIndex = 7;
            this.in_serie.Text = "Selecione a série";
            this.in_serie.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // in_nome_pai
            // 
            this.in_nome_pai.Location = new System.Drawing.Point(397, 36);
            this.in_nome_pai.Name = "in_nome_pai";
            this.in_nome_pai.Size = new System.Drawing.Size(358, 20);
            this.in_nome_pai.TabIndex = 9;
            this.in_nome_pai.Text = "informe o nome do pai";
            // 
            // in_nome_mae
            // 
            this.in_nome_mae.Location = new System.Drawing.Point(397, 62);
            this.in_nome_mae.Name = "in_nome_mae";
            this.in_nome_mae.Size = new System.Drawing.Size(358, 20);
            this.in_nome_mae.TabIndex = 10;
            this.in_nome_mae.Text = "informe o nome da mae";
            // 
            // listaDeAlunos
            // 
            this.listaDeAlunos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listaDeAlunos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listaDeAlunos.FullRowSelect = true;
            this.listaDeAlunos.GridLines = true;
            this.listaDeAlunos.Location = new System.Drawing.Point(12, 231);
            this.listaDeAlunos.Name = "listaDeAlunos";
            this.listaDeAlunos.Size = new System.Drawing.Size(743, 239);
            this.listaDeAlunos.TabIndex = 12;
            this.listaDeAlunos.UseCompatibleStateImageBehavior = false;
            this.listaDeAlunos.View = System.Windows.Forms.View.Details;
            this.listaDeAlunos.SelectedIndexChanged += new System.EventHandler(this.lstvContatos_SelectedIndexChanged);
            this.listaDeAlunos.DoubleClick += new System.EventHandler(this.duploCliqueListaAlunos);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "nome completo";
            this.columnHeader7.Width = 138;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Email";
            this.columnHeader8.Width = 170;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Serie";
            this.columnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader9.Width = 62;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Sexo";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nome do pai";
            this.columnHeader2.Width = 130;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Nome da mae";
            this.columnHeader3.Width = 141;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "id";
            this.columnHeader4.Width = 0;
            // 
            // in_search
            // 
            this.in_search.Location = new System.Drawing.Point(474, 205);
            this.in_search.Name = "in_search";
            this.in_search.Size = new System.Drawing.Size(281, 20);
            this.in_search.TabIndex = 13;
            this.in_search.TextChanged += new System.EventHandler(this.in_search_TextChanged);
            // 
            // in_sexo
            // 
            this.in_sexo.FormattingEnabled = true;
            this.in_sexo.Items.AddRange(new object[] {
            "Masculino ",
            "Feminino"});
            this.in_sexo.Location = new System.Drawing.Point(12, 115);
            this.in_sexo.Name = "in_sexo";
            this.in_sexo.Size = new System.Drawing.Size(379, 21);
            this.in_sexo.TabIndex = 15;
            this.in_sexo.Text = "Selecione o sexo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(233, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(235, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Faça uma busca por nome aluno, email ou serie:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(751, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "_________________________________________________________________________________" +
    "___________________________________________";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Lista de alunos:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(751, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "_________________________________________________________________________________" +
    "___________________________________________";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Cadastre um aluno:";
            // 
            // Tela_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(767, 479);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.in_sexo);
            this.Controls.Add(this.in_search);
            this.Controls.Add(this.listaDeAlunos);
            this.Controls.Add(this.in_nome_mae);
            this.Controls.Add(this.in_nome_pai);
            this.Controls.Add(this.in_serie);
            this.Controls.Add(this.in_email);
            this.Controls.Add(this.in_nome);
            this.Controls.Add(this.button1);
            this.Name = "Tela_main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Controle de alunos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox in_nome;
        private System.Windows.Forms.TextBox in_email;
        private System.Windows.Forms.ComboBox in_serie;
        private System.Windows.Forms.TextBox in_nome_pai;
        private System.Windows.Forms.TextBox in_nome_mae;
        private System.Windows.Forms.ListView listaDeAlunos;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.TextBox in_search;
        private System.Windows.Forms.ComboBox in_sexo;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
    }
}