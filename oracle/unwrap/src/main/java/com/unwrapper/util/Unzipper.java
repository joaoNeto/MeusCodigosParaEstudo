 package com.unwrapper.util;
 
 import java.io.ByteArrayOutputStream;
 import java.util.zip.Inflater;
 
 public class Unzipper
 {
   public static byte[] unzip(byte[] zipped) throws java.util.zip.DataFormatException, java.io.IOException
   {
     Inflater inflater = new Inflater();
     inflater.setInput(zipped);
     ByteArrayOutputStream os = new ByteArrayOutputStream(zipped.length);
     byte[] buffer = new byte['Ѐ'];
     while (!inflater.finished()) {
       int count = 0;
       count = inflater.inflate(buffer);
       os.write(buffer, 0, count);
     }
     os.close();
     return os.toByteArray();
   }
 }
