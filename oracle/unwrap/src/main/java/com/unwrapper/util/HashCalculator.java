package com.unwrapper.util;

import java.security.MessageDigest;

public class HashCalculator
{
  public static byte[] getSHA1(byte[] b) throws java.security.NoSuchAlgorithmException
  {
    MessageDigest md = MessageDigest.getInstance("SHA1");
    md.update(b);
    return md.digest();
  }
  
  protected static final char[] hexArray = "0123456789ABCDEF".toCharArray();
  
  public static String bytesToHex(byte[] bytes) {
    char[] hexChars = new char[bytes.length * 2];
    for (int j = 0; j < bytes.length; j++) {
      int v = bytes[j] & 0xFF;
      hexChars[(j * 2)] = hexArray[(v >>> 4)];
      hexChars[(j * 2 + 1)] = hexArray[(v & 0xF)];
    }
    return new String(hexChars);
  }
}
