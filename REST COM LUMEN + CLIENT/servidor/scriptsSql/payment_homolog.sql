-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: jobbprodt2.cjaevdzbytp1.us-east-1.rds.amazonaws.com
-- Generation Time: Sep 05, 2017 at 08:17 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.0.8-4+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `payment_homolog`
--

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'INSERT',
  `id_subscription_table` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `current_transaction` text COLLATE utf8_unicode_ci,
  `plan_id` int(11) DEFAULT NULL,
  `plan_days` int(11) NOT NULL,
  `plan_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `plan_amount` decimal(12,2) NOT NULL,
  `plan_payment_method` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `card_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorization_code` int(11) DEFAULT NULL,
  `boleto_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `boleto_barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `boleto_expiration_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `post_back` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cliente_nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cliente_email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_sexo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cliente_nascimento` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cliente_tel_ddi` int(11) DEFAULT NULL,
  `cliente_tel_ddd` int(11) NOT NULL,
  `cliente_tel_numero` int(11) NOT NULL,
  `cliente_cpfcpj` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_pais` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_uf` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_cidade` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_logradouro` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_numero` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_complemento` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_bairro` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_cep` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `tipo`, `id_subscription_table`, `id_user`, `cliente_id`, `subscription_id`, `transaction_id`, `current_transaction`, `plan_id`, `plan_days`, `plan_name`, `plan_amount`, `plan_payment_method`, `card_id`, `authorization_code`, `boleto_url`, `boleto_barcode`, `boleto_expiration_date`, `post_back`, `status`, `cliente_nome`, `cliente_email`, `cliente_sexo`, `cliente_nascimento`, `cliente_tel_ddi`, `cliente_tel_ddd`, `cliente_tel_numero`, `cliente_cpfcpj`, `cliente_end_pais`, `cliente_end_uf`, `cliente_end_cidade`, `cliente_end_logradouro`, `cliente_end_numero`, `cliente_end_complemento`, `cliente_end_bairro`, `cliente_end_cep`, `created_at`, `updated_at`) VALUES
(1, 'INSERT', 1, 2, 5, NULL, NULL, NULL, NULL, 30, '5980_Y6EiYPijfRZEb3u8uyEgdaoJ8ljMqVOH', '59.80', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-22 11:53:55', '2017-08-22 11:53:55'),
(2, 'UPDATE', 1, 2, 5, 229087, 1888652, NULL, 198294, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-08-29 12:02:59', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-22 11:53:56', '2017-08-22 11:53:56'),
(3, 'UPDATE_PLANO', 1, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, NULL, '', '', '', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-22 11:56:21', '2017-08-22 11:56:21'),
(4, 'INSERT', 2, 2, 5, NULL, NULL, NULL, NULL, 30, '5980_UdJzpWtGEROf0XIYZg34Ma0vqZiZX5g2', '59.80', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-22 12:01:30', '2017-08-22 12:01:30'),
(5, 'UPDATE', 2, 2, 5, 229088, 1888662, NULL, 198296, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-08-29 12:10:35', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-22 12:01:31', '2017-08-22 12:01:31'),
(6, 'INSERT', 3, 2, 5, NULL, NULL, NULL, NULL, 30, '5980_BHSOvRoKBE7VIfJ6HPmjQALnK9eghy8H', '59.80', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-22 12:21:47', '2017-08-22 12:21:47'),
(7, 'UPDATE', 3, 2, 5, 229089, 1888709, NULL, 198297, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-08-29 12:30:52', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-22 12:21:48', '2017-08-22 12:21:48'),
(8, 'INSERT', 4, 2, 5, NULL, NULL, NULL, NULL, 30, '5980_OwlJjZnKFq9qnMctCT3dVZqouhHRvWDS', '59.80', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-22 18:33:32', '2017-08-22 18:33:32'),
(9, 'UPDATE', 4, 2, 5, 230037, 1893104, NULL, 199004, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-08-29 18:42:37', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-22 18:33:33', '2017-08-22 18:33:33'),
(10, 'INSERT', 5, 2, 5, NULL, NULL, NULL, NULL, 30, '44850_ELuyUmtoklYoyxuEuQwhDnx3R7jKzBMB', '448.50', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-22 19:32:14', '2017-08-22 19:32:14'),
(11, 'UPDATE', 5, 2, 5, 230597, 1895161, NULL, 199310, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-08-29 19:41:20', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-22 19:32:16', '2017-08-22 19:32:16'),
(12, 'UPDATE', 5, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-22 19:33:34', '2017-08-22 19:33:34'),
(13, 'INSERT', 6, 2, 5, NULL, NULL, NULL, NULL, 30, '59800_bjDblHZikZNgjtlNG6EAqbLS4XbDtRRp', '598.00', 'credit_card', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-22 19:46:22', '2017-08-22 19:46:22'),
(14, 'UPDATE', 6, 2, 0, 230623, 1895273, NULL, 199336, 0, '', '0.00', 'credit_card', 'card_cj6o0g9g00aexo66e2ot5n2s2', 767145, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-22 19:46:54', '2017-08-22 19:46:54'),
(15, 'INSERT', 7, 2, 5, NULL, NULL, NULL, NULL, 30, '116610_AG0HWtk6mhh4Irai8BnwCyxxLVD2IKxg', '1.00', 'credit_card', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-22 20:11:11', '2017-08-22 20:11:11'),
(16, 'INSERT', 8, 2, 5, NULL, NULL, NULL, NULL, 30, '116610_z9CQlEaeh6kalMJqbN7JiKNZEtoyRZhl', '1.00', 'credit_card', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-22 20:17:20', '2017-08-22 20:17:20'),
(17, 'UPDATE', 8, 2, 0, 230672, 1895686, NULL, 199376, 0, '', '0.00', 'credit_card', 'card_cj6o1lks609g8ek6d0ndoh1xn', 405048, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-22 20:19:02', '2017-08-22 20:19:02'),
(18, 'UPDATE_PLANO', 8, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, NULL, '', '', '', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-23 16:53:14', '2017-08-23 16:53:14'),
(19, 'UPDATE_PLANO', 8, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, NULL, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-23 17:48:27', '2017-08-23 17:48:27'),
(20, 'UPDATE_PLANO', 8, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, NULL, '', '', '', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-23 17:55:49', '2017-08-23 17:55:49'),
(21, 'INSERT', 9, 2, 5, NULL, NULL, NULL, NULL, 30, '8970_SgV7pXffQpWUI3pN18YZIaMHWE9x6LoS', '89.70', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 11:10:50', '2017-08-25 11:10:50'),
(22, 'UPDATE', 9, 2, 5, 234213, 1916183, NULL, 202006, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 11:20:01', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 11:10:51', '2017-08-25 11:10:51'),
(23, 'INSERT', 10, 2, 5, NULL, NULL, NULL, NULL, 30, '8970_SLx7C8EPqer9xivMkH4hJfGNHQgof1pK', '89.70', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 11:12:21', '2017-08-25 11:12:21'),
(24, 'UPDATE', 10, 2, 5, 234214, 1916187, NULL, 202007, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 11:21:32', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 11:12:22', '2017-08-25 11:12:22'),
(25, 'INSERT', 11, 2, 5, NULL, NULL, NULL, NULL, 30, '8970_4MkW02ndl2MS4RBKFL7tims8blHNJfv7', '89.70', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 11:14:36', '2017-08-25 11:14:36'),
(26, 'UPDATE', 11, 2, 5, 234215, 1916190, NULL, 202008, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 11:23:47', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 11:14:37', '2017-08-25 11:14:37'),
(27, 'INSERT', 12, 2, 5, NULL, NULL, NULL, NULL, 30, '8970_3es5Sp94tBs4OAdFYCdqzdeMhcgJWk7p', '89.70', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 11:28:38', '2017-08-25 11:28:38'),
(28, 'UPDATE', 12, 2, 5, 234216, 1916204, NULL, 202009, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 11:37:49', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 11:28:39', '2017-08-25 11:28:39'),
(29, 'UPDATE', 12, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 11:32:47', '2017-08-25 11:32:47'),
(30, 'INSERT', 13, 2, 5, NULL, NULL, NULL, NULL, 30, '11960_lIjHEZaY6EVYmbqtOE18s8yEYPxb6GwU', '119.60', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 11:45:02', '2017-08-25 11:45:02'),
(31, 'UPDATE', 13, 2, 5, 234217, 1916222, NULL, 202010, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 11:54:13', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 11:45:03', '2017-08-25 11:45:03'),
(32, 'UPDATE', 13, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 11:46:46', '2017-08-25 11:46:46'),
(33, 'INSERT', 14, 2, 5, NULL, NULL, NULL, NULL, 30, '11960_Is808IsbVCX9fVqVYpWhdRzrZSf2JHYz', '119.60', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 12:12:50', '2017-08-25 12:12:50'),
(34, 'UPDATE', 14, 2, 5, 234222, 1916248, NULL, 202012, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 12:22:01', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 12:12:51', '2017-08-25 12:12:51'),
(35, 'UPDATE', 14, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 12:14:34', '2017-08-25 12:14:34'),
(36, 'INSERT', 15, 2, 5, NULL, NULL, NULL, NULL, 30, '11960_ZMj7QF6tjFEQAeOKX7o8Quv11nk8Z8GJ', '119.60', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 12:54:32', '2017-08-25 12:54:32'),
(37, 'UPDATE', 15, 2, 5, 234224, 1916336, NULL, 202013, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 13:03:43', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 12:54:36', '2017-08-25 12:54:36'),
(38, 'UPDATE', 15, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 12:56:11', '2017-08-25 12:56:11'),
(39, 'INSERT', 16, 2, 5, NULL, NULL, NULL, NULL, 30, '11960_xOE6kA5ffzXUTCRgtmc7P8kOHZRSqccA', '119.60', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 13:08:15', '2017-08-25 13:08:15'),
(40, 'UPDATE', 16, 2, 5, 234312, 1916708, NULL, 202157, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 13:17:26', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 13:08:16', '2017-08-25 13:08:16'),
(41, 'UPDATE', 16, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 13:10:24', '2017-08-25 13:10:24'),
(42, 'UPDATE', 16, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 13:10:24', '2017-08-25 13:10:24'),
(43, 'INSERT', 17, 2, 5, NULL, NULL, NULL, NULL, 30, '648830_qiM8Z6U2dhasj0Gm2unmlvaklPx6Hi63', '6.00', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 13:27:02', '2017-08-25 13:27:02'),
(44, 'UPDATE', 17, 2, 5, 234318, 1916761, NULL, 202161, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 13:36:13', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 13:27:03', '2017-08-25 13:27:03'),
(45, 'UPDATE', 17, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 13:29:55', '2017-08-25 13:29:55'),
(46, 'INSERT', 18, 2, 5, NULL, NULL, NULL, NULL, 30, '11960_TQaTKOqRCv2iCL4PbtTp3Yac4O1Xa8IT', '119.60', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 13:35:47', '2017-08-25 13:35:47'),
(47, 'UPDATE', 18, 2, 5, 234327, 1916829, NULL, 202180, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 13:44:58', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 13:35:48', '2017-08-25 13:35:48'),
(48, 'UPDATE', 18, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 13:38:35', '2017-08-25 13:38:35'),
(49, 'INSERT', 19, 2, 5, NULL, NULL, NULL, NULL, 30, '11960_lcy5uboUj4ZPWQWG9VrNsaCXuKuQp6f6', '119.60', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 15:09:18', '2017-08-25 15:09:18'),
(50, 'UPDATE', 19, 2, 5, 234536, 1918343, NULL, 202692, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 15:18:30', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 15:09:20', '2017-08-25 15:09:20'),
(51, 'UPDATE', 19, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 15:11:53', '2017-08-25 15:11:53'),
(52, 'INSERT', 20, 2, 5, NULL, NULL, NULL, NULL, 30, '11960_8bnsI4wQneUki4UlqXOsUkyehCqh3GKf', '119.60', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 15:30:22', '2017-08-25 15:30:22'),
(53, 'UPDATE', 20, 2, 5, 234541, 1918469, NULL, 202694, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 15:39:33', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 15:30:23', '2017-08-25 15:30:23'),
(54, 'UPDATE', 20, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 15:32:42', '2017-08-25 15:32:42'),
(55, 'INSERT', 21, 2, 5, NULL, NULL, NULL, NULL, 30, '11960_vNkaSbZ1cQwD9vP2b9EYv7ptSGjqmNFr', '119.60', 'credit_card', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 15:37:05', '2017-08-25 15:37:05'),
(56, 'UPDATE', 21, 2, 0, 234542, 1918477, NULL, 202695, 0, '', '0.00', 'credit_card', 'card_cj6s1v2ye01oeda6emkmeiw4v', 683914, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 15:37:25', '2017-08-25 15:37:25'),
(57, 'INSERT', 22, 2, 5, NULL, NULL, NULL, NULL, 30, '11960_1oHz3zd54Um7IpzAjO7fweZTAEqwXxdm', '119.60', 'credit_card', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 15:46:58', '2017-08-25 15:46:58'),
(58, 'UPDATE', 22, 2, 0, 234543, 1918483, NULL, 202696, 0, '', '0.00', 'credit_card', 'card_cj6s2ax7601lbms6dmeox69d7', 226515, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 15:49:44', '2017-08-25 15:49:44'),
(59, 'INSERT', 23, 2, 5, NULL, NULL, NULL, NULL, 30, '11960_lTV3gjmQwkfcsvFtBjOBKXeqjPaCQtZK', '119.60', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 18:01:20', '2017-08-25 18:01:20'),
(60, 'UPDATE', 23, 2, 5, 234565, 1918750, NULL, 202708, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-01 18:10:32', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 18:01:21', '2017-08-25 18:01:21'),
(61, 'UPDATE_PLANO', 23, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, NULL, '', '', '', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-25 18:06:10', '2017-08-25 18:06:10'),
(62, 'INSERT', 24, 2, 7408, NULL, NULL, NULL, NULL, 30, '6480_UZFRfy42S3VjUTSxTcXCQUw2eLSTA0qh', '64.80', 'boleto', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro SES', 'sandrocavalcanti@gmail.com', '', NULL, NULL, 81, 837882828, '04007356440', 'Brazil', 'PE', 'Recife', 'Rua Ribeiro de Brito', '901', '903', 'Boa Viagem', '51.021310', '2017-08-28 14:09:16', '2017-08-28 14:09:16'),
(63, 'UPDATE', 24, 2, 7408, 234992, 1927482, NULL, 203179, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-04 14:18:33', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-28 14:09:17', '2017-08-28 14:09:17'),
(64, 'UPDATE', 24, 2, 7408, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-28 14:12:48', '2017-08-28 14:12:48'),
(65, 'INSERT', 25, 2, 8067, NULL, NULL, NULL, NULL, 30, '6480_qJ7Y0dqaTGoTlNfrZFQHRR4lUbHiLJ3Z', '64.80', 'credit_card', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro teste', 'sandrocavalcanti@yahoo.com.br', '', NULL, NULL, 81, 993893981, '04007356440', 'Brazil', 'PE', 'Recife', 'Rua Ribeiro de Brito', '901', 'SL 903', 'Boa Viagem', '51.021310', '2017-08-28 14:18:36', '2017-08-28 14:18:36'),
(66, 'UPDATE', 25, 2, 8067, 234995, 1927537, NULL, 203180, 0, '', '0.00', 'boleto', NULL, NULL, 'https://pagar.me', '1234 5678', '2017-09-04 14:28:15', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-28 14:18:58', '2017-08-28 14:18:58'),
(67, 'UPDATE', 25, 2, 8067, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-28 14:23:09', '2017-08-28 14:23:09'),
(68, 'UPDATE_PLANO', 24, 2, 7408, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, NULL, '', '', '', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 18:00:18', '2017-08-29 18:00:18'),
(69, 'UPDATE', 24, 2, 7408, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 18:01:04', '2017-08-29 18:01:04'),
(70, 'UPDATE', 25, 2, 8067, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'canceled', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 18:15:54', '2017-08-29 18:15:54'),
(71, 'UPDATE_PLANO', 24, 2, 7408, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, NULL, '', '', '', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 18:20:56', '2017-08-29 18:20:56'),
(72, 'UPDATE_PLANO', 24, 2, 7408, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, NULL, '', '', '', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 18:28:29', '2017-08-29 18:28:29'),
(73, 'UPDATE', 24, 2, 7408, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 18:29:54', '2017-08-29 18:29:54'),
(74, 'UPDATE', 24, 2, 7408, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'canceled', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 18:46:44', '2017-08-29 18:46:44'),
(75, 'UPDATE_PLANO', 23, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, NULL, '', '', '', '', 'unpaid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 18:56:30', '2017-08-29 18:56:30'),
(76, 'UPDATE', 23, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 18:57:06', '2017-08-29 18:57:06'),
(77, 'UPDATE', 23, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 19:11:22', '2017-08-29 19:11:22'),
(78, 'UPDATE', 23, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 19:19:09', '2017-08-29 19:19:09'),
(79, 'UPDATE', 23, 2, 5, NULL, NULL, NULL, NULL, 0, '', '0.00', '', NULL, 0, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-29 19:21:24', '2017-08-29 19:21:24'),
(80, 'INSERT', 26, 2, 8067, NULL, NULL, NULL, NULL, 90, '19440_xTeCvZMp2dojFbI9MLpo87WfWOWBNKkk', '194.40', 'credit_card', NULL, NULL, '', '', '', '', 'waiting_payment', 'Sandro teste', 'sandrocavalcanti@yahoo.com.br', '', NULL, NULL, 81, 993893981, '04007356440', 'Brazil', 'PE', 'Recife', 'Rua Ribeiro de Brito', '901', 'SL 903', 'Boa Viagem', '51021310', '2017-08-31 20:05:30', '2017-08-31 20:05:30'),
(81, 'UPDATE', 26, 2, 0, 235657, 1948238, NULL, 203835, 0, '', '0.00', 'credit_card', 'card_cj70w2p1t004pa36divv2hqs9', 510628, '', '', '', '', 'paid', '', '', NULL, NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', '2017-08-31 20:06:06', '2017-08-31 20:06:06');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subscription_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `current_transaction` text COLLATE utf8_unicode_ci,
  `current_period_end` datetime NOT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `plan_days` int(11) NOT NULL,
  `plan_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `plan_amount` decimal(12,2) NOT NULL,
  `plan_amount_user` int(11) NOT NULL,
  `plan_storage_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plan_payment_method` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `card_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorization_code` int(11) DEFAULT NULL,
  `boleto_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `boleto_barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `boleto_expiration_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `post_back` text COLLATE utf8_unicode_ci NOT NULL,
  `cliente_nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cliente_email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_sexo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cliente_nascimento` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cliente_tel_ddi` int(11) DEFAULT NULL,
  `cliente_tel_ddd` int(11) NOT NULL,
  `cliente_tel_numero` int(11) NOT NULL,
  `cliente_cpfcpj` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_pais` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_uf` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_cidade` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_logradouro` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_numero` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_complemento` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_bairro` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_cep` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `id_user`, `cliente_id`, `status`, `subscription_id`, `transaction_id`, `current_transaction`, `current_period_end`, `plan_id`, `plan_days`, `plan_name`, `plan_amount`, `plan_amount_user`, `plan_storage_user`, `plan_payment_method`, `card_id`, `authorization_code`, `boleto_url`, `boleto_barcode`, `boleto_expiration_date`, `post_back`, `cliente_nome`, `cliente_email`, `cliente_sexo`, `cliente_nascimento`, `cliente_tel_ddi`, `cliente_tel_ddd`, `cliente_tel_numero`, `cliente_cpfcpj`, `cliente_end_pais`, `cliente_end_uf`, `cliente_end_cidade`, `cliente_end_logradouro`, `cliente_end_numero`, `cliente_end_complemento`, `cliente_end_bairro`, `cliente_end_cep`, `created_at`, `updated_at`) VALUES
(23, 2, 5, 'paid', 234565, 1934322, NULL, '2018-08-24 19:30:42', 203403, 90, '35880_ZrDhZp3bYXG7cSeJ8IACkFQgObJtuPC5', '358.80', 34, '100MB', 'boleto', NULL, 0, 'https://pagar.me', '1234 5678', '2018-08-24T19:30:42.779Z', '', 'Sandro ME', 'sandro@unitybrasil.com.br', '', NULL, NULL, 81, 33232323, '04007356440', 'Brazil', 'PE', 'Jaboatão dos Guararapes', 'Avenida Bernardo Vieira de Melo', '5389', '501', 'Candeias', '54.440620', '2017-08-25 18:01:20', '2017-08-29 19:21:24'),
(26, 2, 8067, 'paid', 235657, 1948238, NULL, '2017-11-29 20:15:29', 203835, 90, '19440_xTeCvZMp2dojFbI9MLpo87WfWOWBNKkk', '194.40', 2, '1GB', 'credit_card', 'card_cj70w2p1t004pa36divv2hqs9', 510628, NULL, NULL, NULL, '', 'Sandro teste', 'sandrocavalcanti@yahoo.com.br', '', NULL, NULL, 81, 993893981, '04007356440', 'Brazil', 'PE', 'Recife', 'Rua Ribeiro de Brito', '901', 'SL 903', 'Boa Viagem', '51021310', '2017-08-31 20:05:30', '2017-08-31 20:06:06');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `origem` enum('A','G','J','M') COLLATE utf8_unicode_ci NOT NULL,
  `postback` text COLLATE utf8_unicode_ci,
  `key_producao` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `key_teste` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `origem`, `postback`, `key_producao`, `key_teste`, `redirect_url`, `created_at`, `updated_at`) VALUES
(1, 'jobb', '$2y$12$OSKwBaoTlI4GVzCcvSbnl.09Gvuu3cqvuCQRUfEVjfwwixWW9p2fe', 'J', 'http://homolog.jobb.com.br/post-back-payment-service', '', NULL, '', NULL, NULL),
(2, 'meets', '$2y$12$OSKwBaoTlI4GVzCcvSbnl.09Gvuu3cqvuCQRUfEVjfwwixWW9p2fe', 'M', 'http://homolog.meets.com.br/post-back-payment-service', 'ak_live_eDWLCJh4p7m8neUtbAdrYMYIsnNWIS', 'ak_test_zIf5jfwUwQ9OhFGoOxL0K7HNEtjzYO', 'http://homolog.meets.com.br/empresa/meu-plano#3062', NULL, NULL),
(4, 'gestor', '$2y$12$OSKwBaoTlI4GVzCcvSbnl.09Gvuu3cqvuCQRUfEVjfwwixWW9p2fe', 'G', 'http://gestor24hadmin.gestor24h.com.br/post-back-payment-service', 'ak_live_2atVnDghNa8DVK2epF0lR9gp4q6XZH', 'ak_test_snFskg1yO2i8sOMttsHmv0xAhK1McT', 'http://homolog.gestor24h.com.br/extrato', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
