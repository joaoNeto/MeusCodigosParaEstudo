-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'INSERT',
  `id_subscription_table` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `current_transaction` text COLLATE utf8_unicode_ci,
  `plan_id` int(11) DEFAULT NULL,
  `plan_days` int(11) NOT NULL,
  `plan_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `plan_amount` decimal(12,2) NOT NULL,
  `plan_payment_method` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `card_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorization_code` int(11) DEFAULT NULL,
  `boleto_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `boleto_barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `boleto_expiration_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `post_back` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cliente_nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cliente_email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_sexo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cliente_nascimento` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cliente_tel_ddi` int(11) DEFAULT NULL,
  `cliente_tel_ddd` int(11) NOT NULL,
  `cliente_tel_numero` int(11) NOT NULL,
  `cliente_cpfcpj` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_pais` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_uf` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_cidade` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_logradouro` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_numero` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_complemento` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_bairro` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_cep` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `log` (`id`, `tipo`, `id_subscription_table`, `id_user`, `cliente_id`, `subscription_id`, `transaction_id`, `current_transaction`, `plan_id`, `plan_days`, `plan_name`, `plan_amount`, `plan_payment_method`, `card_id`, `authorization_code`, `boleto_url`, `boleto_barcode`, `boleto_expiration_date`, `post_back`, `status`, `cliente_nome`, `cliente_email`, `cliente_sexo`, `cliente_nascimento`, `cliente_tel_ddi`, `cliente_tel_ddd`, `cliente_tel_numero`, `cliente_cpfcpj`, `cliente_end_pais`, `cliente_end_uf`, `cliente_end_cidade`, `cliente_end_logradouro`, `cliente_end_numero`, `cliente_end_complemento`, `cliente_end_bairro`, `cliente_end_cep`, `created_at`, `updated_at`) VALUES
(1,	'INSERT',	1,	4,	1,	NULL,	NULL,	NULL,	NULL,	30,	'100_HN5GkdLFH3ZF7SJiAj1wT0EnLSWzi08a',	1.00,	'credit_card',	NULL,	NULL,	'',	'',	'',	'',	'waiting_payment',	'Empresa S/A',	'glauberthy@gmail.com',	'',	NULL,	NULL,	81,	33382424,	'49101495000134',	'Brazil',	'Pe',	'Recife',	'rua tal',	'51',	'casa A',	'Ipsep',	'51340030',	'2017-07-04 11:32:48',	'2017-07-04 11:32:48'),
(2,	'UPDATE',	1,	4,	0,	210419,	1678422,	NULL,	173193,	0,	'',	0.00,	'credit_card',	'card_cj4pi2j5y0010vy6dsetvqgah',	384354,	'',	'',	'',	'',	'paid',	'',	'',	NULL,	NULL,	NULL,	0,	0,	'',	'',	'',	'',	'',	'',	'',	'',	'',	'2017-07-04 11:35:36',	'2017-07-04 11:35:36'),
(3,	'UPDATE_PLANO',	1,	4,	1,	NULL,	NULL,	NULL,	NULL,	0,	'',	0.00,	'',	NULL,	NULL,	'',	'',	'',	'',	'waiting_payment',	'',	'',	NULL,	NULL,	NULL,	0,	0,	'',	'',	'',	'',	'',	'',	'',	'',	'',	'2017-07-04 11:46:01',	'2017-07-04 11:46:01'),
(4,	'UPDATE_PLANO',	1,	4,	1,	NULL,	NULL,	NULL,	NULL,	0,	'',	0.00,	'',	NULL,	807964,	'',	'',	'',	'',	'paid',	'',	'',	NULL,	NULL,	NULL,	0,	0,	'',	'',	'',	'',	'',	'',	'',	'',	'',	'2017-07-04 11:49:09',	'2017-07-04 11:49:09');

DROP TABLE IF EXISTS `subscription`;
CREATE TABLE `subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `current_transaction` text COLLATE utf8_unicode_ci,
  `current_period_end` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `plan_days` int(11) NOT NULL,
  `plan_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `plan_amount` decimal(12,2) NOT NULL,
  `plan_amount_user` int(11) NOT NULL,
  `plan_storage_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plan_payment_method` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `card_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorization_code` int(11) DEFAULT NULL,
  `boleto_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `boleto_barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `boleto_expiration_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `post_back` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cliente_nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cliente_email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_sexo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cliente_nascimento` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cliente_tel_ddi` int(11) DEFAULT NULL,
  `cliente_tel_ddd` int(11) NOT NULL,
  `cliente_tel_numero` int(11) NOT NULL,
  `cliente_cpfcpj` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_pais` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_uf` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_cidade` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_logradouro` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_numero` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_complemento` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_bairro` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_cep` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `subscription` (`id`, `id_user`, `cliente_id`, `subscription_id`, `transaction_id`, `current_transaction`, `current_period_end`, `plan_id`, `plan_days`, `plan_name`, `plan_amount`, `plan_amount_user`, `plan_storage_user`, `plan_payment_method`, `card_id`, `authorization_code`, `boleto_url`, `boleto_barcode`, `boleto_expiration_date`, `post_back`, `status`, `cliente_nome`, `cliente_email`, `cliente_sexo`, `cliente_nascimento`, `cliente_tel_ddi`, `cliente_tel_ddd`, `cliente_tel_numero`, `cliente_cpfcpj`, `cliente_end_pais`, `cliente_end_uf`, `cliente_end_cidade`, `cliente_end_logradouro`, `cliente_end_numero`, `cliente_end_complemento`, `cliente_end_bairro`, `cliente_end_cep`, `created_at`, `updated_at`) VALUES
(1,	4,	1,	210419,	1678433,	NULL,	'2017-10-02T11:51:07.764Z',	173195,	90,	'300_J322b7ChY6BJxDOUcfteD9Wzx6s5enDU',	3.00,	10,	'10Gb',	'credit_card',	'card_cj4pi2j5y0010vy6dsetvqgah',	807964,	NULL,	NULL,	NULL,	'',	'paid',	'Empresa S/A',	'glauberthy@gmail.com',	'',	NULL,	NULL,	81,	33382424,	'49101495000134',	'Brazil',	'Pe',	'Recife',	'rua tal',	'51',	'casa A',	'Ipsep',	'51340030',	'2017-07-04 11:32:48',	'2017-07-04 11:49:09');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `origem` enum('A','G','J','M') COLLATE utf8_unicode_ci NOT NULL,
  `postback` text COLLATE utf8_unicode_ci,
  `key_producao` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `key_teste` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `password`, `origem`, `postback`, `key_producao`, `key_teste`, `redirect_url`, `created_at`, `updated_at`) VALUES
(1,	'jobb',	'$2y$12$OSKwBaoTlI4GVzCcvSbnl.09Gvuu3cqvuCQRUfEVjfwwixWW9p2fe',	'J',	'http://homolog.jobb.com.br/post-back-payment-service',	'',	NULL,	'',	NULL,	NULL),
(2,	'meets',	'$2y$12$OSKwBaoTlI4GVzCcvSbnl.09Gvuu3cqvuCQRUfEVjfwwixWW9p2fe',	'M',	'http://homolog.meets.com.br/post-back-payment-service',	'ak_live_eDWLCJh4p7m8neUtbAdrYMYIsnNWIS',	'ak_test_zIf5jfwUwQ9OhFGoOxL0K7HNEtjzYO',	'',	NULL,	NULL),
(4,	'gestor',	'$2y$12$OSKwBaoTlI4GVzCcvSbnl.09Gvuu3cqvuCQRUfEVjfwwixWW9p2fe',	'G',	'http://gestor24hadmin.gestor24h.com.br/post-back-payment-service',	'ak_live_2atVnDghNa8DVK2epF0lR9gp4q6XZH',	'ak_test_snFskg1yO2i8sOMttsHmv0xAhK1McT',	'http://homolog.gestor24h.com.br/extrato',	NULL,	NULL);

-- 2017-07-04 12:03:58
