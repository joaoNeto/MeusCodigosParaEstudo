# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.25)
# Database: payment_service
# Generation Time: 2017-05-10 20:52:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table subscription
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subscription`;

CREATE TABLE `subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `current_transaction` text COLLATE utf8_unicode_ci,
  `plan_id` int(11) DEFAULT NULL,
  `plan_days` int(11) NOT NULL,
  `plan_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `plan_amount` decimal(12,2) NOT NULL,
  `plan_payment_method` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `card_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorization_code` int(11) DEFAULT NULL,
  `boleto_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `boleto_barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `boleto_expiration_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `post_back` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cliente_nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cliente_email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_sexo` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_nascimento` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_tel_ddi` int(11) DEFAULT NULL,
  `cliente_tel_ddd` int(11) NOT NULL,
  `cliente_tel_numero` int(11) NOT NULL,
  `cliente_cpfcpj` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_pais` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_uf` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_cidade` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_logradouro` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_numero` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_complemento` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_bairro` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cliente_end_cep` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `origem` enum('A','G','J','M') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `password`, `origem`, `created_at`, `updated_at`)
VALUES
	(1,'jobb','$2y$12$OSKwBaoTlI4GVzCcvSbnl.09Gvuu3cqvuCQRUfEVjfwwixWW9p2fe','J',NULL,NULL),
	(2,'meets','$2y$12$OSKwBaoTlI4GVzCcvSbnl.09Gvuu3cqvuCQRUfEVjfwwixWW9p2fe','M',NULL,NULL);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
