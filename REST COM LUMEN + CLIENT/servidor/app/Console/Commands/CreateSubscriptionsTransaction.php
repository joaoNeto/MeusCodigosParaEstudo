<?php
namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use App\Model\Dao\Subscription;
use App\Model\Dao\Transactions;
use App\Model\EmailService\PhpMailerConf;
use Illuminate\Console\Command;
use App\Model\PaymentService;
use App\Model\Dao\User;

class CreateSubscriptionsTransaction extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Subscription:createtransaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pega todas as assinaturas com forma de pagamento como boleto e um range de um mes a partir de quinze dias atrás que estão com status pago e cria uma nova transação';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){

        $listaClientes = [];
        $erros = [];
        try {

            // PEGANDO TODAS AS SUBSCRIPTIONS SIGN COM UM RANGE DE UM MES CUJA FORMA DE PAGAMENTO SEJA DE BOLETO E O STATUS SEJA PAGA 
            $resultQuery = Subscription::where('payment_type', Subscription::PAYMENT_TYPE_SIGN)
                ->whereBetween('current_period_end', [date('Y-m-d', strtotime("-15 days")), date('Y-m-d', strtotime("+15 days"))])
                ->where('plan_payment_method', 'boleto')
                ->where('status', Transactions::STATUS_PAID)
                ->get(); // ->toSql();
            foreach ($resultQuery as $sign) {
                // ALTERA O VALOR DA ASSINATURA PARA AGUARDANDO PAGAMENTO
                $subscription = Subscription::updateStatusById($sign->id, Transactions::STATUS_WAITING);
                // CRIA UMA NOVA TRANSAÇÃO
                $transaction = PaymentService::handleBoleto($subscription);
                $listaClientes[] = [
                    'id_user'    => $sign->id_user, 
                    'cliente_id' => $sign->cliente_id, 
                    'nome'       => $sign->cliente_nome,
                    'transaction'=> $transaction
                ]; 
            }

        } catch(\Exception $e) {
            $erros[] = ['Erro no sistema ao rodar cron. '.$e->getMessage()];
        }

        // ENVIANDO RELATÓRIO DA CRON
        $htmlListCliente  = "<br>---------------------------------------------------------------------- <br>";
        $htmlListCliente .= "id do sistema - id do cliente - nome do cliente - id da transaction <br>";
        $htmlListCliente .= "---------------------------------------------------------------------- <br>";
        foreach($listaClientes as $cliente) 
            $htmlListCliente .= $cliente['id_user']." - ".$cliente['cliente_id']." - ".$cliente['nome']." - ".$cliente['transaction']->id." <br>";

        $htmlErrors = '';
        foreach($erros as $erro) 
            $htmlErrors .= $erro."<br>";

        $msg  = "<br>";
        $msg .= "DESCRIÇÃO DA CRON.: Pega todas as assinaturas com forma de pagamento como boleto e um range de um mes a partir de quinze dias atrás que estão com status pago e cria uma nova transação. <br>";
        $msg .= "DATA DA CRON.: ".date("d/m/Y h:i:s")."<br>";
        $msg .= "LISTA DE CLIENTES AFETADOS.: <br>".$htmlListCliente."<br>";
        $msg .= "ERROS.: <br> ".$htmlErrors."<br>";
        $msg .= "<br>";

        $user = User::where('id', User::JOBB)->first();
        $mail = PhpMailerConf::getSendScope($user);
        $mail->addBCC('atendimento@sistemajobb.com.br','Felipe santiago');

        $mail->addAddress('atendimento@sistemajobb.com.br','Felipe santiago');
        $mail->SetFrom('atendimento@sistemajobb.com.br','Felipe santiago');
        $mail->AddReplyTo('atendimento@sistemajobb.com.br','Felipe santiago');
        $mail->isHTML(true);
        $mail->Subject = 'UMA CRON ACABA DE SER EXECUTADA';
        $mail->Body    = $msg;
        $mail->send();
    }
}