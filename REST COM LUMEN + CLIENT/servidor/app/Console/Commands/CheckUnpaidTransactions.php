<?php
namespace App\Console\Commands;

use App\Model\Dao\Transactions;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckUnpaidTransactions extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Transaction:checkunpaid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check unpaid transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $atrasadas = Transactions::getExpiredTransactions();

        Log::debug(json_encode($atrasadas));

        return true;

    }
}