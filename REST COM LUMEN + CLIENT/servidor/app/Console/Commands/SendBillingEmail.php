<?php
namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use App\Model\Dao\Subscription;
use App\Model\Dao\Transactions;
use App\Model\EmailService\PhpMailerConf;
use Illuminate\Console\Command;
use App\Model\PaymentService;
use App\Model\Dao\User;

class SendBillingEmail extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Subscription:sendBillingEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check to charge transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $listaClientes = [];
        $erros = [];
        try {
            $subscriptions = Subscription::where('status', Transactions::STATUS_WAITING)
                ->whereBetween('current_period_end', [date('Y-m-d', strtotime("-5 days")), date('Y-m-d', strtotime("+3 days"))])
                ->where('current_period_end', '!=', '')
                ->with('users')
                ->with(['transactions' => function ($query) {
                    $query->where('status', Transactions::STATUS_WAITING);
                }])
                ->get();

            foreach ($subscriptions as $subscription) {
                $dateVenc = new \DateTime($subscription->current_period_end);
                $dateCurrent = new \DateTime(date('Y-m-d'));
                $dateDiff = $dateCurrent->diff($dateVenc)->format('%a');

                $sinal = ($subscription->current_period_end > date('Y-m-d')) ? '-' : '';
                $dateDiff = $sinal . $dateDiff;

                $enviarEm = ['-3', '0', '2', '5'];
                if (in_array($dateDiff, $enviarEm)) {
                    $email = new EnviadorDeEmail($subscription, $subscription->transactions[0]);
                    $email->setTemplate('cobranca');
                    $email->executar();
                }

                $listaClientes[] = [
                    'id_user'    => $subscription->id_user, 
                    'cliente_id' => $subscription->cliente_id, 
                    'nome'       => $subscription->cliente_nome,
                    'enviouEmail'=> in_array($dateDiff, $enviarEm) ? 'Sim' : ' Não'
                ]; 
            }
        } catch(\Exception $e) {
            $erros[] = ['Erro no sistema ao rodar cron. '.$e->getMessage()];
        }        

        // ENVIANDO RELATÓRIO DA CRON
        $htmlListCliente  = "<br>---------------------------------------------------------------------- <br>";
        $htmlListCliente .= "id do sistema - id do cliente - nome do cliente - enviou email? <br>";
        $htmlListCliente .= "---------------------------------------------------------------------- <br>";
        foreach($listaClientes as $cliente) 
            $htmlListCliente .= $cliente['id_user']." - ".$cliente['cliente_id']." - ".$cliente['nome']." - ".$cliente['enviouEmail']." <br>";

        $htmlErrors = '';
        foreach($erros as $erro) 
            $htmlErrors .= $erro."<br>";

        $msg  = "<br>";
        $msg .= "DESCRIÇÃO DA CRON.:  <br>";
        $msg .= "DATA DA CRON.: ".date("d/m/Y h:i:s")."<br>";
        $msg .= "LISTA DE CLIENTES AFETADOS.: <br>".$htmlListCliente."<br>";
        $msg .= "ERROS.: <br> ".$htmlErrors."<br>";
        $msg .= "<br>";

        $user = User::where('id', User::JOBB)->first();
        $mail = PhpMailerConf::getSendScope($user);
        $mail->addBCC('atendimento@sistemajobb.com.br','Felipe santiago');

        $mail->addAddress('atendimento@sistemajobb.com.br','Felipe santiago');
        $mail->SetFrom('atendimento@sistemajobb.com.br','Felipe santiago');
        $mail->AddReplyTo('atendimento@sistemajobb.com.br','Felipe santiago');
        $mail->isHTML(true);
        $mail->Subject = 'UMA CRON ACABA DE SER EXECUTADA';
        $mail->Body    = $msg;
        $mail->send();
    }
}