<?php

namespace App\Model\Dao;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model
{
    
  protected $table = 'usuario';
  public $timestamps = false;
  protected $fillable = ['id', 'nome', 'usuarioGit'];

  public function listarTodos(){
    return User::get()->toArray();
  }

  public function atualizar($data){
    $usuario = User::find($data['id']);
    $usuario->nome = $data['nome'];
    $usuario->usuarioGit = $data['usuarioGit'];
    return $usuario->save();
  }

  public function cadastrar($data){
    return User::create([
      'id' => $data['id'],
      'nome' => $data['nome'],
      'usuarioGit' => $data['usuarioGit']
    ]);    
  }

  public function deletar($id){
    return User::where('id', $id)->delete();
  }

}
