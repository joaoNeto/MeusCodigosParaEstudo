<?php 

namespace App\Model\Factory;
use App\Model\Pojo\Phone;
use App\Model\Pojo\Address;
use App\Model\Pojo\Customer;
class CustomerFactory{	
	
	public static function create(array $data){
		try{
			$address = new Address(Address::getPostAddress($data));
			$phone = new Phone(Phone::getPostPhone($data));
			$customer = new Customer(Customer::getPostCustomer($data), $address, $phone);
			return $customer;
		}
		catch(\Exception $ex){
            throw new \Exception($ex);
		}
	}
}
