<?php
namespace App\Model;
use App\Model\EmailService\TemplateEmail;
use GuzzleHttp\Client;
use Log;
class HttpService
{
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    // create
    public function post($endpoit,$dados){

        $response = $this->client->post($endpoit, [
            'body' => $dados
        ]);

        $statusCode = $response->getStatusCode();
        $content = $response->getBody()->getContents();

        return ['statusCode'=>$statusCode,'content'=>$content];
    }

    //update
    public function put($endpoit,$dados){
        $endpoitAndKey = $endpoit.'?api_key='.Constantes::API_KEY_TESTE;
        $response = $this->client->request('PUT', $endpoitAndKey , [
            'http_errors' => false,
            'json' =>  $dados,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);
        
        $statusCode = (int) $response->getStatusCode() ;
        $contents = $response->getBody()->getContents();
        
        if ($statusCode != 200){

            Log::info('Endpoint: '.$endpoit.' status: ' . $statusCode . ' em ' . \Carbon\Carbon::now());
            Log::info('Error: '.$endpoit.' error: '. json_encode(json_decode($contents,true),JSON_PRETTY_PRINT));
            Log::info('Dados: '. json_encode($dados,JSON_PRETTY_PRINT));

            $errorData =[
                'endpoint'=>$endpoit,
                'statuscode'=>$statusCode,
                'error'=>json_decode($contents),
                'json_dados'=>json_encode($dados),
                'assunto'=>'Payment Error'
            ];

            $Template =   TemplateEmail::enviarErrorNotificacao($errorData);
            $emailError = new EmailNotificaoError($Template);
            //$emailError->executar();
            
            abort($statusCode,$contents);
        }
        return ['contents'=>$contents,'statusCode'=>$statusCode ];
    }

    public function get($endpoit){

        $endpoitAndKey = $endpoit.'?api_key='.Constantes::API_KEY_TESTE;
        $response = $this->client->request('GET', $endpoitAndKey , [
            'http_errors' => false,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);

        $statusCode = (int) $response->getStatusCode() ;
        $contents = $response->getBody()->getContents();
        if ($response->getStatusCode() != 200) {
            Log::info('Error: '.$endpoit.' status: ' . $response->getStatusCode() . ' em ' . \Carbon\Carbon::now());
            Log::info('Error: '.$endpoit.' error: '. $response->getBody());
        }

        return ['contents'=>$contents,'statusCode'=>$statusCode ];
    }

}