<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Dao\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    public function __construct(){}

    public function get(Request $request){   
        $usuarioModel = new User();
        return response()->json(['msg' => 'Lista de usuários', 'data' => $usuarioModel->listarTodos() ]);
    }
    public function post(Request $request){   
        $usuarioModel = new User();
        $data = $request->only('id', 'nome', 'usuarioGit');
        return response()->json(['type' => 'post', 'data' => $usuarioModel->cadastrar($data)]);
    }

    public function put(Request $request){   
        $usuarioModel = new User();
        $data = $request->only('id', 'nome', 'usuarioGit');
        return response()->json(['msg' => 'Alterar dados usuário', 'data' => $usuarioModel->atualizar($data)]);
    }

    public function delete(Request $request, $id){   
        $usuarioModel = new User();
        return response()->json(['msg' => 'Deletar o usuário', 'data' => $usuarioModel->deletar($id) ]);
    }
}