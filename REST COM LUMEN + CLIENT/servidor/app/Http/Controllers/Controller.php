<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function __construct()
    {
 	   if($_SERVER['REQUEST_METHOD'] == "OPTIONS") $this->responseAsJson(200, ['msg' => 'REQUEST METHOD NÃO PERMITIDO']);
    }
}
