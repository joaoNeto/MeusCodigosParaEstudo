<?php

$app->get('/usuario', 'UsuarioController@get');
$app->post('/usuario', 'UsuarioController@post');
$app->put('/usuario', 'UsuarioController@put');
$app->delete('/usuario/{id}', 'UsuarioController@delete');

$app->options('/usuario', function(){
	return response()->json(['msg' => 'request method nao permitido']);
});
