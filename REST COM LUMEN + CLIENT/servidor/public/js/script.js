'use strict';

$(document).ready(function() {
    var loadCache = $('#loading');
    var cardContainerCache = $('#card_container');
    var resultMesageCache = $('#result_mesage');
    var isDataReceived = false;
    // let waitTime = 1000;

    function errorHandler(err) {
        cardContainerCache.show();
        loadCache.hide();

        try {
            if(typeof err.msg_sem_sub !== 'undefined'){
                errorMsgConcat = err.msg_sem_sub;
            }else{
                err.replace(/\"/g, "\"");
                var result = JSON.parse(err)
                var errorMsgConcat = '';

                if (testJSON(result)) {
                    result = JSON.parse(result);
                } else {
                    errorMsgConcat = err;
                }

                if (result.hasOwnProperty('errors')) {
                    for (var objIndex in result.errors) {
                        errorMsgConcat += ' - ' + result.errors[objIndex].message + '<br/>';
                    }
                }

                if (result.hasOwnProperty('erros')) {
                    for (var parameterName in result.erros) {
                        errorMsgConcat += ' - ' + result.erros[parameterName] + '<br/>';
                    }
                }
            }
            
            if (errorMsgConcat !== '') {
                errorHelper(errorMsgConcat, 'card-wrapper');
            }
            
        } catch (err) {
            errorHelper(err.message, 'card-wrapper');
        }
    }

    $('#send-sem-sub').on('click', function(e) {
        e.preventDefault();
        cardContainerCache.hide();
        loadCache.show();
        $('.msg-modal-card-error').remove();
                
        var card = {} 
        card.card_holder_name = $("#payment_form #holder_name").val()
        card.card_expiration_date = $("#payment_form #card_expiration_date").val()
        card.card_number = $("#payment_form #card_number").val()
        card.card_cvv = $("#payment_form #card_cvv").val()

        // pega os erros de validação nos campos do form e a bandeira do cartão
        var cardValidations = pagarme.validate({card: card})

        //Mas caso esteja tudo certo, você pode seguir o fluxo
        pagarme.client.connect({ encryption_key: 'ek_test_5k0cdhlasTJe2FhDMKxkWOPlSkvYcK' })
        .then(client => client.security.encrypt(card))
        .then(card_hash => {
                // o próximo passo aqui é enviar o card_hash para seu servidor, e
            // em seguida criar a transação/assinatura

                var inputs = $('#payment_form').serialize();
                var xhr = $.ajax({
                    type: "POST",
                    url: BASE_URL_API + 'v1/payment/create-transaction-card-sem-sub',
                    data: {inputs, card_hash},
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Authorization", 'Bearer ' + TOKEN);
                        xhr.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded');
                    }

                }).done(function(data) {
                    isDataReceived = true;
                    loadCache.find('img').hide();
                    if (data.msg == 'success') {
                        if (data.status == 'paid') {
                            resultMesageCache.html('<h3 style="color:green">Seu pedido foi realizado com sucesso.</h3> <p>Entre em contato com nossa equipe para tratar a respeito.</p>');
                        } else if(data.status == 'processing') {
                            resultMesageCache.html('<h3 style="color:green">Seu pedido está sendo processado.</h3> <p>Entre em contato com nossa equipe para tratar a respeito.</p>');
                        } else {
                            resultMesageCache.html('<h3 style="color:red">Pagamento não realizado</h3> <p>Caso persista, entre em contato conosco.</p>');
                        }
                    } else {
                        try{
                            var error = JSON.parse(JSON.parse(data.status))
                            console.log(data)
                            console.log(error);
                            resultMesageCache.html('<h3 style="color:red">Pagamento não realizado</h3> <p>Caso persista, entre em contato conosco.</p><p>ERRO: ' + error.errors[0].message + '</p>');
                        }catch(err){
                            resultMesageCache.html('<h3 style="color:red">Pagamento não realizado</h3> <p>Um erro ocorreu no sistema! entre em contato conosco</p>');
                        }
                    }
                    
                    resultMesageCache.show();
                }).fail(function(err) {
                    errorHandler({msg_sem_sub: 'Erro: pagamento não realizado'});
                });
            })
    });

    $('#send').on('click', function(e) {
        e.preventDefault();
        cardContainerCache.hide();
        loadCache.show();
        $('.msg-modal-card-error').remove();

        var inputus = $('#form_payment').serialize();
        var xhr = $.ajax({
            type: "POST",
            url: BASE_URL_API + 'v1/payment/subscription-create',
            data: inputus,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", 'Bearer ' + TOKEN);
            }

        }).done(function(data) {
            isDataReceived = true;
            loadCache.find('img').hide();
            if (data.msg == 'success') {
                if (data.status == 'paid') {
                    resultMesageCache.html('<h3 style="color:green">Sua assinatura foi paga com sucesso.</h3> <p>Enviamos um e-mail com mais informações</p>');
                    setTimeout(function () {
                        parent.location.href = URL_REDIRECT;
                    }, 2000);
                } else {
                    resultMesageCache.html('<h3 style="color:red">Pagamento não realizado</h3> <p>Caso persista, entre em contato conosco.</p>');
                }
            } else {
                try{
                    var error = JSON.parse(JSON.parse(data.status))
                    console.log(data)
                    console.log(error);
                    resultMesageCache.html('<h3 style="color:red">Pagamento não realizado</h3> <p>Caso persista, entre em contato conosco.</p><p>ERRO: ' + error.errors[0].message + '</p>');
                }catch(err){
                    resultMesageCache.html('<h3 style="color:red">Pagamento não realizado</h3> <p>Um erro ocorreu no sistema! entre em contato conosco</p>');
                }
            }
            resultMesageCache.show();
        }).fail(function(err) {
            errorHandler(err.responseText);
        });

    });

    function testJSON(text) {
        try {
            JSON.parse(text);
            return true;
        } catch (error) {
            return false;
        }
    }

    function errorHelper(value, target) {
        $(`<div class="msg-modal-card-error alert alert-danger" style="margin-top:5px;">${value}</div>`).clone().insertAfter("#" + target);
    }

    function expirationDateFormat(data) {
        console.log(data);
    }

    $("#card_expiration_date").blur(function() {
        $(this).val($(this).val().replace(/[\s,.,\/,-]+/g, ''));
        // alert("This input field has lost its focus.");
        //return data.replace(/[\s,.,\/,-]+/g, '');
    });
});;