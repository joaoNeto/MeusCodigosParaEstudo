# API de Pagamentos Unity Brasil

## Consultar pagamentos
/v1/payment/list [GET]
- user
- cliente
- period_from [yyyy-mm-dd]
- period_to [yyyy-mm-dd]
- status ['pending_payment', 'processing', 'authorized', 'paid', 'unpaid', 'expired', 'substituted', 'refunded', 'refused', 'canceled']


## Alterar vencimento/valor da assinatura/obs/emitir_notafiscal
/v1/subscription/change [POST]
- id_user
- cliente_id
- current_period_end [yyyy-mm-dd]
- amount [99.99]
- obs [text]
- emitir_notafiscal [0,1]
- qtd_unidade [int]
- email [String]


## Alterar obs da transação
/v1/transaction/change [POST]
- transaction_id
- obs [text]


## Cancelar transação 
/v1/transaction/cancel [POST]
- transaction_id