<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNfeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('id_cliente');
            $table->string('nome_cliente', 150);
            $table->string('transaction_id', 100);
            $table->decimal('valor', 12,2);
            $table->string('link');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nfe');

        Schema::table('nfe', function($table) {
            $table->dropColumn('transaction_id');
        });
    }
}
