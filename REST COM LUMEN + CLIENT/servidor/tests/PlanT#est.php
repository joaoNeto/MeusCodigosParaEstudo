<?php
use Illuminate\Http\Response as HttpResponse;
use App\Model\Pojo\Plan;
use App\Model\PaymentPlan;
class PlanTest extends TestCase
{
	public function testPlanCreteSuccess()
	{
		try{
			$data = $this->postData();
			$pojo = Plan::getInstance($data);
			$paymentPlan = new PaymentPlan($pojo);
			$response = $paymentPlan->create();
			
			$resultData = json_decode($response['contents'],true);
			$this->assertEquals(200,$response['statusCode']);
			
			$this->assertArrayHasKey('object', $resultData);
			$this->assertNotEmpty($resultData['id']);
			$this->assertEquals( $pojo->getName(), $resultData['name']);
			$this->assertEquals( $pojo->getAmount(), $resultData['amount']);
			$this->assertEquals( $pojo->getCharges(), $resultData['charges']);

		}catch(\Exception $ex){
			dd ($ex->getMessage());
		}
	}
	
	public function _testPlanCreteError()
	{
		try{
			$data = $this->postDataError();
			$pojo = Plan::getInstance($data);
			$paymentPlan = new PaymentPlan($pojo);
			$response = $paymentPlan->create();
			$resultData = json_decode($response['contents'],true);
			$this->assertEquals(400,$response['statusCode']);
			$this->assertArrayHasKey('errors', $resultData);
		}catch(\Exception $ex){
			dd ($ex->getMessage());
		}
	}
	
	private function postData(){
		return [
            'days'=>null,
            'name'=>'Plano Click Gold',
            'charges'=> '2',
            'installments'=>'12',
            'amount'=>10000
        ];
	}
	private function postDataError(){
		return [
            'days'=>null,
            'name'=>null,
            'charges'=> null,
            'installments'=>null,
            'amount'=>3000
        ];
	}
}
