<?php
use Illuminate\Http\Response as HttpResponse;
use App\Model\Pojo\Customer;
use App\Model\PaymentCustomer;
use Log as log;
class CustomerTest extends TestCase
{
	public function testCustomerCreteSuccess()
	{
		$data = $this->postData();
		$pojo = Customer::getInstance($data);
		$paymentCustomer = new PaymentCustomer($pojo);
		$response = $paymentCustomer->create();
		$resultData = json_decode($response['contents'],true);
		
		$this->assertEquals(200,$response['statusCode']);
		$this->assertArrayHasKey('object', $resultData);
		$this->assertNotEmpty($resultData['id']);
		$this->assertEquals( 'customer', $resultData['object']);
   
        log::info('ID-Costomer: '.$resultData['id']);//salvar no bnanco referencia
	}
	
	public function _testCustomerCreteError()
	{
		$data = $this->postDataError();
		$pojo = Customer::getInstance($data);
		$paymentCard = new PaymentCustomer($pojo);
		$response = $paymentCard->create();
		$resultData = json_decode($response['contents'],true);
		$this->assertEquals(400,$response['statusCode']);
		$this->assertArrayHasKey('errors', $resultData);
	
	}
	
	private function postData(){
		return [
			'document_number'=>'64956428570',
			'name'=>'Glauberthy Costumer Filho',
			'email'=>'name@dominio.com',
			'born_at'=>13071976,
			'gender'=>'M',
			'address'=>[
				'street'=>'rua qualquer',
				'complementary'=>'apto',
				'street_number'=>'51',
				'neighborhood'=>'pinheiros',
				'city'=>'sao paulo',
				'state'=>'SP',
				'zipcode'=>'05444040',
				'country'=>'Brasil'
			],
			'phone'=>[
				'ddi'=>55,
				'ddd'=>81,
				'number'=>33382424
			]
		];
	}
	private function postDataError(){
		return [
			'document_number'=>'64956428570',
			'name'=>'Glauberthy Costumer',
			'email'=>'name@dominio.com',
			'born_at'=>13071976,
			'gender'=>'M',
			'address'=>[
				
			],
			'phone'=>[
				
			]
		];
	}
}
