<?php
use Illuminate\Http\Response as HttpResponse;
use App\Model\Pojo\Card;
use App\Model\PaymentCard;
use Log as log;
class CardTest extends TestCase
{
	public function testCardCreteSuccess()
	    {
		$data = $this->postData();
		$pojo = Card::getInstance($data);
		$paymentCard = new PaymentCard($pojo);
		$response = $paymentCard->create();
		$resultData = json_decode($response['contents'],true);

		$this->assertEquals(200,$response['statusCode']);
		$this->assertArrayHasKey('object', $resultData);
		$this->assertEquals( 'card', $resultData['object']);
        $this->assertArrayHasKey('id',$resultData);
		$this->assertEquals( $pojo->getHolderName(), $resultData['holder_name']);
        $this->assertEquals( $pojo->getCardExpirationDate(), $resultData['expiration_date']);
        log::info('ID-Card: '.$resultData['id']);//salvar no bnanco referencia
		
	}
	
	public function _testCardCreteError()
	    {
		$data = $this->postDataError();
		$pojo = Card::getInstance($data);
		$paymentCard = new PaymentCard($pojo);
		$response = $paymentCard->create();
		$resultData = json_decode($response['contents'],true);
		$this->assertEquals(400,$response['statusCode']);
		$this->assertArrayHasKey('errors', $resultData);
	}
	
	private function postData(){
		return [
			'card_number'=>'5316021056294188',
			'holder_name'=>'Glauberthy C',
			'card_expiration_date'=> '0618',
			'card_cvv'=> '0618',
			'customer'=>'175224'
		];
	}
	private function postDataError(){
		return [
			'days'=>'30',
			'name'=>'PlanGold',
			'charges'=> '2',
			'installments'=>'12',
		];
	}
}
