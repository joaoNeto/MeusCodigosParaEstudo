<?php
use Illuminate\Http\Response as HttpResponse;
use App\Model\Pojo\Card;
use App\Model\PaymentCard;
use App\Model\SubscriptionBuilder;
use App\Model\Pojo\Plan;
use App\Model\Pojo\Customer;
use App\Model\Pojo\Address;
use App\Model\Pojo\Phone;
use Log as log;

class SubscriptionBuilderTest extends TestCase
{
    public function testBuilderSubscription()
    {
        $system = 'M';
        $clienteId = '102030';
        try{
            
            $builder = new SubscriptionBuilder();
            $builder
            ->toCustomer(new Customer($this->postCustomer()))
            ->withSystemId($system)
            ->withCustomerId($clienteId)
            ->withCard(new Card($this->postDataCard()))
            ->withPlan(new Plan($this->postPlan()))
            ->withAddres(new Address($this->postAddress()))
            ->withPhone(new Phone($this->postPhone()))
            ->build();
            
        }catch(\Exception $ex){
            dd ($ex->getMessage());
        }
    }
    
    private function postDataCard(){
        return [
        'card_number'=>'349635879173220',
        'holder_name'=>'G Silva',
        'card_expiration_date'=> '0518',
        'card_cvv'=> '210'
        ];
    }
    private function postPlan(){
        return [
        'days'=>'30',
        'name'=>'Teste plano Gold',
        'charges'=> null,
        'installments'=>null,
        'amount'=>17000,
        'paymentMethod'=>'boleto'
        ];
    }
    
    private function postCustomer(){
        return [
        'document_number'=>'92590156006',
        'name'=>'Gilberto Pedro',
        'email'=>'glauberthy@gmail.com',
        'born_at'=>13071976,
        'gender'=>'M'
        ];
    }
    
    private function postAddress(){
        return [
        'street'=>'rua itaimbé',
        'complementary'=>'casa A',
        'street_number'=>'51',
        'neighborhood'=>'Ipsep',
        'city'=>'Recife',
        'state'=>'PE',
        'zipcode'=>'51350030',
        'country'=>'Brasil'
        ];
    }

    private function postPhone(){
        return [
        'ddi'=>'55',
        'ddd'=>'81',
        'number'=>'971191307'
        ];
    }
}