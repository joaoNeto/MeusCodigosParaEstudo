<?php
use Illuminate\Http\Response as HttpResponse;
use App\Model\Pojo\Subscription;
use App\Model\Dao\Subscription as subsDAO;
use App\Model\PaymentSubscription;
use App\Model\Pojo\Card;
use App\Model\Pojo\Plan;
use App\Model\PaymentCard;
use App\Model\PaymentPlan;
use Log as log;

class SubscriptionTest extends TestCase
{
	public function testSubscriptionCreteSuccess()
	{
		
		try{

			$dataCard = $this->postDataCard();
			$dataPlan = $this->postPlan();

			$card = Card::getInstance($dataCard);
			$plan = Plan::getInstance($dataPlan);
			
			//card
			$paymentCard = new PaymentCard($card);
			$cardResponse = $paymentCard->create();
			$cardResult = json_decode($cardResponse['contents'],true);
			
			$cardResult['statusCode'] = $cardResponse['statusCode'];
			$this->assertsCartao($cardResult,$card);
			
			//Plan
			$paymentPlan = new PaymentPlan($plan);
			$planResponse = $paymentPlan->create();
			$planResult = json_decode($planResponse['contents'],true);
			
			$planResult['statusCode'] = $planResponse['statusCode'];
			$this->assertsPlan($planResult,$plan);
			
			// Subsccription
			$dataSubscription = $this->postSubscription();
		
			$dataSubscription['card_id'] = $cardResult['id'];
			$dataSubscription['plan_id'] = $planResult['id'];
			
			$subscription = Subscription::getInstance($dataSubscription);
			$paymentSubscription= new PaymentSubscription($subscription);

			$subscriptionResponse = $paymentSubscription->create();
			$subscriptionResult = json_decode($subscriptionResponse['contents'],true);
			
			log::info('ObjectSubscription: '.$subscriptionResponse['contents']);//salvar no bnanco referencia

			$subscriptionResult['statusCode'] = $subscriptionResponse['statusCode'];
			
			$subscriptionResult['id_user'] = $dataSubscription['id_user'];
			$subscriptionResult['cliente_id'] = $dataSubscription['cliente_id'];
			
			subsDAO::subscriptionSave($subscriptionResult); 

			$this->assertsSubscription($subscriptionResult);
			
			log::info('phone-number: '.$subscriptionResult['phone']['number']);//salvar no bnanco referencia

		}catch(\Exception $ex){
			dd ($ex->getMessage());
		}
	}
	
	public function _testSubscriptionCreteError(){
		$data = $this->postDataError();
		$pojo = Subscription::getInstance($data);
		$paymentSubscription = new PaymentSubscription($pojo);
		$response = $paymentSubscription->create();
		$resultData = json_decode($response['contents'],true);
		$this->assertEquals(400,$response['statusCode']);
		$this->assertArrayHasKey('errors', $resultData);
	}
	//Card
	private function postDataCard(){
		return [
			'card_number'=>'341347908159570',
			'holder_name'=>'Felipe C',
			'card_expiration_date'=> '1017',
			'card_cvv'=> '649'
		];
	}
	//Plan
	private function postPlan(){
		return [
            'days'=>'30',
            'name'=>'Nome do plano atual',
            'charges'=> null,
            'installments'=>null,
            'amount'=>700
        ];
	}
	// Subscriptio
	private function postSubscription(){
		return [
			'id_user'=>'M',
			'cliente_id'=>'102030',
			'customer'=>[
				'document_number'=>'88158838430',
				'name'=>'Felipe Cavalcanti',
				'email'=>'glauberthy@gmail.com',
				'born_at'=>13071976,
				'gender'=>'M',
				'address'=>[
					'street'=>'rua itaimbé',
					'complementary'=>'casa A',
					'street_number'=>'51',
					'neighborhood'=>'Ipsep',
					'city'=>'Recife',
					'state'=>'PE',
					'zipcode'=>'51350030',
					'country'=>'Brasil'
				],
				'phone'=>[
					'ddi'=>'55',
					'ddd'=>'81',
					'number'=>'971191307'
				]
			],
			'postback_url'=>'https://payment.sistemajobb.com.br/v1/payment/subscription-post-back-url'
		];
	}
	
	private function postSubscriptionBoleto(){
		return [
			'plan_id'=>'151569',
			'customer'=>[
				'document_number'=>'64956428570',
				'name'=>'Glauberthy Boleto3 15/04',
				'email'=>'name@dominio.com',
				'born_at'=>13071976,
				'gender'=>'M',
				'address'=>[
					'street'=>'rua qualquer',
					'complementary'=>'apto',
					'street_number'=>'51',
					'neighborhood'=>'pinheiros',
					'city'=>'sao paulo',
					'state'=>'SP',
					'zipcode'=>'05444040',
					'country'=>'Brasil'
				],
				'phone'=>[
					'ddi'=>'55',
					'ddd'=>'81',
					'number'=>'33382424'
				]
			],
			'payment_method'=>'boleto',
			'postback_url'=>''
		];
	}
	private function postSubscriptionError(){
		return [
			'plan_id'=>'151569',
			'card_id'=>'card_cj1i6fq3z06v3616ecq09fcl8',
			'customer'=>[
				'document_number'=>'64956428570',
				'name'=>'Matheus PC',
				// 'email'=>'name@dominio.com',
				'born_at'=>13071976,
				'gender'=>'M',
				'address'=>[
					'street'=>'rua qualquer',
					'complementary'=>'apto',
					'street_number'=>'51',
					'neighborhood'=>'pinheiros',
					'city'=>'sao paulo',
					'state'=>'SP',
					'zipcode'=>'05444040',
					'country'=>'Brasil'
				],
				'phone'=>[
					'ddi'=>'55',
					'ddd'=>'81',
					'number'=>'33382424'
				]
			],

			'postback_url'=>''
		];
	}
	
	/* 
		Asserts
	*/
	//cartao 

	private function assertsCartao($cardResult,$card){
		$this->assertEquals(200,$cardResult['statusCode']);
		$this->assertArrayHasKey('object', $cardResult);
		$this->assertEquals( 'card', $cardResult['object']);
        $this->assertArrayHasKey('id',$cardResult);
		$this->assertEquals( $card->getHolderName(), $cardResult['holder_name']);
        $this->assertEquals( $card->getCardExpirationDate(), $cardResult['expiration_date']);
	}
	private function assertsPlan($planResult,$pojo){
		$this->assertEquals(200,$planResult['statusCode']);
		$this->assertArrayHasKey('object', $planResult);
		$this->assertNotEmpty($planResult['id']);
		$this->assertEquals( $pojo->getName(), $planResult['name']);
		$this->assertEquals( $pojo->getAmount(), $planResult['amount']);
		$this->assertEquals( $pojo->getCharges(), $planResult['charges']);
	}
	private function assertsSubscription($resultData){
		$this->assertEquals(200,$resultData['statusCode']);
		$this->assertArrayHasKey('object', $resultData);
		$this->assertEquals( 'subscription', $resultData['object']);
        $this->assertArrayHasKey('id',$resultData);
	}

}
