<!DOCTYPE html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Payment</title>
		<meta name="description" content="">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/meu-plano.css">
		<script src="https://assets.pagar.me/js/pagarme.min.js"></script>
		<script src="js/jquery-1.8.3.min.js"></script>
		<script src="js/jquery.mask.js"></script>
		<script src="js/jquery_validate1-11.js"></script>
		
		<script src="js/script.js"></script>
	</head>
	<body>
	<div class="container">
		<div id="modal-dados-cartao" class="" tabindex="-1" role="dialog" aria-labelledby="labelPersonalizados" aria-hidden="true" style="z-index: 999999; width: 700px; height: auto;">
			<div class="modal-header">
				<h4 id="labelUsuariosAtivos">Dados do Cartão de Crédito</h4>
			</div>
			<div class="modal-body">
				<div class="alert alert-info">
					<strong> <i class="icon-lock"></i> Segurança:</strong> O Meets não armazena, em hipótese alguma, os dados de seu cartão. As informações são enviadas diretamente para a operadora através de conexão segura.
				</div>
				<form id="form_dados_cartao" method>
				
					<div id="cartoes" class="bandeira" style="padding-bottom: 10px;">
						<div class="visa">
							<input type="radio" id="visa" name="bandeira_cartao" value="visa">	
							<label for="visa" title="Visa">visa</label>
						</div>
						<div class="credcard">
							<input type="radio" id="mastercard" name="bandeira_cartao" value="mastercard">	
							<label for="mastercard" title="MasterCard">mastercard</label>	
						</div>
						<div class="hiper">
							<input type="radio" id="hiper" name="bandeira_cartao" value="hiper">	
							<label for="hiper" title="Hiper">hiper</label>	
						</div>
						<div class="elo">
							<input type="radio" id="elo" name="bandeira_cartao" value="elo">	
							<label for="elo" title="elo">elo</label>	
						</div>
						<div class="dinners">
							<input type="radio" id="dinners" name="bandeira_cartao" value="dinners">	
							<label for="dinners" title="Dinners">dinners</label>	
						</div>
						<div class="amex">
							<input type="radio" id="amex" name="bandeira_cartao" value="amex">	
							<label for="amex" title="amex">amex</label>	
						</div>
					</div>
					
					<div class="row ml0">
						<div class="span3">
							<label for="card_number">Número</label>
							<input type="text" id="card_number" name="card_number" value="4024007145255187" placeholder="0000.0000.0000.0000">
						</div>
						<div class="span3">
							<label for="card_holder_name">Nome do titular</label>
							<input id="card_holder_name" type="text" name="card_holder_name" value="Teste" placeholder="Como escrito no cartão">
						</div>
						<div class="span3">
							<label for="card_expiration_month">Mês</label>
							<input type="text" id="card_expiration_month" name="card_expiration_month" value="6"  placeholder="Informe o mês">
						</div>
						<div class="span3">
							<label for="card_expiration_month">Ano</label>
							<input type="text" id="card_expiration_year" name="card_expiration_year" value="23" placeholder="Informe o Ano">
						</div>
						<div class="span3">
							<label for="card_cvv">Código de segurança</label>
							<input type="text" id="card_cvv" name="card_cvv" value="941" placeholder="informe o CVV">
						</div>
					</div>
					<br/><br/>
										
					<figure class="highlight">
						<pre>
							<code class="language-html"> 
								<div id="field_errors"></div>
							</code>
						</pre>
					</figure>
					<br/><br/>
					
					<div class="row ml0">
						
						<div class="span3">
							<label for="cnpj_cpf_cartao">CPF</label>
							<input id="cnpj_cpf_cartao" type="text" name="cnpj_cpf_cartao" placeholder="000.000.000-00">
						</div>
						<div class="span3">
							<label for="data_nasc_cartao">Data de nascimento</label>
							<input id="data_nasc_cartao" type="text" name="data_nasc_cartao" placeholder="DD/MM/YYYY">
						</div>
						<div class="span3">
							<label for="telefone_cartao">Telefone</label>
							<input id="telefone_cartao" type="text" name="telefone_cartao" placeholder="(00)0000-0000">
						</div>
					</div>
				
					<div class="row ml0" id="tr_cobranca" style="display:none;">
						<div class="span3">
							<label for="cep_cobranca_cartao">CEP</label>
							<input id="cep_cobranca_cartao" type="text" name="cep_cobranca_cartao">
						</div>
						<div class="span6">
							<label for="logradouro_cobranca_cartao">Logradouro</label>
							<input id="logradouro_cobranca_cartao" type="text" name="logradouro_cobranca_cartao">
						</div>
						<div class="span3">
							<label for="numero_cobranca_cartao">Número</label>
							<input id="numero_cobranca_cartao" type="text" name="numero_cobranca_cartao">
						</div>
						<div class="span3 alpha">
							<label for="complemento_cobranca_cartao">Complemento</label>
							<input id="complemento_cobranca_cartao" type="text" name="complemento_cobranca_cartao">
						</div>
						<div class="span3">
							<label for="bairro_cobranca_cartao">Bairro</label>
							<input id="bairro_cobranca_cartao" type="text" name="bairro_cobranca_cartao">
						</div>
						<div class="span3">
							<label for="cidade_cobranca_cartao">Cidade</label>
							<input id="cidade_cobranca_cartao" type="text" name="cidade_cobranca_cartao">
						</div>
						<div class="span3">
							<label for="uf_cobranca_cartao">UF</label>
							<select id="uf_cobranca_cartao" name="uf_cobranca_cartao"></select>
						</div>
					</div>
				 <input type="submit" value="Enviar dados" />
				</form>
			</div>
			
		</div>
	</div>
	</body>
</html>