<!DOCTYPE html>
<html>
<head>
    <title>Payment Card</title>
    <meta name="viewport" content="initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="<?php echo env('BASE_URL_API')?>js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo env('BASE_URL_API')?>js/card.js?v=1"></script>
    <script src="<?php echo env('BASE_URL_API')?>js/script.js?v=8"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo env('BASE_URL_API')?>css/card.css">
    <script><?php echo sprintf('var URL_REDIRECT = "%s";', $redirect_url); ?><?php echo sprintf('var BASE_URL_API = "%s";', env('BASE_URL_API')); ?><?php echo sprintf('var TOKEN = "%s";', $token); ?></script>
</head>
<body>
    <style>
        #loading{
            display:none;text-align: center;margin-top: 30%;
        }
         #result_mesage{
            display:none;
        }
    </style>
    <div class="card-container" id="card_container">
        <div class="card-wrapper" id="card-wrapper"></div>

        <div class="form-container active">
            <form action="" id="form_payment">
        <input type="hidden" name="id_user" id="id_user" value="<?php echo $data->id_user;?>" />
        <input type="hidden" name="id" id="id" value="<?php echo $data->id;?>" />
        <!-- plan -->
        <input type="hidden" name="plan_days" id="plan_days" value="<?php echo $data->plan_days;?>" />
        <input type="hidden" name="plan_name" id="plan_name" value="<?php echo $data->plan_name;?>" />
        <input type="hidden" name="plan_amount" id="plan_amount" value="<?php echo number_format((float)$data->plan_amount*100., 0, '.', '');?>" />
        <input type="hidden" name="plan_payment_method" id="plan_payment_method" value="<?php echo $data->plan_payment_method;?>" />
        
        <!-- customer -->
        
                <input type="hidden" name="cliente_id" id="cliente_id" value="<?php echo $data->cliente_id;?>" />
        <input type="hidden"  name="customer_document_number" id="customer_document_number" value="<?php echo $data->cliente_cpfcpj;?>"/>
        <input type="hidden"  name="customer_name" id="customer_name" value="<?php echo $data->cliente_nome;?>"/>
        <input type="hidden"  name="customer_email" id="customer_email" value="<?php echo $data->cliente_email;?>"/>
        <input type="hidden"  name="customer_born_at" id="customer_born_at" value="<?php echo $data->cliente_nascimento;?>"/>
        <input type="hidden"  name="customer_gender" id="customer_gender" value="<?php echo $data->cliente_sexo;?>"/>
        
                <!-- address -->
        
                <input type="hidden"  name="address_street" id="address_street" value="<?php echo $data->cliente_end_logradouro;?>"/>
        <input type="hidden"  name="address_street_number" id="address_street_number" value="<?php echo $data->cliente_end_numero;?>"/>
        <input type="hidden"  name="address_complementary" id="address_complementary" value="<?php echo $data->cliente_end_complemento;?>"/>
        <input type="hidden"  name="address_neighborhood" id="address_neighborhood" value="<?php echo $data->cliente_end_bairro;?>"/>
        <input type="hidden"  name="address_city" id="address_city" value="<?php echo $data->cliente_end_cidade;?>"/>
        <input type="hidden"  name="address_state" id="address_state" value="<?php echo $data->cliente_end_uf;?>"/>
        <input type="hidden"  name="address_country" id="address_country" value="<?php echo $data->cliente_end_pais;?>"/>
        <input type="hidden"  name="address_zipcode" id="address_zipcode" value="<?php echo $data->cliente_end_cep;?>"/>
        
                <!-- phone -->
        <input type="hidden"  name="phone_ddi" id="phone_ddi" value="<?php echo $data->cliente_tel_ddi;?>"/>
        <input type="hidden"  name="phone_ddd" id="phone_ddd" value="<?php echo $data->cliente_tel_ddd;?>"/>
        <input type="hidden"  name="phone_number" id="phone_number" value="<?php echo $data->cliente_tel_numero;?>"/>

                <div class="container-fluid">
                    
                    <div class="form-group">
                        <label for="card_number" class="col-sm-2 control-label">Número do Cartão</label>
                        <div class="col-sm-10">
                            <input placeholder="Número do Cartão" value="" class="form-control" id="card_number" type="tel" name="card_number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="holder_name" class="col-sm-2 control-label">Nome Completo</label>
                        <div class="col-sm-10">
                            <input placeholder="Nome Completo" value="" class="form-control" id="holder_name" type="text" name="holder_name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="width: 45%;float: left;">
                            <div class="form-group">
                                <label for="card_expiration_date" class="col-sm-2 control-label">Data de Expiração</label>
                                <div class="col-sm-10">    
                                    <input placeholder="MM/YY"  value="" class="form-control"  id="card_expiration_date" type="tel" name="card_expiration_date">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="width: 45%;float: right;">
                            <div class="form-group">
                                <label for="card_cvv" class="col-sm-2 control-label">CVV</label>
                                <div class="col-sm-10">    
                                    <input placeholder="000" value="" class="form-control" id="card_cvv" type="number" name="card_cvv">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <p class="small col-sm-10 text-info">Nosso serviço não armazena nenhuma informação de seu cartão de crédito. Os dados são enviados automaticamente para a operadora através de conexão segura.</p>
                
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input type="submit" class="btn btn-primary" value="Enviar"  id="send"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
     <div id="loading">
        <img src="<?php echo env('BASE_URL_API')?>img/loading.gif" />
        <div id="result_mesage"></div>
     </div>
    <script>
        new Card({
            form: document.querySelector('form'),
            container: '.card-wrapper'
        });
    </script>
</body>
</html>
