<?php

return [

    'locale' => env('APP_LOCALE', 'pt_br'),
    'fallback_locale' => env('APP_FALLBACK_LOCALE', 'en'),
];
