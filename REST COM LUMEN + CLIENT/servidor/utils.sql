-- RETORNA ASSINATURAS MOSTRANDO ÚLTIMO PAGAMENTO (VALOR E DATA)

SELECT s.id, s.payment_type, s.cliente_id, s.cliente_nome, s.cliente_email, s.plan_amount, s.plan_amount_user, s.plan_days, s.current_period_end,
(SUBSTRING_INDEX(GROUP_CONCAT(t.amount ORDER BY t.created_at DESC), ',', 1)) as last_payment_value,
(SUBSTRING_INDEX(GROUP_CONCAT(t.created_at ORDER BY t.created_at DESC), ',', 1)) as last_payment_date
FROM subscription as s
LEFT JOIN transactions as t ON t.cliente_id = s.cliente_id
WHERE t.status = 'paid'
AND s.payment_type = 'sign'
AND s.status != 'canceled'
GROUP BY s.id
ORDER BY s.id ASC