# TRABALHO DO PROJETO DE WEB DA UNIBRATEC
ADS - Prof.: Chickout

EQUIPE.: João Jose de Sousa Neto, Tatiana Emilia da Silva Moreno e Anderson Praxedes

## INSTALAÇÃO

Para rodar o projeto instale o banco de dados projeto_web.sql

## MODO DE USO

Para utilizar o projeto inicie o servidor PHP na porta 8080 na pasta raiz do projeto, quando voce startar o projeto esses serão os links para acessar o client e o servidor.:

<b>http://localhost:8080/cliente/</b> Link do cliente
<b>http://localhost:8080/servidor/public/</b> Link do servidor