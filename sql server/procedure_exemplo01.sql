CREATE OR REPLACE PROCEDURE PROC_MOTIVO_DEVOLUCAO ( op         IN CHAR,
                                                    id_motivo  IN NUMBER,
                                                    descricao  IN VARCHAR2,
                                                    tipo_nota  IN CHAR
                                                  ) IS
/*******************************************************************************
  NOME         : PRO_MOTIVO_DEVOLUCAO_E_TRANSFERENCIA
  PROGRAMADOR  : João Neto -  12/04/2017
  DESCRICAO    : CRUD do motivo de devolução e transferencia
*******************************************************************************/
  
  CREATE SEQUENCE dbamdata.teste_joao
   START WITH     0
   INCREMENT BY   1
   MINVALUE 0
   MAXVALUE 999
   NOCACHE
   NOCYCLE;
   
  ALTER SEQUENCE dbamdata.teste_joao INCREMENT BY 20;
  select max(T701_MOTIVO_IU) from t701_motivo_dev_trans;
  
  vEXCEPTION EXCEPTION;
  
BEGIN

  IF (op = 'c') THEN
    INSERT INTO t701_motivo_dev_trans (T701_MOTIVO_IU,T701_DESCRICAO,T701_TIPO_NOTA) 
    VALUES (dbamdata.teste_joao.nextval,descricao,tipo_nota);
    COMMIT;
  ELSE
  
  IF (op = 'r') THEN   
    SELECT T701_MOTIVO_IU, T701_DESCRICAO, T701_TIPO_NOTA 
    FROM t701_motivo_dev_trans;
    COMMIT;
  ELSE
  
  IF (op = 'u') THEN
    UPDATE t701_motivo_dev_trans 
    SET T701_MOTIVO_IU = id_motivo, T701_DESCRICAO = descricao, T701_TIPO_NOTA = tipo_nota
    WHERE T701_MOTIVO_IU = id_motivo;
    COMMIT;
  ELSE
  
  IF (op = 'd') THEN
    DELETE FROM t701_motivo_dev_trans where T701_MOTIVO_IU = id_motivo;
    COMMIT;
  ELSE
    RAISE vEXCEPTION;
  
  END IF;
  END IF;
  END IF;
  END IF;
  
  EXCEPTION
    WHEN vEXCEPTION THEN
      RAISE_APPLICATION_ERROR(-20999,'ATENÇÃO! Operação diferente de I, D, A.', FALSE);  
  
END;