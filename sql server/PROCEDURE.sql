--------------------------------------------------------
--  Arquivo criado - Ter�a-feira-Abril-25-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure PROC_MOTIVO_DEVOLUCAO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "FARMA"."PROC_MOTIVO_DEVOLUCAO" (
  PI_OPERACAO             IN VARCHAR2,
  PI_ID_MOTIVO            IN NUMBER,
  PI_DESCRICAO            IN VARCHAR2,
  PI_TIPO_NOTA            IN CHAR,
  PO_STATUS               OUT VARCHAR2
) IS


/*******************************************************************************
  NOME         : PROC_MOTIVO_DEVOLUCAO_E_TRANSFERENCIA
  PROGRAMADOR  : Jo�o Neto -  12/04/2017
  DESCRICAO    : CRUD do motivo de devolu��o e transferencia
*******************************************************************************/
  
  ecode NUMBER(10);
  emesg VARCHAR2(2000);
   
   /*CREATE SEQUENCE dbamdata.sequencia
   START WITH     0
   INCREMENT BY   1
   MINVALUE 0
   MAXVALUE 999
   NOCACHE
   NOCYCLE;
   
  ALTER SEQUENCE dbamdata.sequencia INCREMENT BY 1;
  select max(T701_MOTIVO_IU) from t701_motivo_dev_trans;
  
  vEXCEPTION EXCEPTION;*/

  
BEGIN

     PO_STATUS := 'PR';

    /*------------------- CADASTRAR ------------------------*/
    IF PI_OPERACAO = 'c' THEN
      BEGIN
          
          INSERT INTO DBAMDATA.T701_MOTIVO_DEV_TRANS
          (T701_MOTIVO_IU,T701_DESCRICAO,T701_TIPO_NOTA)
          VALUES
          (PI_ID_MOTIVO,PI_DESCRICAO,PI_TIPO_NOTA);
        
        EXCEPTION
        WHEN OTHERS THEN 
        ROLLBACK;
          ecode     := SQLCODE;
          emesg     := SUBSTR(SQLERRM,1, 2048);
          PO_STATUS := 'ER';
          -- PO_MSG    := SUBSTR(ecode ||'-'|| emesg, 1, 2048);
      END;
    END IF;

    /*------------------- ATUALIZAR ------------------------*/
    IF PI_OPERACAO = 'u' THEN
      BEGIN
        
        UPDATE DBAMDATA.T701_MOTIVO_DEV_TRANS
        SET
          T701_DESCRICAO = PI_DESCRICAO
        WHERE
          T701_MOTIVO_IU = PI_ID_MOTIVO;
        
        EXCEPTION
        WHEN OTHERS THEN 
        ROLLBACK;
          ecode     := SQLCODE;
          emesg     := SUBSTR(SQLERRM,1, 2048);
          PO_STATUS := 'ER';
          -- PO_MSG    := SUBSTR(ecode ||'-'|| emesg, 1, 2048);
      END;
    END IF;

    /*--------------------- EXCLUIR ------------------------*/
    IF PI_OPERACAO = 'd' THEN
      BEGIN
        
        DELETE FROM DBAMDATA.T701_MOTIVO_DEV_TRANS
        WHERE
          T701_MOTIVO_IU = PI_ID_MOTIVO ;
          
        EXCEPTION
        WHEN OTHERS THEN 
        ROLLBACK;
          ecode     := SQLCODE;
          emesg     := SUBSTR(SQLERRM,1, 2048);
          PO_STATUS := 'ER';
          -- PO_MSG    := SUBSTR(ecode ||'-'|| emesg, 1, 2048);
      END;
    END IF;

END;

/
