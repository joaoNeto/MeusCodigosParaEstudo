/*------------------------------------------------------------------*/
/*----------------------------- VIEW -------------------------------*/
/*------------------------------------------------------------------*/

/*  O QUE É?

	Uma view (visão) é uma instrução sql para definir uma exibição de uma
	ou mais tabelas relacionadas entre si, isto evita que na sua aplicação
	voce chame várias tabelas para executar o comando desejado. A view não 
	contém dados propriamente ditos, ela não está alocada fisicamente no 
	banco de dados.

*/


/*	  CRIANDO UMA VIEW 		*/

-- ESTRUTURA SIMPLES
	CREATE VIEW NOME_DA_VIEW AS
		INSTRUCAO_SQL

-- CRIANDO UMA VIEW SIMPLES
	CREATE VIEW NOME_DA_VIEW AS 
	   SELECT COLUNA1, COLUNA2*12 APELIDO_COLUNA2
	   FROM TABELA 
	   WHERE COLUNA1 = 20;

-- CRIANDO UMA VIEW COM 2 TABELAS
	CREATE VIEW NOME_DA_VIEW AS
	   SELECT T1.COLUNA1, T2.COLUNA1, T2.COLUNA2, T2.COLUNA3
	   FROM TABELA1 T1, TABELA2 T2
	   WHERE T1.COLUNA1 = T2.COLUNA3;



/*	 INSERINDO NUMA VIEW 	*/

INSERT INTO NOME_DA_VIEW VALUES(VALOR1,VALOR2)

/*	  CHAMANDO UMA VIEW 	*/

SELECT * FROM NOME_DA_VIEW;

/*	  DELETANDO UMA VIEW 	*/

DROP VIEW NOME_DA_VIEW