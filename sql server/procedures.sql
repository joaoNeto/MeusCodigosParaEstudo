/*------------------------------------------------------------------*/
/*-------------------------  PROCEDURES ----------------------------*/
/*------------------------------------------------------------------*/

/*  O QUE É?

	Uma procedure ou procedimento é um grupo de instruções SQL
	que formam uma unidade lógica e executam uma tarefa específica 
	e são usados ​​para encapsular um conjunto de operações ou 
	consultas a serem executadas em um servidor de banco de dados.
	A procedure fica armazenada fisicamente no banco de dados e 
	diferente da trigger ela é chamada atraves de comando sql.


*/


/*	CRIANDO UMA PROCEDURE 	*/

CREATE OR REPLACE PROCEDURE "NOME DO SCHEMA"."NOME DA PROCEDURE"(
	VARIAVEL1 IN VARCHAR2(100),
	VARIAVEL2 IN NUMBER,
	VARIAVEL3 IN CHAR,
	VARIAVEL4 OUT VARCHAR(100)
) IS

BEGIN

	-- BLOCO DE COMANDOS SQL

	COMMIT;
END;
/

/*	EXPLICANDO O CÓDIGO

	-- CREATE OR REPLACE PROCEDURE "NOME DO SCHEMA"."NOME DA PROCEDURE"

		O código cria ou subsitui a procedure criada referenciando o
		nome do usuário (schema) e o nome da procedure.

	-- CRIANDO VARIÁVEIS 

		Em toda procedure voce pode ou não declarar variaveis que serão
		executadas dentro do bloco de comandos sql. ao declarar as 
		variaveis voce deve espesificar se ela é de entrada ou saída
		e logo apos declarar o tipo dela.

	-- IN E OUT 
		
		As variáveis de entrada (IN) são aquelas declaradas	na hora 
		de chamar a procedure passando dados de entrada. As variáveis
		de saída (OUT) são aquelas que voce terá o retorno da procedure.

		+---------+     IN      +-----------+
		|         | ----------> |           |
		| SISTEMA |     OUT     | PROCEDURE |
		|         | <---------  |           |
		+---------+             +-----------+


	-- BEGIN END

		Entre estes comandos voce deve inserir o seu código ou bloco de 
		instrução que serão processados na execussão da procedure.

	-- COMMIT

		O commit executa todo o código ou bloco de código sql 


*/


/*	CHAMANDO UMA PROCEDURE 	*/

	-- EXECUTANDO NA IDE SQL SERVER ORACLE
	DECLARE
		P_ID_CARTAO number;
	BEGIN
		CALL "NOME DO SCHEMA"."NOME DA PROCEDURE"(1, P_ID_CARTAO);
												--  (IN, OUT)	
	END;

/*	DELETANDO UMA PROCEDURE 	*/

	DROP PROCEDURE "NOME DO SCHEMA"."NOME DA PROCEDURE";