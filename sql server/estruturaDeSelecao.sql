﻿SELECT 'V' V_CONTEUDO,
		T003_CGC, 
		T014_CGC_CPF, 
		T128_QTDE_VENDIDA, 
		T128_VALOR_LIQUIDO, 
        T123_NOTA_FISCAL_IU, 
		T123_DATA_EMISSAO_IU, 
		T123_FLAG_CANCELADA, 
		T123_NATUREZA_OPERACAO_E, 
        T014_PESSOA_FISJUR, 
		T014_RAZAO_SOCIAL, 
		T014_CEP_E, 
		T123_UNIDADE_IE, 
		T123_SERIE_IU, 
        NVL(T123_VENDEDOR_E,78) T123_VENDEDOR_E, 
		T009_NOME,  
		T123_TIPO_NOTAFISC_IU, 
        T128_PRODUTO_IE, 
		T082_REFERENC_FABRICA, 
		T076_PRODUTO_REFERENCIA, 
		T019_CGC,
        case when T123_FLAG_CANCELADA = 'S' THEN 
          'C' 
          ELSE 
            case when T123_TIPO_NOTAFISC_IU = 'B' THEN
              'V'
            ELSE
              case when T123_TIPO_NOTAFISC_IU = 'D' THEN
                'D'
              ELSE
                case when T123_TIPO_NOTAFISC_IU = 'C' THEN
                  'B'
              ELSE
                'V'
         END msTipoTrans
FROM T123_SAIDA,                                    
      T128_SAIDA_ITENS,                              
      T076_PRODUTO,                                  
      T082_PRODUTO_FORNECEDOR,                       
      T014_CLIENTE,                                  
      T019_FORNECEDOR,                               
      T009_SETOR,                                    
      T003_UNIDADE                                   
WHERE T123_UNIDADE_IE          = T128_UNIDADE_IE       
   AND T123_NOTA_FISCAL_IU     = T128_NOTA_FISCAL_IE   
   AND T123_SERIE_IU           = T128_SERIE_IE         
   AND T123_TIPO_NOTAFISC_IU   = T128_TIPO_NOTAFISC_IE 
   AND T123_DATA_EMISSAO_IU    = T128_DATA_EMISSAO_IE  
   AND T076_PRODUTO_IU         = T128_PRODUTO_IE       
   AND T082_UNIDADE_IE         = T123_UNIDADE_IE       
   AND T082_PRODUTO_IE         = T128_PRODUTO_IE       
   AND T082_FORNECEDOR_IE      = T076_INDUSTRIA_E      
   AND T019_FORNECEDOR_IU      = T082_FORNECEDOR_IE    
   AND T014_CLIENTE_IU         = T123_CLIENTE_E        
   AND NVL(T123_VENDEDOR_E,78) = T009_SETOR_IU(+)      
   AND T003_UNIDADE_IU         = T123_UNIDADE_IE       
   AND (                                               
        ( T123_TIPO_NOTAFISC_IU IN ('A','C') AND T123_SISTEMA_E = 20 )   
         OR                                                                  
        ( T123_TIPO_NOTAFISC_IU IN ('B','E') AND T003_TIPO_UNIDADE = 2 ) 
      )                                                                      
   AND T076_INDUSTRIA_E        = ${sIndArq}
   AND T123_DATA_EMISSAO_IU >= '01/'||TO_CHAR(ADD_MONTHS(SYSDATE,-1),'MM')||'/'||TO_CHAR(ADD_MONTHS(SYSDATE,-1),'YY')||' 00:00:00'
   AND T123_DATA_EMISSAO_IU <= trunc(LAST_DAY(ADD_MONTHS(SYSDATE,-1)))
 ORDER BY T123_UNIDADE_IE, T123_SERIE_IU, T123_NOTA_FISCAL_IU, T123_DATA_EMISSAO_IU 