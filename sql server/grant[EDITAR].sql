/*------------------------------------------------------------------*/
/*---------------------------- GRANT -------------------------------*/
/*------------------------------------------------------------------*/

/*  O QUE É?

	Use a instrução GRANT para conceder privilégios a um usuário ou 
	função específico, ou a todos os usuários, para executar ações 
	em objetos de banco de dados. Você também pode usar a instrução 
	GRANT para conceder uma função a um usuário, a PUBLIC ou a outra 
	função.

*/


/*	  CRIANDO UMA GRANT 	*/

-- criando grant para funcoes ou procedures
	GRANT EXECUTE ON {FUNCTION | PROCEDURE} usuario_da_procedure TO usuario_desejado

-- criando grant para sequencias
	GRANT USAGE ON SEQUENCE [ schemaName. ] SQL92Identifier TO grantees

-- criando um grant para tabelas
	GRANT privilege-type ON [TABLE] { table-Name | view-Name } TO grantees

/*	  DELETANDO UMA GRANT 	*/
