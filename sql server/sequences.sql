/*----------------------------------------------------------------------*/
/*----------------------------- SEQUENCE -------------------------------*/
/*----------------------------------------------------------------------*/

/*  O QUE É?

	Uma sequence (sequencia) é utilizada quando uma aplicação necessita 
	utilizar valores numéricos seqüenciais em uma tabela faz-se o uso de
	sequence. Esses valores são gerados automaticamente pelo oracle.

*/


/*	  CRIANDO UMA SEQUENCE 		*/

-- ESTRUTURA BÁSICA
Create sequence nome_da_seqüência
	[increment by n]
	[start with n]
	[maxvalue n | nomaxvalue] or [minvalue n | nominvalue]
	[cycle | nocycle]
	[cache n | nocache];

-- EXEMPLO BASICO DE UMA SEQUENCE
create sequence idcodcli
	increment by 1
	start with 10
	maxvalue 999
	nocycle;



/* 	 EXPLICANDO A ESTRUTURA BÁSICA DA SEQUENCE 

nome_da_seqüência 	=> Nome da sequencia, não podendo ser o mesmo de uma tabela ou view
Increment by n 		=> Especifica de quanto será o incremento ou decremento. O padrão é 1
Start with n 		=> Especifica o primeiro número a ser gerado. O padrão é 1.
Maxvalue n 			=> Especifica o valor máximo que a seqüência gerada pode atingir. O padrão é nomaxvalue, indo até 1027
Minvalue n 			=> Especifica o valor mínimo para as seqüências que estiverem sendo decrementadas. É mutuamente exclusiva ao maxvalue.
Cycle | nocycle 	=> Indica que ao atingir o valor máximo a numeração continuará a partir do valor inicial. O default é nocycle.
Cache n | nocache 	=> Especifica quantos valores o Oracle pré-aloca e mantém em memória. O padrão é 20.

*/


/*	  CHAMANDO UMA SEQUENCE 	*/

-- CHAMANDO UMA SEQUENCE ATRAVES DE UM INSERT
	INSERT INTO CLIENTES(codcli,nome) VALUES (idcodcli.nextval,'nome teste pegando o proximo valor da sequence');
	INSERT INTO CLIENTES(codcli,nome) VALUES (idcodcli.currval,'nome teste pegando o valor atual da sequence');

/*

OBS.: 	Duas pseudocolunas são utilizadas nas sequences: nextval e currval.
		O nextval retorna o próximo número da seqüência.
		O Currval retorna o valor corrente 

*/

/*	  DELETANDO UMA SEQUENCE 	*/

DROP SEQUENCE NOME_DA_SEQUENCE