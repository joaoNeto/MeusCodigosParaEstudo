#!/usr/bin/python
# -*- coding: utf-8 -*-
from DAO.firstClass import FirstClass
from DAO.oraculo import Oraculo
from DAO.protheus import Protheus
from DAO.warMachine import WarMachine
from datetime import date, timedelta
from Services.sendMail import SendMail
import sys
import re

def get_date_params():
    if len(sys.argv) == 3:
        date_pattern = r"[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}"
        str_date_to = sys.argv[2]
        str_date_from = sys.argv[1]

        if len(re.findall(date_pattern, str_date_to)) == 0 and len(re.findall(date_pattern, str_date_from)) == 0:
            raise Exception("Erro de sintaxe, voce deve passar os parametros corretamente no formato YYYY-MM-DD, ex.: 2020-10-21.")

        list_date_to:list = str_date_to.split('-')
        list_date_from:list = str_date_from.split('-')

        date_to   = date(
            int(list_date_to[0]), 
            int(list_date_to[1]), 
            int(list_date_to[2])
        )

        date_from = date(
            int(list_date_from[0]),
            int(list_date_from[1]),
            int(list_date_from[2])
        )

        if date_from > date_to:
            raise Exception("A data de origem está com data maior do que a data final.")

    else:
        date_to   = date.today()
        date_from = date_to - timedelta(days=5)

    return (date_from, date_to)

def conciliation_of_values():
    list_disagreement  = oraculo.list_disagreements_first_class_war_machine()
    list_disagreement += oraculo.list_disagreements_war_machine_protheus()
    return list_disagreement


def conciliation_of_commission():
    return oraculo.list_disagreement_conciliation_war_machine_protheus()


# Description.: List approved orders from protheus, firstclass and warmachine.
def list_orders(list_id_disagreements):
    try:

        list_order_protheus = protheus.list_order({
            "date_from": f"{DATE_FROM:%Y%m%d}",
            "date_to": f"{DATE_TO:%Y%m%d}",
            "list_id_disagreements": list_id_disagreements
        })

        list_order_first_class = firstClass.list_order({
            "date_from": f"{DATE_FROM:%Y-%m-%d}",
            "date_to": f"{DATE_TO:%Y-%m-%d}",
            "list_id_disagreements": list_id_disagreements
        })

        list_order_war_machine = warMachine.list_order({
            "date_from": f"{DATE_FROM:%Y-%m-%d}",
            "date_to": f"{DATE_TO:%Y-%m-%d}",
            "list_id_disagreements": list_id_disagreements
        })

        oraculo.delete_in_table(oraculo.PEDIDOS_SELLERS_PROTHEUS)
        oraculo.insert_in_table(
            table=oraculo.PEDIDOS_SELLERS_PROTHEUS,
            data=list_order_protheus
        )

        oraculo.delete_in_table(oraculo.PEDIDOS_SELLERS_FIRST_CLASS)
        oraculo.insert_in_table(
            table=oraculo.PEDIDOS_SELLERS_FIRST_CLASS, 
            data=list_order_first_class
        )

        oraculo.delete_in_table(oraculo.PEDIDOS_SELLERS_WAR_MACHINE)
        oraculo.insert_in_table(
            table=oraculo.PEDIDOS_SELLERS_WAR_MACHINE, 
            data=list_order_war_machine
        )

    except Exception as e:
        raise Exception('Não conseguiu inserir pedidos.: ' + str(e))


def list_commissions(list_id_disagreements):
    try:
        oraculo.log('listando as comissoes')

        list_comission_protheus = protheus.list_comission({
            "date_from": f"{DATE_FROM:%Y%m%d}",
            "date_to": f"{DATE_TO:%Y%m%d}",
            "list_id_disagreements": list_id_disagreements
        })

        list_comission_war_machine = warMachine.list_comission({
            "list_id_disagreements": [comission['PEDIDO_FILHO'] for comission in list_comission_protheus]
        })

        oraculo.delete_in_table(oraculo.COMISSAO_SELLERS_PROTHEUS)
        oraculo.insert_in_table(
            table=oraculo.COMISSAO_SELLERS_PROTHEUS, 
            data=list_comission_protheus
        )

        oraculo.delete_in_table(oraculo.COMISSAO_SELLERS_WAR_MACHINE)
        oraculo.insert_in_table(
            table=oraculo.COMISSAO_SELLERS_WAR_MACHINE,
            data=list_comission_war_machine
        )

    except Exception as e:
        raise Exception('Não conseguiu inserir comissao.: ' + str(e))


def insert_diverg_sellers_disagreement(disagreements:list = [], disagreement_comission:list = [], disagreement_transfer:list = []):
    oraculo.delete_in_table(oraculo.DIVERG_SELLERS)

    # START insert disagreement_comission at list_disagreement
    for comission in disagreement_comission:

        occurence_disagreement_comission = [
            disagreement for disagreement in disagreements
            if disagreement['PEDIDO_FILHO'] == comission['PEDIDO_FILHO'] 
                and disagreement['NOME_SISTEMA'] == 'protheus_warmachine'
        ]

        if len(occurence_disagreement_comission) > 0:
            disagreement = occurence_disagreement_comission[0]
            index_disagreement = disagreements.index(disagreement)

            disagreement['COMISSAO']    = 'error'
            disagreement['COMISSAO_WM'] = comission['COMISSAO_WM']
            disagreement['COMISSAO_PR'] = comission['COMISSAO_PR']

            disagreements[index_disagreement] = disagreement
        else: 
            disagreements.append(comission)
    # END insert disagreement_comission at list_disagreement

    oraculo.insert_in_table(
        table=oraculo.DIVERG_SELLERS,
        data=disagreements
    )


def list_transfer(list_latest_disagreements):
    pass


def conciliation_of_transfer():
    return []


def main():
    oraculo.log('INICIOU A CRON')
    try:

        # list_disagreement_sellers = oraculo.list_disagreement_sellers()
        # latest_disagreements = [pedido['numero_pedido'] for pedido in list_disagreement_sellers]
        latest_disagreements   = [] # ao utilizar, favor retirar o lacre

        list_orders(latest_disagreements)
        disagreement:list = conciliation_of_values()

        list_commissions(latest_disagreements)
        disagreement_comission:list = conciliation_of_commission()

        # list_transfer(latest_disagreements)
        # disagreement_transfer:list = conciliation_of_transfer()

        insert_diverg_sellers_disagreement(
            disagreements=disagreement,
            disagreement_comission=disagreement_comission,
            disagreement_transfer=[] # disagreement_transfer
        )

    except Exception as e:
        oraculo.log(
            descricao='erro no main.: ' + str(e), 
            tipo='erro'
        )

    oraculo.log('FIM DA CRON')

firstClass = FirstClass()
oraculo    = Oraculo()
protheus   = Protheus()
warMachine = WarMachine()

DATE_FROM, DATE_TO = get_date_params()

main()