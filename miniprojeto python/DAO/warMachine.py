from DAO.core.model import Model
from DAO.oraculo import Oraculo
import sys


class WarMachine(Model):


    def __init__(self):
        Model.__init__(self)
        self.oraculo = Oraculo()
        self.connection.set_connection(
            drive='',
            host='',
            port='',
            username='',
            password='',
            database=''
        )


    def list_order(self, search):
        try:
            self.oraculo.log('Listando os pedidos do WarMachine.', self)

            where = []
            if('date_from' in search and 'date_to' in search):
                where.append(" ped.data_criacao BETWEEN '"+search['date_from']+" 00:00:00' AND '"+search['date_to']+" 23:59:59' ")

            if('list_id_disagreements' in search and len(search['list_id_disagreements']) > 0 ):
                where.append(" ped.pedido_wd IN ("+( ', '.join([str(i) for i in search['list_id_disagreements']]))+") ")

            filter_disagreement = ''            
            if(len(where) > 0):
                strWhere   = ' OR '.join([str(i) for i in where])
                filter_disagreement += ' AND ('+strWhere+') '

            sql_string = f"""
                SELECT 
                    (ped.subtotal + ped.frete) VALORREP,
                    tl.descricao DESCRI,
                    ped.pedido_wd PEDIDO_FILHO,
                    ped.pedido_mm PEDIDO_PAI,
                    se.id_seller SELLER,
                    ped.subtotal VALORTOT,
                    ped.frete FRETE,
                    se.nome nome_seller,
                    ped.total total_pedido,
                    ped.status situacao
                FROM pedido ped
                    INNER JOIN seller se ON ped.id_seller = se.id_seller
                    LEFT JOIN lancamento_financeiro lf ON lf.id_pedido = ped.id_pedido
                    LEFT JOIN tipo_lancamento tl ON tl.id_tipo_lancamento = lf.id_tipo_lancamento
                WHERE ped.id_seller > 1
                    AND tl.descricao = 'Venda'
                    AND ped.datahora_aprovacao IS NOT NULL
                    {filter_disagreement}
                GROUP BY ped.pedido_wd
            """

            print(f"\n\n QUERY LISTAR PEDIDO WAR MACHINE \n {sql_string} \n\n")

            return self.select(sql_string)

        except Exception as e:
            self.oraculo.log(
                instancia=self, 
                descricao='Erro ao listar pedidos no WarMachine.: '+str(e), 
                tipo='erro'
            )
            sys.exit()


    def list_comission(self, search):
        try:
            self.oraculo.log('Listando comissão warmachine.', self)

            where = []
            if('date_from' in search and 'date_to' in search):
                where.append(" ped.data_criacao BETWEEN '"+search['date_from']+" 00:00:00' AND '"+search['date_to']+" 23:59:59' ")

            if('list_id_disagreements' in search and len(search['list_id_disagreements']) > 0 ):
                where.append(" ped.pedido_wd IN ("+( ', '.join([str(i) for i in search['list_id_disagreements']]))+") ")

            filter_disagreement = ''            
            if(len(where) > 0):
                strWhere   = ' OR '.join([str(i) for i in where])
                filter_disagreement += ' AND ('+strWhere+') '

            sql_string = f"""
                SELECT
                    (ped.subtotal + ped.frete) VALORREP,
                    tl.descricao DESCRI,
                    ped.pedido_wd PEDIDO_FILHO,
                    ped.pedido_mm PEDIDO_PAI,
                    se.id_seller SELLER,
                    ped.subtotal VALORTOT,
                    ped.frete FRETE,
                    se.nome nome_seller,
                    ped.total total_pedido,
                    ped.status situacao,
                    ped.id_pedido,
                    lf.valor,
                    ped.comissao,
                    ped.datahora_aprovacao,
                    lf.data_pagamento,
                    ped.data_criacao
                FROM pedido ped
                    INNER JOIN seller se ON ped.id_seller = se.id_seller
                    INNER JOIN lancamento_financeiro lf ON lf.id_pedido = ped.id_pedido
                    INNER JOIN tipo_lancamento tl ON tl.id_tipo_lancamento = lf.id_tipo_lancamento
                WHERE ped.id_seller > 1
                    AND ped.datahora_aprovacao IS NOT NULL
                    {filter_disagreement}
            """
            # ped.comissao_original,
            # AND ped.status in (3, 4, 5)
            # AND lf.data_pagamento IS NOT NULL
            print(f"\n\n QUERY LISTAR COMISSAO WAR MACHINE \n {sql_string} \n\n")

            return self.select(sql_string)

        except Exception as e:
            self.oraculo.log(
                instancia=self, 
                descricao='Erro ao listar comissões do WarMachine.: '+str(e), 
                tipo='erro'
            )
            sys.exit()
