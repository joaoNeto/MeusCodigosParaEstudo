from DAO.core.connection import Connection
from datetime import date, timedelta
import sys


class Model():


    def __init__(self):
        self.connection = Connection()


    def select(self, sql):
        self.connection.open_connection()
        self.connection.cursorDB.execute(sql)
        if self.connection.drive == 'sqlserver':
            columns = [column[0] for column in self.connection.cursorDB.description]
            lista = [dict(zip(columns, row)) for row in self.connection.cursorDB.fetchall()]
        elif self.connection.drive == 'mysql':
            lista = self.connection.cursorDB.fetchall()

        return lista


    def insert(self, table, columns = [], data = None):        
        self.connection.open_connection()

        if isinstance(data, dict):
            list_data = [data]
        if isinstance(data, list):
            list_data = data

        if not isinstance(list_data[0], dict):
            raise Exception("os dados precisam ser um dicionário")

        data = None
        columns = []
        for key in list_data[0]:
            columns.append(key)
        
        params  = ', '.join(['%s'] * len(columns))
        columns = ', '.join(columns)
        list_data = [list(item.values()) for item in list_data]

        sql = f"INSERT INTO {table} ({columns}) VALUES ({params})"

        def split_list(lista, n):
            for i in range(0, len(lista), n):
                yield lista[i:i + n]
        
        new_list_splited = list(split_list(list_data, 5000))
        response = 1
        for list_data in new_list_splited:
            try:
                self.connection.cursorDB.executemany(sql, list_data)
                self.connection.conn_db.commit()
            except Exception as e: 
                # pode ser que algum registro esteja poluido e precisará de tratamento
                # print(str(e))
                response = 0
                self.connection.conn_db.rollback()            

        return response


    def delete(self, table, where = 'true'):
        self.connection.open_connection()
        try:
            sql = f'DELETE FROM {table} WHERE {where} '
            self.connection.cursorDB.execute(sql)
            self.connection.conn_db.commit()
            response = mycursor.rowcount > 0
        except:
            self.connection.conn_db.rollback()
            response = 0
        return response
