import mysql.connector
import pyodbc


class Connection():
    

    def open_connection_mysql(self):
        return mysql.connector.connect(
            host    = self.host,
            user    = self.username,
            passwd  = self.password,
            database= self.database
        )


    def open_connection_sql_server(self):
        return pyodbc.connect(
          r'DRIVER={ODBC Driver 17 for SQL Server};' +
          ('SERVER={server},{port};'   +
          'DATABASE={database};'      +
          'UID={username};'           +
          'PWD={password}').format( server= self.host, port= self.port, database= self.database, username= self.username, password= self.password))


    def open_connection(self):

        if(not hasattr(self, 'conn_db')):
            if self.drive == 'mysql':
                self.conn_db = self.open_connection_mysql()
                self.cursorDB = self.conn_db.cursor(dictionary=True)
            elif self.drive == 'sqlserver':
                self.conn_db = self.open_connection_sql_server()
                self.cursorDB = self.conn_db.cursor()
            else:
                print('nenhum drive selecionado')


    def set_connection(self, drive, host, port, username, password, database):
        self.drive = drive
        self.host  = host
        self.port  = port
        self.username = username
        self.password = password
        self.database = database
