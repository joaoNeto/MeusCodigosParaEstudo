from DAO.core.model import Model
import sys


class Oraculo(Model):


    DIVERG_SELLERS = 'diverg_sellers'
    PEDIDOS_SELLERS_PROTHEUS = 'pedidos_sellers_protheus'
    PEDIDOS_SELLERS_WAR_MACHINE = 'pedidos_sellers_war_machine'
    PEDIDOS_SELLERS_FIRST_CLASS = 'pedidos_sellers_first_class'
    COMISSAO_SELLERS_WAR_MACHINE = 'comissao_sellers_war_machine'
    COMISSAO_SELLERS_PROTHEUS = 'comissao_sellers_protheus'


    def __init__(self):
        Model.__init__(self)
        self.connection.set_connection(
            drive='',
            host='',
            port='',
            username='',
            password='',
            database=''
        )


    def insert_in_table(self, table, data):
        try:
            self.log(f'inserindo dados na tabela {table}', self)
            if(len(data) > 0):
                return self.insert(
                    table=table, 
                    data=data
                )
            else:
                return 1
        except Exception as e:
            self.log(
                instancia=self, 
                descricao=f'erro ao inserir dados na tabela {table} .: '+str(e), 
                tipo='erro'
            )
            sys.exit()


    def delete_in_table(self, table):
        try:
            self.log(f'Deletando registros da tabela {table}', self)
            return self.delete(table)
        except Exception as e:
            self.log(
                instancia=self, 
                descricao=f'Erro ao deletar registros da tabela {table}.: '+str(e), 
                tipo='erro'
            )
            sys.exit()


    def list_disagreement_sellers(self):
        try:
            self.log('Listando as inconsistencias dos sellers no oraculo.', self)
            return self.select("SELECT * FROM diverg_sellers")
        except Exception as e:
            self.log(
                instancia=self, 
                descricao='Erro ao listar as inconsistencias dos sellers no oraculo.: '+str(e), 
                tipo='erro'
            )
            sys.exit()


    def list_disagreements_first_class_war_machine(self):
        try:
            self.log('Listando as inconsistencias de venda no sistema first class e war machine', self)

            sql_string = """
                SELECT
                    fc.PEDIDO_FILHO PEDIDO_FILHO,
                    wm.PEDIDO_PAI PEDIDO_PAI,
                    'firstclass_warmachine' NOME_SISTEMA,
                    CASE
                        WHEN fc.VALORTOT != wm.VALORTOT THEN 'error'
                        WHEN fc.VALORTOT IS NULL THEN 'error'
                        WHEN wm.VALORTOT IS NULL THEN 'error'
                        ELSE 'ok'
                    END VALORTOT,
                    CASE
                        WHEN fc.FRETE != wm.FRETE THEN 'error'
                        WHEN fc.FRETE IS NULL THEN 'error'
                        WHEN wm.FRETE IS NULL THEN 'error'
                        ELSE 'ok'
                    END FRETE,
                    CASE
                        WHEN statusDictionary(wm.situacao, 'warmachine') != statusDictionary(fc.situacao, 'firstclass') THEN 'error'
                        ELSE 'ok'
                    END SITUACAO,
                    fc.VALORTOT VALORTOT_FC,
                    wm.VALORTOT VALORTOT_WM,
                    fc.FRETE FRETE_FC,
                    wm.FRETE FRETE_WM,
                    statusDictionary(wm.situacao, 'warmachine') SITUACAO_WM,
                    statusDictionary(fc.situacao, 'firstclass') SITUACAO_FC
                FROM pedidos_sellers_first_class fc
                INNER JOIN pedidos_sellers_war_machine wm ON wm.PEDIDO_FILHO = fc.PEDIDO_FILHO
                WHERE wm.DESCRI <> 'COMISSAO'
                HAVING VALORTOT = 'error'
                    OR FRETE = 'error'
            """

            return self.select(sql_string)

        except Exception as e:
            self.log(
                instancia=self, 
                descricao='Erro ao inserir inconsistencias do protheus.: '+str(e), 
                tipo='erro'
            )
            sys.exit()


    def list_disagreements_war_machine_protheus(self):
        try:
            self.log('Listando as inconsistencias de venda no war machine e protheus', self)
            # ADICIONAR UM WHERE PARA SELECIONAR APENAS OS QUE TEM ALGUM ERRO
            sql_string = """
                SELECT
                    pr.PEDIDO_FILHO PEDIDO_FILHO,
                    pr.PEDIDO_PAI PEDIDO_PAI,
                    'warmachine_protheus' NOME_SISTEMA,
                    CASE
                        WHEN pr.VALOR_PEDIDO != wm.VALORTOT THEN 'error'
                        WHEN pr.VALOR_PEDIDO IS NULL THEN 'error'
                        WHEN wm.VALORTOT IS NULL THEN 'error'
                        ELSE 'ok'
                    END VALORTOT,
                    CASE
                        WHEN pr.VALOR_FRETE != wm.FRETE THEN 'error'
                        WHEN pr.VALOR_FRETE IS NULL THEN 'error'
                        WHEN wm.FRETE IS NULL THEN 'error'
                        ELSE 'ok'
                    END FRETE,
                    CASE
                        WHEN statusDictionary(pr.SITUACAO, 'protheus') != statusDictionary(wm.situacao, 'warmachine') THEN 'error'
                        ELSE 'ok'
                    END SITUACAO,
                    pr.VALOR_PEDIDO VALORTOT_PR,
                    wm.VALORTOT VALORTOT_WM,
                    pr.VALOR_FRETE FRETE_PR,
                    wm.FRETE FRETE_WM,
                    statusDictionary(wm.situacao, 'warmachine') SITUACAO_WM,
                    statusDictionary(pr.SITUACAO, 'protheus') SITUACAO_PR
                FROM pedidos_sellers_war_machine wm
                INNER JOIN pedidos_sellers_protheus pr ON wm.PEDIDO_FILHO = pr.PEDIDO_FILHO
                WHERE wm.DESCRI <> 'COMISSAO'
                HAVING VALORTOT = 'error'
                    OR FRETE = 'error'
            """

            return self.select(sql_string)
        except Exception as e:
            self.log(
                instancia=self, 
                descricao='Erro ao inserir inconsistencias do war machine.: '+str(e), 
                tipo='erro'
            )
            sys.exit()

    def list_disagreement_conciliation_war_machine_protheus(self):
        try:
            self.log('Listando as inconsistencias de comissao no war machine e protheus', self)

            sql_string = """
                SELECT 
                    pr.PEDIDO_FILHO PEDIDO_FILHO,
                    pr.PEDIDO_PAI PEDIDO_PAI,
                    'warmachine_protheus' NOME_SISTEMA,
                    CASE
                        WHEN wm.valor_comissao != pr.valor_comissao THEN 'error'
                        WHEN pr.valor_comissao IS NULL THEN 'error'
                        WHEN wm.valor_comissao IS NULL THEN 'error'
                        ELSE 'ok'
                    END COMISSAO,
                    wm.valor_comissao comissao_war_machine,
                    pr.valor_comissao comissao_protheus    
                FROM comissao_sellers_war_machine_agrupado wm
                    INNER JOIN comissao_sellers_protheus_agrupado pr ON pr.PEDIDO_FILHO = wm.PEDIDO_FILHO
                HAVING COMISSAO = 'error'
            """

            return self.select(sql_string)
        except Exception as e:
            self.log(
                instancia=self, 
                descricao='Erro ao inserir inconsistencias do war machine.: '+str(e), 
                tipo='erro'
            )
            sys.exit()

    def log(self, descricao='', instancia = None, tipo = 'info'):

        if instancia is None:
            nomeClasse = ''
        else:
            nomeClasse = f"{instancia.__class__.__name__}.py"

        return self.insert(
            table='log', 
            data={
                "servico": "cron_seller",
                "descricao": descricao,
                "localOcorrencia": nomeClasse,
                "tipo": tipo,
            }
        )
