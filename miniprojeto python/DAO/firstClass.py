from DAO.core.model import Model
from DAO.oraculo import Oraculo
import sys
import logging


class FirstClass(Model):


    def __init__(self):
        Model.__init__(self)
        self.oraculo = Oraculo()
        self.connection.set_connection(
            drive='',
            host='',
            port='',
            username='',
            password='',
            database=''
        )


    def list_order(self, search):
        try:
            self.oraculo.log('Listando os pedidos do firstClass.', self)

            where = []
            if('date_from' in search and 'date_to' in search):
                where.append(f" ped_dta_compra between '{search['date_from']} 00:00:00' AND '{search['date_to']} 23:59:59' ")

            if('list_id_disagreements' in search and len(search['list_id_disagreements']) > 0 ):
                strIdsDisagreement = (', '.join([str(i) for i in search['list_id_disagreements']]))
                where.append(f" id_pedped IN ({strIdsDisagreement}) ")

            filter_disagreement = ''
            if(len(where) > 0):
                strWhere = ' OR '.join([str(i) for i in where])
                filter_disagreement = ' AND ('+strWhere+') '

            sql_string = f"""
                    SELECT
                        ped.ped_valor_total VALORREP,
                        '' DESCRI,
                        ped.id_pedped PEDIDO_FILHO, 
                        parent_pedped PEDIDO_PAI,  
                        ped.sellerid SELLER,
                        ped.ped_valor_subtotal VALORTOT,
                        ped.ped_valor_frete FRETE,
                        ped_dta_compra dta_compra,
                        ped_situacao situacao,
                        ped_valor_desconto valor_desconto,
                        ped_valor_valecompra valor_valecompra,
                        ped_valor_total_1 valor_total_1,
                        ped_valor_real_1 valor_real_1,
                        ped_valor_total_2 valor_total_2,
                        ped_valor_real_2 valor_real_2,
                        ped_cupom cupom,
                        ped.ped_valor_garantia
                    FROM
                        b2c_pedped ped 
                    WHERE ped.sellerid > 0
                        AND ped.parent_pedped <> 0
                        AND ped.ped_situacao in (3, 7)
                        {filter_disagreement}
                    GROUP BY ped.id_pedped
            """

            print(f"\n\n QUERY LISTAR PEDIDO FIRST CLASS \n {sql_string} \n\n")

            return self.select(sql_string)

        except Exception as e:
            self.oraculo.log(
                instancia=self, 
                descricao='Erro ao listar pedidos no firstclass.: '+str(e), 
                tipo='erro'
            )
            sys.exit()
