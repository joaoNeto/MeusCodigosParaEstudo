from DAO.core.model import Model
from DAO.oraculo import Oraculo
import sys


class Protheus(Model):


    def __init__(self):
        Model.__init__(self)
        self.oraculo = Oraculo()
        self.connection.set_connection(
            drive='',
            host='',
            port='',
            username='',
            password='',
            database=''
        )


    def list_order(self, search):
        try:
            self.oraculo.log('Listando os pedidos do Protheus.', self)

            where = []
            if('date_from' in search and 'date_to' in search):
                where.append(" Z0R.Z0R_DATA BETWEEN '"+search['date_from']+"' AND '"+search['date_to']+"' ")

            if('list_id_disagreements' in search and len(search['list_id_disagreements']) > 0 ):
                where.append("Z0R_NUM IN (Z"+( ', Z'.join([str(i) for i in search['list_id_disagreements']]))+")")

            # retirar pedidos filhos com estorno

            filter_disagreement = ''
            if(len(where) > 0):
                strWhere   = ' OR '.join([str(i) for i in where])
                filter_disagreement += ' AND ('+strWhere+') '

            sql_string = f"""
                SELECT
                    DISTINCT SUBSTRING(Z0R_NUM, 2,8) PEDIDO_FILHO,
                    Z0R_PEDPAI PEDIDO_PAI,
                    Z0R_SELLER SELLER,
                    ROUND(SE2.E2_VALOR - Z0R_FRETE, 2) VALOR_PEDIDO,		
                    ROUND(Z0R_FRETE, 2) VALOR_FRETE,
                    ROUND(SE2.E2_VALOR, 2) SUM_PEDIDO_FRETE,
                    CASE
                        WHEN SE2.E2_VALOR IS NULL THEN 7
                        ELSE 3
                    END AS SITUACAO
                FROM dbo.Z0R010 Z0R WITH(NOLOCK) 
                LEFT JOIN dbo.Z0S010 Z0S WITH(NOLOCK) ON Z0S_FILIAL = Z0R_FILIAL AND Z0S_NUM = Z0R_NUM AND Z0S.D_E_L_E_T_ <> '*'
                LEFT JOIN dbo.SE2010 SE2 WITH(NOLOCK) ON Z0R_RECSE2 <> ' ' AND SE2.R_E_C_N_O_ = Z0R_RECSE2 AND SE2.D_E_L_E_T_ <> '*'
                WHERE Z0R_FILIAL = Z0S_FILIAL
                AND Z0R_NUM = Z0S_NUM
                AND Z0R.D_E_L_E_T_ <> '*'
                {filter_disagreement}
                ORDER BY PEDIDO_FILHO
            """
            # AND Z0R.Z0R_DATA BETWEEN '20200201' AND '20200201'

            print(f"\n\n QUERY LISTAR PEDIDOS PROTHEUS \n {sql_string} \n\n")

            return self.select(sql_string)

        except Exception as e:
            self.oraculo.log(
                instancia=self, 
                descricao='Erro ao listar pedidos no Protheus.: '+str(e), 
                tipo='erro'
            )
            sys.exit()


    def list_canceled_order(self, date_from, date_to):
        sql_string = f"""
            SELECT
            Z1F.Z1F_PEDFIL
            FROM dbo.Z0R010 Z0R WITH(NOLOCK)
            INNER JOIN dbo.Z1F010 Z1F WITH(NOLOCK) ON Z1F.Z1F_FILIAL = Z0R_FILIAL AND Z1F.Z1F_PEDFIL = SubString(Z0R_NUM,2,8) AND Z1F.D_E_L_E_T_ <> '*'
            WHERE Z0R.D_E_L_E_T_ <> '*'
            AND Z0R.Z0R_DATA BETWEEN '{date_from}' AND '{date_to}'
        """
        return self.select(sql_string)


    def list_comission(self, search):
        try:
            self.oraculo.log('Listando comissão protheus.', self)

            where = []
            if('date_from' in search and 'date_to' in search):
                where.append(" Z0R.Z0R_DATA BETWEEN '"+search['date_from']+"' AND '"+search['date_to']+"' ")
                # search['list_id_canceled_order'] = self.list_canceled_order(search['date_from'], search['date_to'])
                # where.append(" Z1F.Z1F_PEDFIL NOT IN ("+( ', '.join([str(i) for i in search['list_id_canceled_order']]))+") ")

            if('list_id_disagreements' in search and len(search['list_id_disagreements']) > 0 ):
                where.append(" Z1F.Z1F_PEDFIL IN ("+( ', '.join([str(i) for i in search['list_id_disagreements']]))+") ")

            filter_disagreement = ''            
            if(len(where) > 0):
                strWhere   = ' OR '.join([str(i) for i in where])
                filter_disagreement += ' AND ('+strWhere+') '

            sql_string = f"""
                SELECT
                    Z1F.Z1F_DESCRI DESCRI, SUBSTRING(Z0R_NUM, 2,8) PEDIDO_FILHO,
                    Z0R_PEDPAI PEDIDO_PAI,
                    Z1F.Z1F_VALOR VALORREP,
                    Z0R_SELLER SELLER,
                    Z0R_FRETE FRETE
                FROM dbo.Z0R010 Z0R WITH(NOLOCK)
                    INNER JOIN dbo.Z1F010 Z1F WITH(NOLOCK) ON Z1F.Z1F_FILIAL = Z0R_FILIAL AND Z1F.Z1F_PEDFIL = SubString(Z0R_NUM,2,8) AND Z1F.D_E_L_E_T_ <> '*'
                    RIGHT JOIN dbo.Z0S010 Z0S WITH(NOLOCK) ON Z0S_FILIAL = Z0R_FILIAL AND Z0S_NUM = Z0R_NUM AND Z0S.D_E_L_E_T_ <> '*'
                    RIGHT JOIN dbo.SE2010 SE2 WITH(NOLOCK) ON Z0R_RECSE2 <> ' ' AND SE2.R_E_C_N_O_ = Z0R_RECSE2 AND SE2.D_E_L_E_T_ <> '*'
                WHERE Z0R_FILIAL = Z0S_FILIAL	
                    AND Z0R_NUM = Z0S_NUM
                    AND Z0R.D_E_L_E_T_ <> '*'
                    {filter_disagreement}
                GROUP BY Z1F.Z1F_DESCRI, Z0R_NUM, Z0R_PEDPAI, Z1F.Z1F_VALOR, Z0R_SELLER, Z0R_FRETE
            """

            print(f"\n\n QUERY LISTAR COMISSAO PROTHEUS \n {sql_string} \n\n")

            return self.select(sql_string)

        except Exception as e:
            self.oraculo.log(
                instancia=self, 
                descricao='Erro ao listar comissões do protheus.: '+str(e), 
                tipo='erro'
            )
            sys.exit()
