import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate


class SendMail():


    PASSWORD_KEY = ""
    USER_KEY = ""


    def __init__(self):
        self.server = smtplib.SMTP('smtp.gmail.com', 587)
        self.server.starttls()
        self.message = None
        self.authentication()


    def authentication(self):
        self.server.login(self.USER_KEY, self.PASSWORD_KEY)


    def body_message(self, title, text):
        self.message = f'Subject: {title}\n\n{text}'
        self.message = self.message.encode('utf-8')


    def send(self, to_addrs):
        self.server.sendmail(self.USER_KEY, to_addrs, self.message)


    def quit(self):
        self.server.quit()

    """
    SendMail().sendSimpleMessage(
        to="joao.neto@madeiramadeira.com.br",
        title="meu titulo",
        message="meu texto"
    )
    """
    def send_simple_mail(self, to, title, message):
        self.body_message(title, message)
        self.send(to)
        self.quit()

    """
    SendMail().send_mail_with_attachment(
        to="joao.neto@madeiramadeira.com.br",
        title="meu titulo com arquivo",
        message="meu texto com arquivo",
        files=['Files/report_pedidos_sellers_first_class_20200325.xlsx']
    )
    """
    def send_mail_with_attachment(self, to, title, message, files=None):
        msg = MIMEMultipart()
        msg['From'] = self.USER_KEY
        msg['To'] = COMMASPACE.join(to)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = title

        msg.attach(MIMEText(message))

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(fil.read(), Name=basename(f))

            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
            msg.attach(part)

        smtp = smtplib.SMTP('smtp.gmail.com', 587)
        smtp.starttls()
        smtp.login(self.USER_KEY, self.PASSWORD_KEY)
        smtp.sendmail(self.USER_KEY, to, msg.as_string())
        smtp.close()
