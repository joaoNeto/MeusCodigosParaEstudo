## Projeto auditoria sellers
# [Python - serviço de auditoria dos sellers]

<img src="https://upload.wikimedia.org/wikipedia/commons/1/1b/MadeiraMadeira_Logo.png" width="100%" height="100%">

Algoritmo que serve para mostrar as inconsistencias nas vendas dos sellers.

### Requerimentos

    python 3.6.2

### Equipe Responsável 

João Neto <joao.neto@madeiramadeira.com.br>

### Como instalar

Voce precisa instalar as dependencias que o projeto necessita, para isso voce precisa abrir o seu terminal na pasta do projeto e rodar o seguinte comando

    python3 -m venv venv
    source venv/bin/activate
    pip3 install -r requirements.txt

veja mais detalhes em https://pt.stackoverflow.com/questions/209384/instalar-com-pip-atrav%c3%a9s-do-arquivo-requirements-txt-dentro-do-virtualenv/209511#209511

### Dependencias

driver mysql 
driver sql server (odbc) [link para baixar o driver](https://docs.microsoft.com/pt-br/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?redirectedfrom=MSDN&view=sql-server-ver15) [link do problema de não instalar no stackoverflow](https://stackoverflow.com/questions/34785653/pyodbc-cant-open-the-driver-even-if-it-exists)

### Como executar

Precisa entrar no virtualenv, para isso va para a pasta raiz do projeto no terminal e digite o seguinte comando:

    . venv/bin/activate

Para rodar o projeto voce deve executar a página no terminal, veja o exemplo a seguir:

    python main.py

### Structure


    DAO/
        core/
            connection.py
            model.py
        First_class.py
        protheus.py
        War_machine.py
    main.py


#### connection.py

Classe para fazer conexão com o banco de dados

#### model.py

Classe de abstração para fazer o select

#### First_class.py, protheus.py e War_machine.py

Classes que vão fazer conexão com a bases de dados para consultar e inserir dados

#### main.py

arquivo principal que fará todo o processamento dos dados

### Fluxograma

1 - LISTA AS INCONSISTENCIAS JA EXISTENTES DOS SELLERS;
2 - DELETA OS PEDIDOS DO PROTHEUS, WARMACHINE E FIRSTCLASS QUE ESTÃO NA BASE DO ORACULO; 
3 - PROCURA OS PEDIDOS DA BASE DO PROTHEUS, WARMACHINE E FIRSTCLASS E INSERE DENTRO DA BASE DO ORACULO LEVANDO EM CONTA DOIS CRITÉRIOS;
    3.1 - BUSCAR OS PEDIDOS NO RANGE DE UMA SEMANA A PARTIR DE HOJE E 7 DIAS ATRÁS;
    3.2 - BUSCAR TAMBÉM OS PEDIDOS QUE ESTÃO NA TABELA DE INCONSISTENCIAS DOS SELLERS (PASSO 1);
4 - PROCURA INCONSISTENCIAS NOS DADOS DOS PEDIDOS DAS TABELAS PROTHEUS, WARMACHINE E FIRSTCLASS E INSERE AS INCONSISTENCIAS NA TABELA 'diverg_sellers';