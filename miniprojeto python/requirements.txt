et-xmlfile==1.0.1
jdcal==1.4.1
mysql-connector==2.2.9
numpy==1.18.2
openpyxl==3.0.3
pandas==1.0.3
# pkg-resources==0.0.0
pyodbc==4.0.30
python-dateutil==2.8.1
pytz==2019.3
six==1.14.0
