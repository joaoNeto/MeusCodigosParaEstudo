##################################
######		angularJS 		######
##################################

O angularJS é uma bibliotéca do javascript.

O AngularJS é distribuído como um arquivo JavaScript e pode ser adicionado a uma página da web com uma tag de script:

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>


####	PRIMEIROS CÓDIGOS AngularJS

	1- ng-app define um aplicativo AngularJS.
	2- ng-model vincula o valor dos controles HTML (input, select, textarea) aos dados do aplicativo.
	3- ng-bind vincula os dados do aplicativo à visualização HTML.

##	PRIMEIRO EXEMPLO DE CODIGO angularJS 

<!DOCTYPE html>
<html>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	<body>

		<div ng-app="">
		  <p>Name: <input type="text" ng-model="name"></p>
		  <p ng-bind="name"></p>
		</div>

	</body>
</html>

## 	EXPLICANDO O PRIMEIRO EXEMPLO  DE CÓDIGO angularJS

O AngularJS é iniciado automaticamente quando a página da web foi carregada.

ng-app diz ao AngularJS que o elemento <div> é o "proprietário" de um aplicativo AngularJS.
ng-model liga o valor do campo de entrada ao nome da variável do aplicativo.
ng-bind liga o innerHTML do elemento <p> ao nome da variável do aplicativo.

####	INICIANDO VARIAVEL COM angujarJS

ng-init inicializa as variáveis ​​de aplicação AngularJS.

## EXEMPLO DE CODIGO

<div ng-app="" ng-init="firstName='John'">
	<p>The name is <span ng-bind="firstName"></span></p>
</div>

## EXEMPLO DE CODIGO COM OBJETOS

<div ng-app="" ng-init="person={firstName:'John',lastName:'Doe'}">
	<p>The name is {{ person.lastName }}</p>
</div>

## EXEMPLO DE CODIGO COM ARRAYS

<div ng-app="" ng-init="points=[1,15,19,2,40]">
	D<p>The third result is {{ points[2] }}</p>
</div>



#### LOOP NO ANGULARJS

A ng-repeat diretiva repete um elemento HTML:

## EXEMPLO DE CODIGO UTILIZANDO ARRAY

<div ng-app="" ng-init="names=['Jani','Hege','Kai']">
  <ul>
    <li ng-repeat="x in names">
      {{ x }}
    </li>
  </ul>
</div>

## EXEMPLO DE CODIGO UTILIZANDO OBJETO

<div ng-app="" ng-init="names=[
{name:'Jani',country:'Norway'},
{name:'Hege',country:'Sweden'},
{name:'Kai',country:'Denmark'}]">

	<ul>
	  <li ng-repeat="x in names">
	    {{ x.name + ', ' + x.country }}
	  </li>
	</ul>

</div>


#### 	Compreendendo o escopo

Se considerarmos um pedido de AngularJS consistir em:

	1- Modelo, que são os dados disponíveis para a vista atual. (ng-model)
	2- View, que é o HTML.
	3- Controller, que é a função JavaScript que faz / muda / remove / controla os dados. (ng-controller)

#### 	MODULOS

Um módulo AngularJS define um aplicativo.

O módulo é um contêiner para as diferentes partes de um aplicativo.
O módulo é um contêiner para os controladores de aplicativo.
Os controladores sempre pertencem a um módulo.

Um módulo é criado usando a função AngularJS  -- angular.module -- 

## EXEMPLO DE CODIGO

<div ng-app="myApp">...</div>
<script>
	var app = angular.module("myApp", []); 
</script>

#### 	CONTROLADOR

Adicione um controlador ao seu aplicativo e consulte o controlador com a ng-controllerdiretiva:

## 	EXEMPLO DE CODIGO

<div ng-app="myApp" ng-controller="myCtrl">
	{{ firstName + " " + lastName }}
</div>

<script>
	var app = angular.module("myApp", []);
	app.controller("myCtrl", function($scope) {
	    $scope.firstName = "John";
	    $scope.lastName = "Doe";
	});
</script>


#### 	FILTROS COM ANGULARJS

O AngularJS fornece filtros para transformar dados:

	1- |currency| Formatar um número para um formato de moeda.
	2- !date| Formatar uma data para um formato especificado.
	3- |filter| Selecione um subconjunto de itens de uma matriz.
	4- |json| Formatar um objeto para uma seqüência de caracteres JSON.
	5- |limitTo| Limita um array / string, em um número especificado de elementos / caracteres.
	6- |lowercase| Formatar uma seqüência de caracteres para minúsculas.
	7- |uppercase| Formatar uma cadeia para maiúsculas.
	8- |number| Formate um número para uma string.
	9- |orderBy| Ordena uma matriz por uma expressão.

## EXEMPLO DE FILTRO COMO uppercase

<div ng-app="myApp" ng-controller="personCtrl">
	<p>The name is {{ lastName | uppercase }}</p>
</div>

OBS.: Filtros podem ser adicionados a expressões usando o caractere de pipe |, seguido por um filtro.

## EXEMPLO DE FILTRO UTILIZANDO orderBy

<div ng-app="myApp" ng-controller="namesCtrl">
	<ul>
	  <li ng-repeat="x in names | orderBy:'country'">
	    {{ x.name + ', ' + x.country }}
	  </li>
	</ul>
</div>

#### 	JSON

JSON: J ava S cript O bject N otação.
JSON é uma sintaxe para armazenar e trocar dados.
JSON é um texto escrito com a notação de objeto JavaScript.
JSON é um formato de intercâmbio de dados leve
JSON é "auto-descrevente" e fácil de entender
JSON é independente de linguagem 

OBS.: O tipo de arquivo para arquivos JSON é ".json".

## Trocando Dados

Ao trocar dados entre um navegador e um servidor, os dados só podem ser texto.
JSON é texto, e podemos converter qualquer objeto JavaScript em JSON, e enviar JSON para o servidor.
Também podemos converter qualquer JSON recebido do servidor em objetos JavaScript.
Desta forma, podemos trabalhar com os dados como objetos JavaScript, sem parsing complicado e traduções.

## EXEMPLO DE CONVERSÃO DE OBJETO JAVASCRIPT PARA OBJETO JSON

var myObj = { "name":"John", "age":31, "city":"New York" };
var myJSON = JSON.stringify(myObj);
window.location = "demo_json.php?x=" + myJSON;

## EXEMPLO DE CONVERSAO DE OBJETO JSON PARA OBJETO JAVASCRIPT

var myJSON = '{ "name":"John", "age":31, "city":"New York" }';
var myObj = JSON.parse(myJSON);
document.getElementById("demo").innerHTML = myObj.name;


## JSON VS XML

Tanto o JSON como o XML podem ser usados ​​para receber dados de um servidor web.

# JSON é como o XML, porque
	1- Ambos JSON e XML são "auto-descrevendo" (legível por humanos)
	2- Tanto o JSON como o XML são hierárquicos (valores dentro de valores)
	3- Tanto o JSON quanto o XML podem ser analisados ​​e usados ​​por muitas linguagens de programação
	4- Tanto o JSON quanto o XML podem ser obtidos com um XMLHttpRequest

# JSON é diferente de XML porque
	1- JSON não usa etiqueta de fim
	2- JSON é mais curto
	3- JSON é mais rápido de ler e escrever
	4- JSON pode usar matrizes

a maior diferença é que o  XML tem de ser analisado com um analisador XML. O JSON pode ser analisado por uma função JavaScript padrão.


## EXEMPLO DE CONVERSÃO DE OBJETO PHP PARA JSON

<?php

	$myObj->name 	= "John";
	$myObj->age 	= 30;
	$myObj->city 	= "New York";

	$myJSON 		= json_encode($myObj);
	echo $myJSON;

?>

## EXEMPLO DE CONVERSAO DE ARRAY PHP PARA JSON

<?php
	
	$myArr = array("John", "Mary", "Peter", "Sally");
	$myJSON = json_encode($myArr);
	
	echo $myJSON;

?>



#### 	XMLHttpRequest Object

O objeto XMLHttpRequest é um sonho de desenvolvedores , porque você pode:

	Atualizar uma página da Web sem recarregar a página
	Solicitar dados de um servidor - depois que a página foi carregada
	Receber dados de um servidor - depois que a página foi carregada
	Enviar dados para um servidor - em segundo plano


## EXEMPLO 

<div id="demo">
	<button type="button" onclick="loadXMLDoc()">Change Content</button>
</div>

<script>
	function loadXMLDoc() {
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	      document.getElementById("demo").innerHTML =
	      this.responseText;
	    }
	  };
	  xhttp.open("GET", "xmlhttp_info.txt", true);
	  xhttp.send();
	}
</script>


## EXPLICANDO

	1- xhttp.onreadystatechange = function() // cria um objeto XMLHttpRequest 
	2- xhttp.onreadystatechange = function() // função a ser executada sempre que o status do objeto XMLHttpRequest for alterado
	3- if (this.readyState == 4 && this.status == 200) // Quando a propriedade readyState é 4 ea propriedade status é 200, a resposta está pronta
	4- document.getElementById("demo").innerHTML = xhttp.responseText 	// A propriedade responseText retorna a resposta do servidor como uma seqüência de caracteres de texto.



#### 	angularJS SQL

AngularJS é perfeito para exibir dados de um banco de dados. Apenas certifique-se de que os dados estejam no formato JSON.

