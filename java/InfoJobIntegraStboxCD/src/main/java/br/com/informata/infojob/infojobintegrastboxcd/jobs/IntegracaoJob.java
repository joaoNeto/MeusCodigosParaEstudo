/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobintegrastboxcd.jobs;

import br.com.informata.infojob.infojobintegrastboxcd.IntegracaoStboxCdMain;
import br.com.informata.infojob.infojobintegrastboxcd.infra.ConstantesEstruturas;
import br.com.informata.infojobkernel.infra.NomesParametrosProperties;
import br.com.informata.infojobkernel.job.AbstractJob;
import br.com.informata.infojobkernel.util.HibernateUtil;
import br.com.informata.infolibpentaho.util.PentahoUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.JobExecutionContext;

/**
 *
 * @author joao.neto
 */
public class IntegracaoJob  extends AbstractJob{

    @Override
    protected void setNomeGrupoJar() {
        super.nomeJar  = IntegracaoStboxCdMain.nomeJar;
        super.grupoJar = IntegracaoStboxCdMain.grupoJar;
    }

    @Override
    protected void processaOperacao(JobExecutionContext jec) {

        try {
            
            String strConexao = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_URL);
            String user = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_USERNAME);
            String pass = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_PASSWORD);

            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI027, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI030, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI031, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI032, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI147, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI148, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI258, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI264, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI266, strConexao, user, pass);
                
        } catch (Exception ex) {
            Logger.getLogger(IntegracaoJob.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
    }
    
}
