package br.com.informata.infojob.infojobintegrastboxcd.infra.configuracao;

import br.com.informata.infojobkernel.infra.AbstractListaConfiguracoes;

/**
 *
 * @author danilo.muniz
 */
public class ListaMappingHibernateImpl extends AbstractListaConfiguracoes {

    public ListaMappingHibernateImpl() {
        super(true, true);
//        lista.add("br/com/informata/integracao/model/Rotas.hbm.xml");        
    }

}
