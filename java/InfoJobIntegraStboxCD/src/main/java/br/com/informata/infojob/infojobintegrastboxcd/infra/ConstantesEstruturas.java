/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobintegrastboxcd.infra;

import br.com.informata.infojobkernel.infra.Sistema;
import java.io.File;

/**
 *
 * @author joao.neto
 */
public class ConstantesEstruturas {
    public static String PASTA_PENTAHO     = Sistema.PASTA_INFOJOB + File.separator + "Pentaho" + File.separator;
    public static String PASTA_TRANSF      = Sistema.PASTA_INFOJOB + File.separator + "Pentaho" + File.separator + "integracaoStboxCantinhoDoce"+ File.separator;
    
    public static String PASTA_KTR_TI027   = PASTA_TRANSF + "ktr_ti027.ktr";
    public static String PASTA_KTR_TI030   = PASTA_TRANSF + "ktr_ti030.ktr";
    public static String PASTA_KTR_TI031   = PASTA_TRANSF + "ktr_ti031.ktr";
    public static String PASTA_KTR_TI032   = PASTA_TRANSF + "ktr_ti032.ktr";
    public static String PASTA_KTR_TI147   = PASTA_TRANSF + "ktr_ti147.ktr";
    public static String PASTA_KTR_TI148   = PASTA_TRANSF + "ktr_ti148.ktr";
    public static String PASTA_KTR_TI258   = PASTA_TRANSF + "ktr_ti258.ktr";
    public static String PASTA_KTR_TI264   = PASTA_TRANSF + "ktr_ti264.ktr";
    public static String PASTA_KTR_TI266   = PASTA_TRANSF + "ktr_ti266.ktr";   
    
}
