/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobmerial.infra;

import br.com.informata.infojobkernel.infra.Sistema;
import java.io.File;

/**
 *
 * @author joao.neto
 */
public class ConstantesEstruturas {
    public static String PASTA_PENTAHO     = Sistema.PASTA_INFOJOB + File.separator + "Pentaho" + File.separator;
    public static String ARQUIVO_MERIAL    = Sistema.PASTA_INFOJOB + File.separator + "Pentaho" + File.separator + "merial.ktr";
    public static String ARQUIVO_OURO_FINO = Sistema.PASTA_INFOJOB + File.separator + "Pentaho" + File.separator + "ourofino.ktr";
}