/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobmerial.job;

import br.com.informata.infojob.infojobmerial.MerialMain;
import br.com.informata.infojob.infojobmerial.infra.ConstantesEstruturas;
import br.com.informata.infojobkernel.infra.NomesParametrosProperties;
import br.com.informata.infojobkernel.job.AbstractJob;
import br.com.informata.infojobkernel.util.HibernateUtil;
import br.com.informata.infolibpentaho.util.PentahoUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.JobExecutionContext;

/**
 *
 * @author joao.neto
 */
public class MerialJob extends AbstractJob {

    @Override
    protected void setNomeGrupoJar() {
        super.nomeJar = MerialMain.nomeJar;
        super.grupoJar = MerialMain.grupoJar;
    }

    @Override
    protected void processaOperacao(JobExecutionContext jec) {

        try {
            String strConexao = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_URL);
            String user = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_USERNAME);
            String pass = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_PASSWORD);

            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.ARQUIVO_MERIAL, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.ARQUIVO_OURO_FINO, strConexao, user, pass);
        } catch (Exception ex) {
            Logger.getLogger(MerialJob.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
