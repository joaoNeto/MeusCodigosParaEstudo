/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobmerial;

import br.com.informata.infojob.infojobmerial.infra.ConstantesEstruturas;
import br.com.informata.infojob.infojobmerial.infra.configuracao.ListaMappingHibernateImpl;
import br.com.informata.infojob.infojobmerial.infra.configuracao.MapInfoJobProperties;
import br.com.informata.infojobkernel.exception.InfoJobExceptionDao;
import br.com.informata.infojobkernel.infra.ConfiguracoesInfra;
import br.com.informata.infojobkernel.infra.NomesParametrosProperties;
import br.com.informata.infojobkernel.util.HibernateUtil;
import br.com.informata.infolibpentaho.util.PentahoUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author joao.neto
 */
public class MerialTeste {

    private ConfiguracoesInfra configuracoesBanco;

   // @Before
    public void setUp() throws InfoJobExceptionDao {
        configuracoesBanco = new ConfiguracoesInfra(null, new ListaMappingHibernateImpl(), new MapInfoJobProperties());
        configuracoesBanco.configurar();

    }

    @Test
    public void testeExecutaMarialJob() {
        try {
            String strConexao = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_URL);
            String user = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_USERNAME);
            String pass = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_PASSWORD);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.ARQUIVO_MERIAL, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.ARQUIVO_OURO_FINO, strConexao, user, pass);
        } catch (Exception ex) {
            System.out.println("erro: " + ex.getLocalizedMessage());
        }
    }

}
