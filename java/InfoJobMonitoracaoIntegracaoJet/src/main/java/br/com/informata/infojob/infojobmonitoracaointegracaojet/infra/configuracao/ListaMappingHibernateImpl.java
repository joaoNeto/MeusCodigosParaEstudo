package br.com.informata.infojob.infojobmonitoracaointegracaojet.infra.configuracao;

import br.com.informata.infojobkernel.infra.AbstractListaConfiguracoes;

/**
 *
 * @author danilo.muniz
 */
public class ListaMappingHibernateImpl extends AbstractListaConfiguracoes {

    public ListaMappingHibernateImpl() {
        //vai usar os mapeamentos padrão e das configurações para WS
        super(true, true);
//        lista.add("br/com/informata/integracao/model/Rotas.hbm.xml");        
    }

}
