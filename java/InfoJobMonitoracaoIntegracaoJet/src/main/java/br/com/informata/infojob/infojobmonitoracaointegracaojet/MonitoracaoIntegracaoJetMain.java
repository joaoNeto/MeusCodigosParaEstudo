/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobmonitoracaointegracaojet;

import br.com.informata.infojobkernel.infra.AbstractMain;
import br.com.informata.infojob.infojobmonitoracaointegracaojet.infra.configuracao.ListaMappingHibernateImpl;
import br.com.informata.infojob.infojobmonitoracaointegracaojet.infra.configuracao.MapInfoJobProperties;
import br.com.informata.infojob.infojobmonitoracaointegracaojet.job.MonitoracaoIntegracaoJetJob;
import br.com.informata.infojobkernel.cron.InfoJobCron;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author joao.neto
 */
public class MonitoracaoIntegracaoJetMain extends AbstractMain {
    
    public static String nomeJar;
    public static String grupoJar;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //cria a instância da classe principal.
        MonitoracaoIntegracaoJetMain main = new MonitoracaoIntegracaoJetMain();
        //configurar nome/grupo do jar
        main.setNomeGrupoJar(args);
        // executa a  configuração do banco de dados
        main.executaConfiguracaoBanco(null, new ListaMappingHibernateImpl(), new MapInfoJobProperties());
        // executa a configuracao do quartz, e retorna o caminho so properties.
        String caminho = main.setConfiguracoesQuartzSystem();
        log.info("inicio...");
        try {
            SchedulerFactory schedFact = new StdSchedulerFactory(caminho);
            Scheduler sched;
            sched = schedFact.getScheduler();
            //pega as configurações do banco de dados informações sobre os jobs e triggers que pertecem a este Jar(InfoJob).
            InfoJobCron infoJobCron = new InfoJobCron(nomeJar);
            sched = infoJobCron.configuraSchedulerPorJar(new MonitoracaoIntegracaoJetJob(), sched);
            sched.start();
            log.info("executou com sucesso.");
        } catch (SchedulerException ex) {
            log.error("Erro.", ex);
        }
        log.info("fim...");
    }

    @Override
    protected void setNomeGrupoJar(String[] args) {
        if (args.length >= 2) {
            nomeJar = args[0];
            grupoJar = args[1];
        } else {
            //o que devo fazer lança a exceção.
            nomeJar = "InfoJobMonitoracaoIntegracaoJet";
            grupoJar = "jarsLinhaComando";
        }
    }
    
}
