/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobmonitoracaointegracaojet.model;

import java.util.Objects;

/**
 *
 * @author joao.neto
 */
public class Usuario {
    
    private String emailDestinatario;
    private String nomeDestinatario;

    public Usuario() {
    }

    public Usuario(String emailDestinatario, String nomeDestinatario) {
        this.emailDestinatario = emailDestinatario;
        this.nomeDestinatario = nomeDestinatario;
    }

    public String getEmailDestinatario() {
        return emailDestinatario;
    }

    public void setEmailDestinatario(String emailDestinatario) {
        this.emailDestinatario = emailDestinatario;
    }

    public String getNomeDestinatario() {
        return nomeDestinatario;
    }

    public void setNomeDestinatario(String nomeDestinatario) {
        this.nomeDestinatario = nomeDestinatario;
    }

    @Override
    public String toString() {
        return "Usuario{" + "emailDestinatario=" + emailDestinatario + ", nomeDestinatario=" + nomeDestinatario + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.emailDestinatario);
        hash = 79 * hash + Objects.hashCode(this.nomeDestinatario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.emailDestinatario, other.emailDestinatario)) {
            return false;
        }
        if (!Objects.equals(this.nomeDestinatario, other.nomeDestinatario)) {
            return false;
        }
        return true;
    }
    
    
    
}
