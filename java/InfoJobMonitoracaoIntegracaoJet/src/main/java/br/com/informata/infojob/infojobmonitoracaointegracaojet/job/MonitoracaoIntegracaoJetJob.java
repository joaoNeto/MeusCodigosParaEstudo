/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobmonitoracaointegracaojet.job;

import br.com.informata.infojob.infojobmonitoracaointegracaojet.MonitoracaoIntegracaoJetMain;
import br.com.informata.infojob.infojobmonitoracaointegracaojet.controller.MonitoracaoIntegracaoJetController;
import br.com.informata.infojobkernel.job.AbstractJob;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.JobExecutionContext;

/**
 *
 * @author joao.neto
 */
public class MonitoracaoIntegracaoJetJob extends AbstractJob {

    @Override
    protected void setNomeGrupoJar() {
        super.nomeJar  = MonitoracaoIntegracaoJetMain.nomeJar;
        super.grupoJar = MonitoracaoIntegracaoJetMain.grupoJar;
    }

    @Override
    protected void processaOperacao(JobExecutionContext jec) {
        
        MonitoracaoIntegracaoJetController objMonJetContr = new MonitoracaoIntegracaoJetController();
        
        try {
            if(objMonJetContr.verificaPedido2hrs())
            {
                objMonJetContr.enviarEmail2hrs();
            }
        } catch (SQLException ex) {
            Logger.getLogger(MonitoracaoIntegracaoJetJob.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            String qtd_produtos = objMonJetContr.verificaMais200Produtos();
            if( Integer.parseInt(qtd_produtos) > 200)
            {
                objMonJetContr.enviarEmailMais200Produtos(qtd_produtos);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MonitoracaoIntegracaoJetJob.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        
    }

    
    
}
