/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobmonitoracaointegracaojet.controller;

import br.com.informata.infojob.infojobmonitoracaointegracaojet.dao.MonitoracaoIntegracaoJetDao;
import br.com.informata.infojobkernel.util.EmailUtil;
import br.com.informata.infojob.infojobmonitoracaointegracaojet.email.Email2hrs;
import br.com.informata.infojob.infojobmonitoracaointegracaojet.email.Email200produtos;
import br.com.informata.infojob.infojobmonitoracaointegracaojet.model.Usuario;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.mail.EmailException;

/**
 *
 * @author joao.neto
 */
public class MonitoracaoIntegracaoJetController {
    
    MonitoracaoIntegracaoJetDao jetDao = new MonitoracaoIntegracaoJetDao();
    
    public String verificaMais200Produtos() throws SQLException
    {
        return jetDao.verificaMais200Produtos();
    }
    
    public void enviarEmailMais200Produtos(String qtd_pedidos)
    {
        try {

            MonitoracaoIntegracaoJetDao objDao = new MonitoracaoIntegracaoJetDao();
            Usuario usuario = objDao.configuracaoUsuario();
            
            String assunto  = "Resumo monitoração integração.";
            
            Email200produtos email200produtos = new Email200produtos();
            EmailUtil emailUtil = new EmailUtil();

            email200produtos.apresentacaoDia  = "Bom dia";
            email200produtos.nomeDestinatario = usuario.getNomeDestinatario();
            email200produtos.qtd_produtos = qtd_pedidos;
            
            StringBuilder strBuilderEmail = email200produtos.montarEmailMonitoracaoIntegracao();            
            
            emailUtil.enviarHtmlMail(usuario.getEmailDestinatario(), assunto, strBuilderEmail);
            
        } catch (EmailException ex) {
            Logger.getLogger(MonitoracaoIntegracaoJetController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
 
    public boolean verificaPedido2hrs() throws SQLException
    {
        return jetDao.verificaPedido2hrs();
    }
    
    public void enviarEmail2hrs()
    {

        try {

            MonitoracaoIntegracaoJetDao objDao = new MonitoracaoIntegracaoJetDao();
            Usuario usuario = objDao.configuracaoUsuario();
            
            String assunto = "Resumo monitoração integração com email mais de 200 pedidos.";

            Email2hrs email2hrs = new Email2hrs();
            EmailUtil emailUtil = new EmailUtil();

            email2hrs.apresentacaoDia = "Bom dia";
            email2hrs.nomeDestinatario = usuario.getNomeDestinatario();
            
            
            StringBuilder strBuilderEmail = email2hrs.montarEmailMonitoracaoIntegracao();            
            
            emailUtil.enviarHtmlMail(usuario.getEmailDestinatario(), assunto, strBuilderEmail);
            
        } catch (EmailException ex) {
            Logger.getLogger(MonitoracaoIntegracaoJetController.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
    }
    
}
