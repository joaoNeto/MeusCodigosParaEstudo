package br.com.informata.infojob.infojobmonitoracaointegracaojet.infra.configuracao;

import br.com.informata.infojobkernel.infra.AbstractMapConfiguracoes;
import br.com.informata.infojobkernel.infra.NomesParametrosProperties;

/**
 *
 * @author danilo.muniz
 */
public class MapInfoJobProperties extends AbstractMapConfiguracoes {

    public MapInfoJobProperties() {
        map.put(NomesParametrosProperties.HIBERNATE_CONNECTION_URL, "jdbc:oracle:thin:@192.168.0.228:1521:NEGRAOPD");
        map.put(NomesParametrosProperties.HIBERNATE_CONNECTION_USERNAME, "INTEGRA");
        map.put(NomesParametrosProperties.HIBERNATE_CONNECTION_PASSWORD, "8I9p3tkrwRiEF0h7qSOBxA==");
        map.put(NomesParametrosProperties.HIBERNATE_DIALECT, "org.hibernate.dialect.Oracle10gDialect");
        map.put(NomesParametrosProperties.HIBERNATE_CONNECTION_DRIVER_CLASS, "oracle.jdbc.driver.OracleDriver");
        map.put(NomesParametrosProperties.HIBERNATE_C3P0_MIN_SIZE, "1");
        map.put(NomesParametrosProperties.HIBERNATE_C3P0_MAX_SIZE, "100");
        map.put(NomesParametrosProperties.HIBERNATE_C3P0_TIMEOUT, "600");
        map.put(NomesParametrosProperties.HIBERNATE_C3P0_MAX_STATEMENTS, "50");
        map.put(NomesParametrosProperties.HIBERNATE_CONNECTION_PASSWORD_ENCRIPTADO, "1");
    }
}
