/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobmonitoracaointegracaojet.email;

/**
 *
 * @author joao.neto
 */
public class Email200produtos {
    
    public String apresentacaoDia;
    public String nomeDestinatario;
    public String qtd_produtos;

    private StringBuilder formatarTopo() {
        StringBuilder topo = new StringBuilder();
        topo.append("<tr>\n");
        topo.append("<td colspan=\"4\" style=\"background-color: #ddd; border-top: 3px solid #0068ad;\">\n");
        topo.append("<img src=\"http://web.informata.com.br:2016/74.00.00.00/wp-content/uploads/2017/06/infocron.png\">\n");
        topo.append("</td>\n");
        topo.append("</tr>\n");
        topo.append("<tr>\n");
        topo.append("<td colspan=\"4\">\n");
        topo.append("<h2>" + ((apresentacaoDia != null) ? apresentacaoDia + ", " : "") + ((nomeDestinatario != null) ? nomeDestinatario + "." : "") + "</h2>\n");
        topo.append("<p>Relatório do resumo do monitoramento das entidades de integração.</p>\n");
        topo.append("</td>\n");
        topo.append("</tr>\n");
        return topo;
    }

    private StringBuilder formatarConteudoResumoInicial() {
        StringBuilder resumoInicial = new StringBuilder();
        
        resumoInicial.append("<tr>\n");
        resumoInicial.append("<td colspan=\"4\" style=\"text-align: center\">\n");
        resumoInicial.append("Nesse momento há  "+qtd_produtos+" produtos não processados na integração com o E-Commerce. Favor verificar se há problemas no procedimento de integração.!");
        resumoInicial.append("</td>\n");
        resumoInicial.append("</tr>\n");
        
        return resumoInicial;
    }

    private StringBuilder formatarRodape() {
        StringBuilder rodape = new StringBuilder();
        rodape.append("<tr>\n");
        rodape.append("<td></td>\n");
        rodape.append("<td></td>\n");
        rodape.append("<td colspan=\"2\">Desenvolvido por: <img src=\"http://web.informata.com.br:2016/74.00.00.00/wp-content/uploads/2017/06/logo_informata.png\" style=\"padding: 20px 0 10px 0; vertical-align: middle; font-size: 11px;\"></td>\n");
        rodape.append("</tr>\n");
        return rodape;
    }

    public StringBuilder montarEmailMonitoracaoIntegracao() {
        StringBuilder email = new StringBuilder();
        
        email.append("<!DOCTYPE html>\n");
        email.append("<html>\n");
        email.append("<head>\n");
        email.append("<title>Email Teste</title>\n");
        email.append("<meta harset=\"UTF-8\">\n");
        email.append("</head>\n");
        email.append("<body>\n");
        email.append("<table align=\"center\" width=\"550px\" style=\"font-family: Arial, Verdana, Helvetica, sans-serif;\" cellspacing=\"0\">\n");
        email.append(formatarTopo());
        email.append(formatarConteudoResumoInicial());
        email.append(formatarRodape());
        email.append("</table>\n");
        email.append("</body>\n");
        email.append("</html>\n");
        
        String emailout = email.toString();
        emailout = emailout.trim().replaceAll("\t", "").replaceAll("\n", "");
        email = new StringBuilder();
        email.append(emailout);
        return email;
    }
    
    
}
