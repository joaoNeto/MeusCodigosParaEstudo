/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobmonitoracaointegracaojet.dao;

import br.com.informata.infojobkernel.dao.AbstractDao;
import br.com.informata.infojob.infojobmonitoracaointegracaojet.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.hibernate.HibernateException;

/**
 *
 * @author joao.neto
 */
public class MonitoracaoIntegracaoJetDao extends AbstractDao {
    
    public String verificaMais200Produtos() throws SQLException
    {
        String sql = "SELECT \n" +
                     "  count(*) qtd_produtos\n" +
                     "FROM TI027_PRODUTOS\n" +
                     "WHERE TI027_STATUS_REGISTRO  = 'NP'\n" +
                     "	  AND TI027_SISTEMA_DESTINO_IE = 5\n" +
                     "	  AND TI027_SISTEMA_ORIGEM_E   = 1";
        
        try {
            
            abrirSessao();            
            Connection connection = obterConexaoJDBC();
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {

                return Integer.toString(rs.getInt("qtd_produtos"));
                
            }
            stm.close();
            rs.close();            
            
        } catch (HibernateException hibernateException) {
            System.out.println("Erro de banco de dados: " + hibernateException.getLocalizedMessage());
        }finally{
            fecharSessao();
            desconectarConexaoJDBC();
            desconectar();            
        }
        return "0";
          
    }
    
    public boolean verificaPedido2hrs() throws SQLException
    {
        String sql = "SELECT \n" +
                     "  CASE WHEN COUNT(*) > 0 THEN\n" +
                     "    '1'\n" +
                     "  ELSE\n" +
                     "    '0'\n" +
                     "  END bol_enviar_email,\n" +
                     "  count(*)\n" +
                     "FROM TI009_PRE_NOTA\n" +
                     "WHERE TI009_DATA_PRC_REGISTRO >= sysdate-2/24\n" +
                     "  AND TI009_SISTEMA_DESTINO_IE = 5\n" +
                     "  AND TI009_SISTEMA_ORIGEM_E   = 1";
        
        try {
            
            abrirSessao();
            Connection connection = obterConexaoJDBC();
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {
                
                if( rs.getString("bol_enviar_email") == "1" )
                {
                    return true;
                }else{
                    return false;
                }
                
            }
            stm.close();
            rs.close();            
            
        } catch (HibernateException hibernateException) {
            System.out.println("Erro de banco de dados: " + hibernateException.getLocalizedMessage());
        }finally{
            fecharSessao();
            desconectarConexaoJDBC();
            desconectar();            
        }
        return false;           
        
    }
    
    public Usuario configuracaoUsuario()
    {
        Usuario usuario = new Usuario();
        
        usuario.setEmailDestinatario("joao.neto@informata.com.br");
        usuario.setNomeDestinatario("joao neto");
        
        return usuario;
        
    }
    
}
