/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobmonitoracaointegracaojet;

import br.com.informata.infojob.infojobmonitoracaointegracaojet.controller.MonitoracaoIntegracaoJetController;
import br.com.informata.infojob.infojobmonitoracaointegracaojet.infra.configuracao.ListaMappingHibernateImpl;
import br.com.informata.infojob.infojobmonitoracaointegracaojet.infra.configuracao.MapInfoJobProperties;
import br.com.informata.infojobkernel.exception.InfoJobExceptionDao;
import br.com.informata.infojobkernel.infra.ConfiguracoesInfra;
import java.sql.SQLException;
import org.junit.Test;

/**
 *
 * @author joao.neto
 */
public class EnvioEmailTest {
    
   private ConfiguracoesInfra configuracoesBanco;

//    @Before
   public void setUp() throws InfoJobExceptionDao {
       configuracoesBanco = new ConfiguracoesInfra(null, new ListaMappingHibernateImpl(), new MapInfoJobProperties());
       configuracoesBanco.configurar();
   }

   //@Test
   public void testeJobExec() throws InfoJobExceptionDao, SQLException {
       
        MonitoracaoIntegracaoJetController objMonJetContr = new MonitoracaoIntegracaoJetController();
        
        if(objMonJetContr.verificaPedido2hrs())
        {
            objMonJetContr.enviarEmail2hrs();
        }

        String qtd_produtos = objMonJetContr.verificaMais200Produtos();
        
        if( Integer.parseInt(qtd_produtos) > 200)
        {
            objMonJetContr.enviarEmailMais200Produtos(qtd_produtos);
        }
        
        
   }

    
}
