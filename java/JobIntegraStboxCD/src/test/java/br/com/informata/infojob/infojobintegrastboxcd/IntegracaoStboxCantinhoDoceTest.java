package br.com.informata.infojob.infojobintegrastboxcd;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import br.com.informata.infojob.infojobintegrastboxcd.infra.ConstantesEstruturas;
import br.com.informata.infojob.infojobintegrastboxcd.infra.configuracao.ListaMappingHibernateImpl;
import br.com.informata.infojob.infojobintegrastboxcd.infra.configuracao.MapInfoJobProperties;
import br.com.informata.infojob.infojobintegrastboxcd.jobs.IntegracaoProdutoJob;
import br.com.informata.infojobkernel.infra.ConfiguracoesInfra;
import br.com.informata.infojobkernel.infra.NomesParametrosProperties;
import br.com.informata.infojobkernel.util.HibernateUtil;
import br.com.informata.infolibpentaho.util.PentahoUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author joao.neto
 */
public class IntegracaoStboxCantinhoDoceTest {

    private ConfiguracoesInfra configuracoesBanco;

    @Before
    public void setUp() {

        configuracoesBanco = new ConfiguracoesInfra(null, new ListaMappingHibernateImpl(), new MapInfoJobProperties());
        configuracoesBanco.configurar();

    }

    @Test
    public void execucaoJob() {

        try {

            String strConexao = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_URL);
            String user = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_USERNAME);
            String pass = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_PASSWORD);

//            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI027, strConexao, user, pass);
//            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI030, strConexao, user, pass);
            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI031_TI032, strConexao, user, pass);
//            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI147, strConexao, user, pass);
//            PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI148, strConexao, user, pass);
            //PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI258, strConexao, user, pass);
            //PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI264, strConexao, user, pass);
            //PentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI266, strConexao, user, pass);

        } catch (Exception ex) {
            Logger.getLogger(IntegracaoProdutoJob.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
