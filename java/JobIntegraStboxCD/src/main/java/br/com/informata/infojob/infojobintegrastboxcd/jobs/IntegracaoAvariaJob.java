/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobintegrastboxcd.jobs;

import br.com.informata.infojob.infojobintegrastboxcd.IntegracaoStboxCdMain;
import br.com.informata.infojob.infojobintegrastboxcd.infra.ConstantesEstruturas;
import br.com.informata.infojobkernel.infra.NomesParametrosProperties;
import br.com.informata.infojobkernel.job.AbstractJob;
import br.com.informata.infojobkernel.util.HibernateUtil;
import br.com.informata.infolibpentaho.util.PentahoUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

/**
 *
 * @author joao.neto
 */
public class IntegracaoAvariaJob  extends AbstractJob{

    @Override
    protected void setNomeGrupoJar() {
        super.nomeJar  = IntegracaoStboxCdMain.nomeJar;
        super.grupoJar = IntegracaoStboxCdMain.grupoJar;
    }

    @Override
    protected void processaOperacao(JobExecutionContext jec) {

        try {
            JobDataMap dataMap = jec.getJobDetail().getJobDataMap();
            PentahoUtil pentahoUtil = new PentahoUtil();
            String strConexao = dataMap.getString("strConexao");
            String user = dataMap.getString("user");
            String pass = dataMap.getString("pass");

            pentahoUtil.ExecutarTransTrataErro(ConstantesEstruturas.PASTA_KTR_TI031_TI032, strConexao, user, pass);
                
        } catch (Exception ex) {
            Logger.getLogger(IntegracaoAvariaJob.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
    }
    
}
