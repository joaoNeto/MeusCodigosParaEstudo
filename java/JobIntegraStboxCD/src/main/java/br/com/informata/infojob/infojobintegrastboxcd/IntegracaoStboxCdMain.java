/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobintegrastboxcd;

import br.com.informata.infojob.infojobintegrastboxcd.infra.configuracao.ListaMappingHibernateImpl;
import br.com.informata.infojob.infojobintegrastboxcd.infra.configuracao.MapInfoJobProperties;
import br.com.informata.infojob.infojobintegrastboxcd.jobs.IntegracaoAvariaJob;
import br.com.informata.infojob.infojobintegrastboxcd.jobs.IntegracaoBalancoJob;
import br.com.informata.infojob.infojobintegrastboxcd.jobs.IntegracaoPedidoJob;
import br.com.informata.infojob.infojobintegrastboxcd.jobs.IntegracaoeRotasEntidadesNotaFiscalJob;
import br.com.informata.infojob.infojobintegrastboxcd.jobs.IntegracaoProdutoJob;
import br.com.informata.infojobkernel.cron.InfoJobCron;
import br.com.informata.infojobkernel.infra.AbstractMain;
import br.com.informata.infojobkernel.infra.NomesParametrosProperties;
import br.com.informata.infojobkernel.util.HibernateUtil;
import java.util.HashMap;
import java.util.Map;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author joao.neto
 */
public class IntegracaoStboxCdMain extends AbstractMain {
    
    public static String nomeJar;
    public static String grupoJar;    
    
    public static void main(String[] args) {
    
        //cria a instância da classe principaOl.
        IntegracaoStboxCdMain main = new IntegracaoStboxCdMain();
        //configurar nome/grupo do jar
        main.setNomeGrupoJar(args);
        // executa a  configuração do banco de dados
        main.executaConfiguracaoBanco(null, new ListaMappingHibernateImpl(), new MapInfoJobProperties());
        // executa a configuracao do quartz, e retorna o caminho so properties.
        String caminho = main.setConfiguracoesQuartzSystem();
        log.info("inicio...");
        try {
            SchedulerFactory schedFact = new StdSchedulerFactory(caminho);
            Scheduler schedProduto,schedFornecedor,schedAvaria,schedBalanco, schedPedido ;
            schedProduto = schedFact.getScheduler();
            schedFornecedor = schedFact.getScheduler();
            schedAvaria = schedFact.getScheduler();
            schedBalanco = schedFact.getScheduler();
            schedPedido = schedFact.getScheduler();
            
            
            //
            String strConexao = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_URL);
            String user = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_USERNAME);
            String pass = HibernateUtil.obterValorDePropertie(NomesParametrosProperties.HIBERNATE_CONNECTION_PASSWORD);
            Map<String, Object> paremetros = new HashMap();            
            paremetros.put("strConexao", strConexao);
            paremetros.put("user", user);
            paremetros.put("pass", pass);            


            // EXECUTA JOB AVARIA
            InfoJobCron infoJobCronAvaria = new InfoJobCron(nomeJar,"jobIntegracaoStboxCdAvaria","grupoIntegracaoAvaria",paremetros);
            schedAvaria = infoJobCronAvaria.configuraSchedulerPorJobGrupo(new IntegracaoAvariaJob(), schedAvaria);

            // EXECUTA JOB BALANCO
            InfoJobCron infoJobCronBalanco = new InfoJobCron(nomeJar,"jobIntegracaoStboxCdBalanco","grupoIntegracaoBalanco",paremetros);
            schedBalanco = infoJobCronBalanco.configuraSchedulerPorJobGrupo(new IntegracaoBalancoJob(), schedBalanco);            

            // EXECUTA JOB PEDIDO
            InfoJobCron infoJobCronPedido = new InfoJobCron(nomeJar,"jobIntegracaoStboxCdPedido","grupoIntegracaoPedido",paremetros);
            schedPedido = infoJobCronPedido.configuraSchedulerPorJobGrupo(new IntegracaoPedidoJob(), schedPedido);            
            
            // EXECUTA JOB PRODUTO
            InfoJobCron infoJobCronProduto = new InfoJobCron(nomeJar,"jobIntegracaoStboxCdProduto","grupoIntegracaoProduto",paremetros);
            schedProduto = infoJobCronProduto.configuraSchedulerPorJobGrupo(new IntegracaoProdutoJob(), schedProduto);

            // EXECUTA JOB ROTAS/ENTIDADES/NOTA_FISCAL            
            InfoJobCron infoJobCronFornecdor = new InfoJobCron(nomeJar,"jobIntegracaoStboxCdNotaEntidadeRota","grupoIntegracaoNotaEntidadeRota",paremetros);
            schedFornecedor = infoJobCronFornecdor.configuraSchedulerPorJobGrupo(new IntegracaoeRotasEntidadesNotaFiscalJob(), schedFornecedor);
            

            schedProduto.start();
            schedFornecedor.start();
            schedAvaria.start();
            schedBalanco.start();
            schedPedido.start();
            
            log.info("executou com sucesso.");
        } catch (SchedulerException ex) {
            log.error("Erro.", ex);
        }
        log.info("fim...");        
        
    }

    protected void setNomeGrupoJar(String[] args) {
        if (args.length >= 2) {
            nomeJar = args[0];
            grupoJar = args[1];
        } else {
            //o que devo fazer lança a exceção.
            nomeJar = "InfoJobIntegraStboxCD";
            grupoJar = "jarsLinhaComando";
        }
    }     
    
}
