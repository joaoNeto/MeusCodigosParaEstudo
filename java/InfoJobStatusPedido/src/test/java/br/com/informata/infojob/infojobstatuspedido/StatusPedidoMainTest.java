/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobstatuspedido;

import br.com.informata.infojob.infojobstatuspedido.client.OrderStateCompleteClient;
import br.com.informata.infojob.infojobstatuspedido.infra.configuracao.ListaMappingHibernateImpl;
import br.com.informata.infojob.infojobstatuspedido.infra.configuracao.MapInfoJobProperties;
import br.com.informata.infojob.infojobstatuspedido.model.StatusPedido;
import br.com.informata.infojobkernel.exception.InfoJobExceptionDao;
import br.com.informata.infojobkernel.infra.ConfiguracoesInfra;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author joao.neto
 */
public class StatusPedidoMainTest {

    private ConfiguracoesInfra configuracoesBanco;

    ///Before
    public void setUp() throws InfoJobExceptionDao {
        configuracoesBanco = new ConfiguracoesInfra(null, new ListaMappingHibernateImpl(), new MapInfoJobProperties());
        configuracoesBanco.configurar();

    }

    //@Test
    public void testePedidoConcluido() {

        try {

            int idConfiguracaoWebService = 4; // ecommerce
            OrderStateCompleteClient orderStateCompleteClient = new OrderStateCompleteClient(idConfiguracaoWebService);
            List<StatusPedido> listaStatus = (List<StatusPedido>) orderStateCompleteClient.carregarInterfaceImport();
            orderStateCompleteClient.executarEnvioInterfaceImport(listaStatus); // pedidos em status

        } catch (InfoJobExceptionDao ex) {
            Logger.getLogger(StatusPedidoMainTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
