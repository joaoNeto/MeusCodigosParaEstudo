/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobstatuspedido.client;

import br.com.informata.infojob.InfoJobLibJet.infra.ConfiguracoesWsJet;
import br.com.informata.infojob.InfoJobLibJet.wsdl.wsjet.MethodReturn;
import br.com.informata.infojob.InfoJobLibJet.wsdl.wsjet.WSJETSoap;
import br.com.informata.infojob.infojobstatuspedido.controller.StatusPedidoController;
import br.com.informata.infojob.infojobstatuspedido.model.StatusPedido;
import br.com.informata.infojobkernel.exception.InfoJobExceptionDao;
import br.com.informata.infojobkernel.infra.Import;
import java.net.MalformedURLException;
import java.util.List;

/**
 *
 * @author danilo.muniz
 */
public class OrderStateCompleteClient extends ConfiguracoesWsJet implements Import {

    private StatusPedidoController statusPedidoController;
    public int idOrderStateTracking = 0;

    public OrderStateCompleteClient(int idConfiguracaoWebService) throws InfoJobExceptionDao {
        super(idConfiguracaoWebService);
        this.statusPedidoController = new StatusPedidoController();
    }

    @Override
    public Object carregarInterfaceImport() {
        return this.statusPedidoController.obtemPedidosStatusTracking();
    }

    @Override
    public void tratarRetornoInterfaceImport(Object... object) {
        String[] mensagemCodigo;
        String idProductParceiro = "";
        MethodReturn methodReturn = (MethodReturn) object[0];
        StatusPedido statusPedido = (StatusPedido) object[1];
        mensagemCodigo = methodReturn.getMessage().split("->");
        idProductParceiro = (mensagemCodigo.length > 1) ? mensagemCodigo[1] : "";
        //a chave do registro.
        Object[] chave = {statusPedido.getT123NumeroDav(), idProductParceiro};
        try {
            if (methodReturn.getId() == 1) {
                //imprimindo no log.
                System.out.println("idOrderState=" + idProductParceiro);
                System.out.println("Resultado: \n id=" + methodReturn.getId() + "\n mensagem=" + methodReturn.getMessage());
            } else {
                //panssado os valores do registro
                System.out.println("idProductParceiro=" + idProductParceiro);
                System.out.println("Resultado: \n id=" + methodReturn.getId() + "\n mensagem=" + methodReturn.getMessage());
            }
        } catch (Exception e) {
            //imprimindo no log.
            System.out.println("erro:" + e.getLocalizedMessage());
        }
    }

    @Override
    public void executarEnvioInterfaceImport(Object object) {
        WSJETSoap clientWsJetSoapInterface = null;
        try {
            clientWsJetSoapInterface = obtemInstanciaInterfaceClient();

            List<StatusPedido> lista = (List<StatusPedido>) object;
            for (StatusPedido statusPedido : lista) {
                MethodReturn methodReturn = clientWsJetSoapInterface.alterOrderStateComplete(super.configuracaoWebService.getTi902User(),
                        super.configuracaoWebService.getTi902Password(),
                        Integer.valueOf(statusPedido.getT123NumeroDav()),
                        idOrderStateTracking,
                        statusPedido.getT163TrackNumber());
                tratarRetornoInterfaceImport(methodReturn, statusPedido);
            }
        } catch (MalformedURLException ex) {
            System.out.println("erro: " + ex.getLocalizedMessage());
        } catch (Exception e) {
            System.out.println("erro:" + e.getLocalizedMessage());
        }
    }

}
