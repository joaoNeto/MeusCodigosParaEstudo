package br.com.informata.infojob.infojobstatuspedido;

import br.com.informata.infojob.InfoJobLibJet.infra.configuracao.ListaMappingHibernateImpl;
import br.com.informata.infojob.InfoJobLibJet.infra.configuracao.MapInfoJobProperties;
import br.com.informata.infojob.infojobstatuspedido.job.StatusPedidoJob;
import br.com.informata.infojobkernel.cron.InfoJobCron;
import br.com.informata.infojobkernel.infra.AbstractMain;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author danilo.muniz
 */
public class StatusPedidoMain extends AbstractMain {

    public static String nomeJar;
    public static String grupoJar;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //cria a instância da classe principal.
        StatusPedidoMain main = new StatusPedidoMain();
        //configurar nome/grupo do jar
        main.setNomeGrupoJar(args);
        // executa a  configuração do banco de dados
        main.executaConfiguracaoBanco(null,StatusPedidoMain.nomeJar, new ListaMappingHibernateImpl(), new MapInfoJobProperties());
        // executa a configuracao do quartz, e retorna o caminho so properties.
        String caminho = main.setConfiguracoesQuartzSystem();
        log.info("inicio...");
        try {
            SchedulerFactory schedFact = new StdSchedulerFactory(caminho);
            Scheduler sched;
            sched = schedFact.getScheduler();
            //pega as configurações do banco de dados informações sobre os jobs e triggers que pertecem a este Jar(InfoJob).
            InfoJobCron infoJobCron = new InfoJobCron(nomeJar);
            sched = infoJobCron.configuraSchedulerPorJar(new StatusPedidoJob(), sched);
            sched.start();
            log.info("executou com sucesso.");
        } catch (SchedulerException ex) {
            log.error("Erro.", ex);
        }
        log.info("fim...");
    }

    @Override
    protected void setNomeGrupoJar(String[] args) {
        if (args.length >= 2) {
            nomeJar = args[0];
            grupoJar = args[1];
        } else {
            //o que devo fazer lança a exceção.
            nomeJar = "InfoJobStatusPedido";
            grupoJar = "jarsLinhaComando";
        }
    }
}
