package br.com.informata.infojob.infojobstatuspedido.client;

import br.com.informata.infojob.InfoJobLibJet.infra.ConfiguracoesWsJet;
import br.com.informata.infojob.InfoJobLibJet.wsdl.wsjet.ArrayOfOrderState;
import br.com.informata.infojob.InfoJobLibJet.wsdl.wsjet.MethodReturn;
import br.com.informata.infojob.InfoJobLibJet.wsdl.wsjet.WSJETSoap;
import br.com.informata.infojob.infojobstatuspedido.controller.StatusPedidoController;
import br.com.informata.infojob.infojobstatuspedido.dao.StatusPedidoDao;
import br.com.informata.infojob.infojobstatuspedido.model.StatusPedido;
import br.com.informata.infojobkernel.exception.InfoJobExceptionDao;
import br.com.informata.infojobkernel.infra.Export;
import br.com.informata.infojobkernel.infra.Import;
import br.com.informata.infojobkernel.model.Ti902Webservice;
import java.net.MalformedURLException;
import java.util.List;

/**
 *
 * @author danilo.muniz
 */
public class OrderStateClient extends ConfiguracoesWsJet implements Export, Import {

    private ArrayOfOrderState arrayOfOrderState;
    private StatusPedidoController statusPedidoController;
    public int idOrderStateConcluido = 0;


    public OrderStateClient(int idConfiguracaoWebService) throws InfoJobExceptionDao {
        super(idConfiguracaoWebService);
        this.statusPedidoController = new StatusPedidoController();
    }

    public OrderStateClient(Ti902Webservice configuracaoWebService) {
        super(configuracaoWebService);
        this.statusPedidoController = new StatusPedidoController();
    }

    public ArrayOfOrderState getArrayOfOrderState() {
        return this.arrayOfOrderState;
    }

    @Override
    public void carregarInterfaceExport(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void executarRecebeInterfaceExport() {
        WSJETSoap clientWsJetSoapInterface = null;
        try {
            clientWsJetSoapInterface = obtemInstanciaInterfaceClient();
            //Retorna os status do pedidoss
            this.arrayOfOrderState = clientWsJetSoapInterface.exportOrderState(super.configuracaoWebService.getTi902User(), super.configuracaoWebService.getTi902Password(), 0);
        } catch (MalformedURLException ex) {
            System.out.println("erro: " + ex.getLocalizedMessage());
        } catch (Exception e) {
            System.out.println("erro:" + e.getLocalizedMessage());
        }
    }

    @Override
    public Object carregarInterfaceImport() {
         return this.statusPedidoController.obtemPedidosStatusConcluido();
    }

    @Override
    public void tratarRetornoInterfaceImport(Object... object) {
        String[] mensagemCodigo;
        String idProductParceiro = "";
        MethodReturn methodReturn = (MethodReturn) object[0];
        StatusPedido statusPedido = (StatusPedido) object[1];
        mensagemCodigo = methodReturn.getMessage().split("->");
        idProductParceiro = (mensagemCodigo.length > 1) ? mensagemCodigo[1] : "";
        //a chave do registro.
        Object[] chave = {statusPedido.getT123NumeroDav(), idProductParceiro};
        try {
            if (methodReturn.getId() == 1) {
                //imprimindo no log.
                System.out.println("idOrderState=" + idProductParceiro);
                System.out.println("Resultado: \n id=" + methodReturn.getId() + "\n mensagem=" + methodReturn.getMessage());
            } else {
                //panssado os valores do registro
                System.out.println("idProductParceiro=" + idProductParceiro);
                System.out.println("Resultado: \n id=" + methodReturn.getId() + "\n mensagem=" + methodReturn.getMessage());
            }
        } catch (Exception e) {
            //imprimindo no log.
            System.out.println("erro:" + e.getLocalizedMessage());
        }
    }

    @Override
    public void executarEnvioInterfaceImport(Object object) {
         WSJETSoap clientWsJetSoapInterface = null;

        try {
            clientWsJetSoapInterface = obtemInstanciaInterfaceClient();
            List<StatusPedido> lista = (List<StatusPedido>) object;
            StatusPedidoDao statusPedidoDao = new StatusPedidoDao();

            for (StatusPedido statusPedido : lista) {
                MethodReturn methodReturn = clientWsJetSoapInterface.alterOrderState(
                        super.configuracaoWebService.getTi902User(),
                        super.configuracaoWebService.getTi902Password(),
                        Integer.valueOf(statusPedido.getT123NumeroDav()),
                        idOrderStateConcluido);
                tratarRetornoInterfaceImport(methodReturn, statusPedido);
                statusPedidoDao.atualizandoStatusVerificacaoT123(statusPedido.getT123NumeroDav()); // atualiza a coluna 
            }
        } catch (MalformedURLException ex) {
            System.out.println("erro: " + ex.getLocalizedMessage());
        } catch (Exception e) {
            System.out.println("erro:" + e.getLocalizedMessage());
        }
    }
}
