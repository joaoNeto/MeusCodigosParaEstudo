package br.com.informata.infojob.infojobstatuspedido.dao;

import br.com.informata.infojob.infojobstatuspedido.model.StatusPedido;
import br.com.informata.infojobkernel.dao.AbstractDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.JDBCException;

/**
 *
 * @author danilo.muniz
 */
public class StatusPedidoDao extends AbstractDao {

    private static Logger log = Logger.getLogger(StatusPedidoDao.class);

    public List<StatusPedido> obtemPedidosStatusTracking() {
        List<StatusPedido> lista = new ArrayList<StatusPedido>();
        String sql = "SELECT \n"
                + "T123_NUMERO_PRENOTA,\n"
                + "T123_NUMERO_DAV,\n"
                + "T163_TRACK_NUMBER\n"
                + "FROM\n"
                + "DBAMDATA.T123_SAIDA S\n"
                + "JOIN  DBAMDATA.T163_PRENOTA_VOLUMES PV ON\n"
                + "   S.T123_UNIDADE_LOGISTICA_E = PV.T163_UNIDADE_LOGISTICA_IE\n"
                + "   AND S.T123_NUMERO_PRENOTA = PV.T163_NUMERO_PRENOTA_IE\n"
                + "WHERE T123_ORIGEM_NOTA = 4\n"
                + " AND T123_UNIDADE_IE = (select T925_LOJA_INTEG_ECOMMERCE from T925_PARAM_GERAIS_UNIDADES) \n"
                + " AND TRUNC(T123_DATA_EMISSAO_IU) >= TO_DATE('01/09/2017','DD/MM/YYYY') \n"
                + " AND T163_TRACK_NUMBER IS NOT NULL\n"
                + " AND T123_NUMERO_DAV IS NOT NULL\n"
                + " AND T163_DATA_ENTREGA_CORREIOS IS NULL\n"
                + " AND T123_PEDIDO_ECOMERCE_CONCLUIDO = 0";
        
        try {
            abrirSessao();
            Connection connection = obterConexaoJDBC();
            //prepara o cursor da stament(instrução), para que retorne um ResultSet(cursor) circular.
            PreparedStatement stm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                StatusPedido statusPedido = new StatusPedido();
                statusPedido.setT123NumeroDav(rs.getString("T123_NUMERO_PRENOTA"));
                statusPedido.setT123NumeroPrenota(rs.getString("T123_NUMERO_DAV"));
                statusPedido.setT163TrackNumber(rs.getString("T163_TRACK_NUMBER"));
                lista.add(statusPedido);
            }
            stm.close();
            rs.close();
        } catch (JDBCException e) {
            SQLException causa = (SQLException) e.getCause();
            log.error("Erro de banco de dados: " + causa.getLocalizedMessage());
        } catch (Exception e) {
            log.error("Erro de banco de dados: " + e.getLocalizedMessage());
        } finally {
            fecharSessao();
            desconectarConexaoJDBC();
            desconectar();
        }
        return lista;
    }

    public List<StatusPedido> obtemPedidosStatusConcluido() {
        List<StatusPedido> lista = new ArrayList<StatusPedido>();
        String sql = "SELECT\n"
                + "  T123_NUMERO_PRENOTA,\n"
                + "  T123_NUMERO_DAV,\n"
                + "  T163_TRACK_NUMBER,\n"
                + "  T163_DATA_ENTREGA_CORREIOS\n"
                + "FROM\n"
                + "  DBAMDATA.T123_SAIDA S\n"
                + "    JOIN  DBAMDATA.T163_PRENOTA_VOLUMES PV ON\n"
                + "        S.T123_UNIDADE_LOGISTICA_E = PV.T163_UNIDADE_LOGISTICA_IE\n"
                + "        AND S.T123_NUMERO_PRENOTA = PV.T163_NUMERO_PRENOTA_IE\n"
                + "WHERE T123_ORIGEM_NOTA = 4\n"
                + "  AND T123_UNIDADE_IE = (select T925_LOJA_INTEG_ECOMMERCE from T925_PARAM_GERAIS_UNIDADES) \n"
                + "  AND TRUNC(T123_DATA_EMISSAO_IU) >= TO_DATE('01/09/2017','DD/MM/YYYY') \n"
                + "  AND T163_TRACK_NUMBER IS NOT NULL\n"
                + "  AND T123_NUMERO_DAV IS NOT NULL\n"
                + "  AND (T163_DATA_ENTREGA_CORREIOS IS NOT NULL or T123_CANHOTO_USUARIO IS NOT NULL)\n "
                + "  AND T123_PEDIDO_ECOMERCE_CONCLUIDO = 0";
        try {
            abrirSessao();
            Connection connection = obterConexaoJDBC();
            //prepara o cursor da stament(instrução), para que retorne um ResultSet(cursor) circular.
            PreparedStatement stm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                StatusPedido statusPedido = new StatusPedido();
                statusPedido.setT123NumeroDav(rs.getString("T123_NUMERO_PRENOTA"));
                statusPedido.setT123NumeroPrenota(rs.getString("T123_NUMERO_DAV"));
                statusPedido.setT163TrackNumber(rs.getString("T163_TRACK_NUMBER"));
                lista.add(statusPedido);
            }
            stm.close();
            rs.close();
        } catch (JDBCException e) {
            SQLException causa = (SQLException) e.getCause();
            log.error("Erro de banco de dados: " + causa.getLocalizedMessage());
        } catch (Exception e) {
            log.error("Erro de banco de dados: " + e.getLocalizedMessage());
        } finally {
            fecharSessao();
            desconectarConexaoJDBC();
            desconectar();
        }
        return lista;
    }

    public void atualizandoStatusVerificacaoT123(String numero_dav){
    
        String sql =  "UPDATE \n" +
                      "	DBAMDATA.T123_SAIDA \n" +
                      "SET \n" +
                      "  T123_PEDIDO_ECOMERCE_CONCLUIDO = 1 \n" +
                      "WHERE \n" +
                      "  T123_UNIDADE_IE = (select T925_LOJA_INTEG_ECOMMERCE from T925_PARAM_GERAIS_UNIDADES) and T123_NUMERO_DAV = ?";
        
        try {
            abrirSessao();
            Connection connection = obterConexaoJDBC();
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, numero_dav);
            ResultSet rs = stm.executeQuery();
            rs.close();
        } catch (JDBCException e) {
            SQLException causa = (SQLException) e.getCause();
            log.error("Erro de banco de dados: " + causa.getLocalizedMessage());
        } catch (Exception e) {
            log.error("Erro de banco de dados: " + e.getLocalizedMessage());
        } finally {
            fecharSessao();
            desconectarConexaoJDBC();
            desconectar();
        }        
        
    }
    
}
