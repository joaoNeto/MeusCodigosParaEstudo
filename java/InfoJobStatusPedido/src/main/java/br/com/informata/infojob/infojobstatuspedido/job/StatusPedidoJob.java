/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobstatuspedido.job;

import br.com.informata.infojob.InfoJobLibJet.wsdl.wsjet.ArrayOfOrderState;
import br.com.informata.infojob.InfoJobLibJet.wsdl.wsjet.OrderState;
import br.com.informata.infojob.infojobstatuspedido.StatusPedidoMain;
import br.com.informata.infojob.infojobstatuspedido.client.OrderStateClient;
import br.com.informata.infojob.infojobstatuspedido.client.OrderStateCompleteClient;
import br.com.informata.infojob.infojobstatuspedido.model.StatusPedido;
import br.com.informata.infojobkernel.exception.InfoJobExceptionDao;
import br.com.informata.infojobkernel.job.AbstractJob;
import java.util.List;
import org.quartz.JobExecutionContext;

/**
 *
 * @author danilo.muniz
 */
public class StatusPedidoJob extends AbstractJob {

    @Override
    protected void setNomeGrupoJar() {
        super.nomeJar = StatusPedidoMain.nomeJar;
        super.grupoJar = StatusPedidoMain.grupoJar;
    }

    @Override
    protected void processaOperacao(JobExecutionContext jec) {
        try {
            int idConfiguracaoWebService = 4, idOrderStateTracking = 0, idOrderStateConcluido = 0;     
            
            OrderStateCompleteClient orderStateCompleteClient = new OrderStateCompleteClient(idConfiguracaoWebService);
            OrderStateClient orderStateClient = new OrderStateClient(idConfiguracaoWebService);

            orderStateClient.executarRecebeInterfaceExport();
            ArrayOfOrderState arrayOfOrderState = orderStateClient.getArrayOfOrderState();
            for (OrderState orderState : arrayOfOrderState.getOrderState()) {
                if (orderState.getDescription().equalsIgnoreCase("Tracking")) {
                    idOrderStateTracking = orderState.getIdOrderState();
                }
                if (orderState.getDescription().equalsIgnoreCase("Concluído")) {
                    idOrderStateConcluido = orderState.getIdOrderState();
                }
            }

            orderStateCompleteClient.idOrderStateTracking = idOrderStateTracking;            
            orderStateClient.idOrderStateConcluido        = idOrderStateConcluido;            
            
            List<StatusPedido> listaStatusTracking   = (List<StatusPedido>) orderStateCompleteClient.carregarInterfaceImport();         
            List<StatusPedido> listaStatusConcluido  = (List<StatusPedido>) orderStateClient.carregarInterfaceImport();         
            
            orderStateCompleteClient.executarEnvioInterfaceImport(listaStatusTracking);   // pedidos em status tracking
            orderStateClient.executarEnvioInterfaceImport(listaStatusConcluido);
            
        } catch (Exception | InfoJobExceptionDao e) {
            log.error("Aconteceu algum erro no método processaOperacao: ", e);
        }
    }
}
