/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.informata.infojob.infojobstatuspedido.model;

/**
 *
 * @author danilo.muniz
 */
public class StatusPedido {

    private String t123NumeroPrenota;
    private String t123NumeroDav;
    private String t163TrackNumber;

    public StatusPedido() {
    }

    public StatusPedido(String t123NumeroPrenota, String t123NumeroDav, String t163TrackNumber) {
        this.t123NumeroPrenota = t123NumeroPrenota;
        this.t123NumeroDav = t123NumeroDav;
        this.t163TrackNumber = t163TrackNumber;
    }

    public String getT123NumeroPrenota() {
        return t123NumeroPrenota;
    }

    public void setT123NumeroPrenota(String t123NumeroPrenota) {
        this.t123NumeroPrenota = t123NumeroPrenota;
    }

    public String getT123NumeroDav() {
        return t123NumeroDav;
    }

    public void setT123NumeroDav(String t123NumeroDav) {
        this.t123NumeroDav = t123NumeroDav;
    }

    public String getT163TrackNumber() {
        return t163TrackNumber;
    }

    public void setT163TrackNumber(String t163TrackNumber) {
        this.t163TrackNumber = t163TrackNumber;
    }

    @Override
    public String toString() {
        return "StatusPedido{" + "t123NumeroPrenota=" + t123NumeroPrenota + ", t123NumeroDav=" + t123NumeroDav + ", t163TrackNumber=" + t163TrackNumber + '}';
    }

}
