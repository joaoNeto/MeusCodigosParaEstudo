package br.com.informata.infojob.infojobstatuspedido.controller;

import br.com.informata.infojob.infojobstatuspedido.dao.StatusPedidoDao;
import br.com.informata.infojob.infojobstatuspedido.model.StatusPedido;
import br.com.informata.infojobkernel.controller.InfoJobController;
import java.util.List;

/**
 *
 * @author danilo.muniz
 */
public class StatusPedidoController extends InfoJobController {

    private final StatusPedidoDao statusPedidoDao;

    public StatusPedidoController() {
        this.statusPedidoDao = new StatusPedidoDao();
    }

    public List<StatusPedido> obtemPedidosStatusTracking() {
        return this.statusPedidoDao.obtemPedidosStatusTracking();
    }

    public List<StatusPedido> obtemPedidosStatusConcluido() {
        return this.statusPedidoDao.obtemPedidosStatusConcluido();
    }
}
