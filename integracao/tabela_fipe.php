<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script type="text/javascript">

var AppAngular = angular.module('myApp', []);

AppAngular.controller('indexCntrl', function($scope,$http) {

    $http.get('http://fipeapi.appspot.com/api/1/carros/marcas.json')
    .then(function (data) {
    	$scope.listaMarcas = data.data;
        console.log(data.data);
    });

    $scope.filtrarVeiculo = function (){

    	document.getElementById('veiculo').disabled = false;
	    $http.get('http://fipeapi.appspot.com/api/1/carros/veiculos/'+$scope.marca+'.json')
	    .then(function (data) {
	    	$scope.listaVeiculo = data.data;
	        console.log(data.data);
	    });

    }

    $scope.filtrarModelo = function(){
    	document.getElementById('modelo').disabled = false;
	    
	    $http.get('http://fipeapi.appspot.com/api/1/carros/veiculo/'+$scope.marca+'/'+$scope.veiculo+'.json')
	    .then(function (data) {
	    	$scope.listaModelos = data.data;
	        console.log(data.data);
	    });

    }

});	
</script>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body ng-app="myApp" ng-controller="indexCntrl">


<select id="marca" ng-model="marca" ng-change="filtrarVeiculo()" placeholder="Informe sua marca">
	<option ng-repeat="atb_marca in listaMarcas" value="{{atb_marca.id}}">{{atb_marca.name}}</option>
</select>

<select placeholder="Informe seu veiculo" id="veiculo" ng-model="veiculo" disabled="" ng-change="filtrarModelo()" >
	<option ng-repeat="atb_veiculo in listaVeiculo" value="{{atb_veiculo.id}}">{{atb_veiculo.name}}</option>
</select>

<select placeholder="Modelos e os anos disponíveis" id="modelo" ng-model="modelo" disabled="" >
	<option ng-repeat="atb_modelo in listaModelos" value="{{atb_modelo.id}}">{{atb_modelo.name}}</option>
</select>

<input type="text" placeholder="Informe o mes / ano" disabled="" >


</body>
</html>


